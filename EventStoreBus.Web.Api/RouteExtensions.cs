﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace EventStoreBus.Web.Api
{
    public static class RouteExtensions
    {
        public static string Route<T>(this UrlHelper urlHelper, HttpMethod method, object parameters)
        {
            var routeName = GetRouteName(typeof(T), method);
            return urlHelper.Route(routeName, parameters);
        }               

        public static HttpRouteCollection MapHttpRoute<T>(this HttpRouteCollection routeCollection, string template, params HttpMethod[] methods)
        {
            var controllerName = GetControllerName(typeof(T));

            foreach (var method in methods)
            {
                var routeName = GetRouteName(typeof(T), method);
                routeCollection.MapHttpRoute(routeName, template, new { controller = controllerName });    
            }
            return routeCollection;
        }

        private static string GetControllerName(Type resourceType)
        {
            return resourceType.Name.Replace("Controller", "");
        }

        private static string GetRouteName(Type resourceType, HttpMethod method)
        {
            return GetControllerName(resourceType).ToLowerInvariant() + "_" + method.Method.ToUpper();
        }
    }
}