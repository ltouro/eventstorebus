﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using EventStore.ClientAPI;

namespace DurableSubscriber.Demo
{
    class Program
    {
        private static SingleStreamEventStoreCheckpointingStrategy checkpointingStrategy;

        private const int TotalCount = 1000;
        private static int i;
        private static ManualResetEventSlim evnt = new ManualResetEventSlim(false);
        private static Stopwatch watch = new Stopwatch();

        static void Main(string[] args)
        {
            var tcpEndpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1113);

            using (var conn = EventStoreConnection.Create())
            {
                conn.Connect(tcpEndpoint);
                var connection = new DefaultEventStoreConnectionAccessor(conn);

                checkpointingStrategy = new SingleStreamEventStoreCheckpointingStrategy(connection, "source");

                var pullSource = new SingleEventStreamPullSource(connection, "TestStream", checkpointingStrategy);
                var pushSource = new SingleEventStreamPushSource(connection, "TestStream");

                var selector = new EventSourceSelector<int>(pullSource, pushSource, new RecordedEventIdentityExtractor());
                selector.Event += OnEvent;

                Console.WriteLine("Press <enter> to start reading");
                Console.ReadLine();

                selector.Start();

                evnt.Wait();

                selector.Stop();

                Console.WriteLine("Elapsed time (ms): {0}", watch.ElapsedMilliseconds);
                Console.WriteLine("Events per second: {0}", ((decimal)TotalCount / (decimal)watch.ElapsedMilliseconds) * 1000);

                Console.WriteLine("Press <enter> to exit");
                Console.ReadLine();
            }
        }

        static void OnEvent(object sender, RecordedEventEventArgs<int> e)
        {
            if (i == 0)
            {
                watch.Start();
            }
            if (i % 100 == 0)
            {
                Console.Write(".");
            }
            if (i == TotalCount-1)
            {
                evnt.Set();
            }
            i++;
        }
    }
}
