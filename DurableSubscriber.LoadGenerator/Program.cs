﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using EventStore.ClientAPI;

namespace DurableSubscriber.LoadGenerator
{
    class Program
    {
        private const int TotalCount = 1000;
        private const int DataSize = 10000;
        private const int MetadataSize = 0;

        static void Main(string[] args)
        {
            var data = new byte[DataSize];
            var metadata = new byte[MetadataSize];

            var tcpEndpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1113);

            using (var conn = EventStoreConnection.Create())
            {
                conn.Connect(tcpEndpoint);

                Console.WriteLine("Press <enter> to start generating events");
                Console.ReadLine();

                var watch = new Stopwatch();
                watch.Start();
                for (int i = 0; i < TotalCount; i++)
                {
                    var eventData = new EventData(Guid.NewGuid(), "TestEvent", false, data, metadata);
                    conn.AppendToStream("TestStream", ExpectedVersion.Any, eventData);
                    if (i % 100 == 0)
                    {
                        Console.Write(".");
                    }
                }
                watch.Stop();
                Console.WriteLine();
                Console.WriteLine("Elapsed time (ms): {0}", watch.ElapsedMilliseconds);
                Console.WriteLine("Events per second: {0}", ((decimal)TotalCount / (decimal)watch.ElapsedMilliseconds)*1000);
                Console.WriteLine("Press <enter> to exit");
                Console.ReadLine();
            }
        }
    }
}
