﻿using System;
using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates
{
    public class PersistentAggregateReference<T> : IAggregateReference<T>
        where T : IAggregate
    {
        private readonly IAggregateReference<T> wrappedReference;
        private readonly int? loadedVersion;
        private readonly HashSet<string> processedCommandIds;

        public PersistentAggregateReference(IAggregateReference<T> wrappedReference, int? loadedVersion, IEnumerable<string> processedCommandIds)
        {
            this.wrappedReference = wrappedReference;
            this.loadedVersion = loadedVersion;
            this.processedCommandIds = new HashSet<string>(processedCommandIds);
        }

        public bool HasCommandAlreadyBeenProcessed(string commandId)
        {
            return processedCommandIds.Contains(commandId);
        }

        public string AggregateId
        {
            get { return wrappedReference.AggregateId; }
        }

        public int? LoadedVersion
        {
            get { return loadedVersion; }
        }

        public T Instance
        {
            get { return wrappedReference.Instance; }
        }

        public void LoadState(IEnumerable<IEvent> events)
        {
            Instance.LoadState(events);
        }
    }
}