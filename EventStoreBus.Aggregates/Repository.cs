﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DurableSubscriber;
using EventStore.ClientAPI;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Logging;
using IEvent = EventStoreBus.Api.IEvent;
using ILogger = EventStoreBus.Logging.ILogger;

namespace EventStoreBus.Aggregates
{
    public class Repository<TAggregate, TStorage> : IRepository<TAggregate>
        where TStorage : IAggregateStorage<TAggregate>, new()
        where TAggregate : IAggregate
    {
        private static readonly ILogger log = LogManager.GetLogger("Repository(" + typeof(TAggregate).Name + ")");

        private static readonly TStorage storage = new TStorage();

        private readonly IFactory<TAggregate> factory;
        private readonly IEventStoreConnectionAccessor connection;
        private readonly ISerializer serializer;

        public Repository(IFactory<TAggregate> factory, IEventStoreConnectionAccessor connection, ISerializer serializer)
        {
            this.factory = factory;
            this.connection = connection;
            this.serializer = serializer;
        }

        public IAggregateReference<TAggregate> Find(string aggregateId)
        {
            StreamEventsSlice slice;
            if (!connection.TryInvoke(x => x.ReadStreamEventsForward(storage.MakeStreamId(aggregateId), 0, int.MaxValue, true), out slice))
            {
                throw new InvalidOperationException("Cannot read stream");
            }
            if (slice.Status != SliceReadStatus.Success)
            {
                return null;
            }
            var deserialized = slice.Events.Select(Deserialize).Where(x => x != null).ToList();   //Only ESB events.
            var messageIds = deserialized.Select(x => x.Metadata.RelatesTo).Where(x => x != null);
            var version = slice.LastEventNumber;

            var newReference = factory.Create(aggregateId);
            var persistentAggregateReference = new PersistentAggregateReference<TAggregate>(newReference, version, messageIds);
            persistentAggregateReference.LoadState(deserialized.Select(x => (IEvent) x.Payload));
            return persistentAggregateReference;
        }

        public IFactory<TAggregate> Factory
        {
            get { return factory; }
        }

        public void SaveChanges(IAggregateReference<TAggregate> aggregateReference, string commandId)
        {
            var streamId = storage.MakeStreamId(aggregateReference.AggregateId);
            var events = aggregateReference.Instance.Changes.Select(x => Serialize(x, commandId)).ToList();
            var expectedVersion = aggregateReference.LoadedVersion.HasValue ? aggregateReference.LoadedVersion.Value : ExpectedVersion.NoStream;
            connection.Invoke(x => x.AppendToStream(streamId, expectedVersion, events));
        }

        public void Invoke(string aggregateId, string commandId, Action<TAggregate> action)
        {
            var reference = Find(aggregateId) ?? Factory.Create(aggregateId);

            if (!reference.HasCommandAlreadyBeenProcessed(commandId))
            {
                action(reference.Instance);
                SaveChanges(reference, commandId);
            }
            else
            {
                log.Debug("Command {0} has already been processed by aggregate {0}. Skipping.", x => x.Args(commandId, aggregateId));
            }
        }

        public void InvokeEnsuringNew(string newAggregateId, string commandId, Action<TAggregate> action)
        {
            var existing = Find(newAggregateId);
            if (existing != null)
            {
                if (existing.HasCommandAlreadyBeenProcessed(commandId))
                {
                    log.Debug("Command {0} has already been processed by aggregate {0}. Skipping.", x => x.Args(commandId, newAggregateId));                    
                    return;
                }
                throw new InvalidOperationException(string.Format("Aggregate {0} already exists and was not created by this command.", newAggregateId));
            }
            var reference = Factory.Create(newAggregateId);

            action(reference.Instance);
            SaveChanges(reference, commandId);
        }

        public void InvokeEnsuringExists(string aggregateId, string commandId, Action<TAggregate> action)
        {
            var reference = Find(aggregateId);
            if (reference == null)
            {
                throw new InvalidOperationException(string.Format("Aggregate {0} not found.", aggregateId));
            }

            if (!reference.HasCommandAlreadyBeenProcessed(commandId))
            {
                action(reference.Instance);
                SaveChanges(reference, commandId);
            }
            else
            {
                log.Debug("Command {0} has already been processed by aggregate {0}. Skipping.", x => x.Args(commandId, aggregateId));                
            }
        }

        private EventData Serialize(IEvent evnt, string commandId)
        {
            return serializer.Serialize(Guid.NewGuid(), evnt, commandId);
        }

        private PersistentEvent Deserialize(ResolvedEvent resolvedEvent)
        {
            return serializer.Deserialize(resolvedEvent.Event);
        }
    }
}