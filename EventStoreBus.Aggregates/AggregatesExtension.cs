﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Aggregates
{
    public class AggregatesExtension : IExtension
    {
        private static readonly ITypeWrapper[] typeWrappers = new ITypeWrapper[]
            {
                new AggregateComponentFactoryTypeWrapper()
            };

        public IEnumerable<ITypeWrapper> TypeWrappers
        {
            get { return typeWrappers; }
        }

        public IEnumerable<ITypeAbbreviationProvider> AbbreviationProviders 
        {
            get { return Enumerable.Empty<ITypeAbbreviationProvider>(); }
        }

        private class AggregateComponentFactoryTypeWrapper : ITypeWrapper
        {
            public bool CanWrap(Type canidate)
            {
                return canidate.GetInterfaces().Contains(typeof (IAggregateComponentFactory))
                    && canidate.GetConstructor(Type.EmptyTypes) != null;
            }

            public Type Wrap(Type bareType)
            {
                return typeof (AggregateComponentFactory<>).MakeGenericType(bareType);
            }
        }        
    }
}