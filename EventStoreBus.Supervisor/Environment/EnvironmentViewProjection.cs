using System;
using System.Linq;
using EventStoreBus.Api;
using EventStoreBus.Config;
using EventStoreBus.Supervisor.EventStores;
using EventStoreBus.Supervisor.Nodes;
using EventStoreBus.Supervisor.ViewStores;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Environment
{
    public class EnvironmentViewProjection
    {
        private const string ViewId = "single";
        private readonly IViewWriter<EnvironmentView> viewWriter;

        public EnvironmentViewProjection(IViewWriter<EnvironmentView> viewWriter)
        {
            this.viewWriter = viewWriter;
        }

        public void When(Envelope<EventStoreAdded> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.CreateOrUpdate(ViewId, evnt,
                                       x => x.Data.EventStores.Add(new EventStoreNode
                                                                       {
                                                                           Address = evnt.Payload.Address,
                                                                           Name = evnt.Payload.StoreId,
                                                                           TcpPort = evnt.Payload.Port
                                                                       }));
            }
        }

        public void When(Envelope<DatabaseCreated> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(ViewId, evnt,
                               x =>
                                   {
                                       var store = x.Data.EventStores.First(es => es.Name == evnt.Payload.StoreId);
                                       store.Databases.Add(new EventStoreDatabase()
                                           {
                                               Name =  evnt.Payload.DatabaseName
                                           });
                                   });
            }
        }

        public void When(Envelope<EventStoreRemoved> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(ViewId, evnt, 
                               x => x.Data.EventStores.RemoveAll(es => es.Name == evnt.Payload.StoreId));
            }
        }

        public void When(Envelope<ViewStoreAdded> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.CreateOrUpdate(ViewId, evnt,
                                       x => x.Data.ViewStores.Add(new ViewStoreNode
                                                                      {
                                                                          Name = evnt.Payload.StoreId,
                                                                          ConnectionString =
                                                                              evnt.Payload.ConnectionString
                                                                      }));
            }
        }

        public void When(Envelope<ViewStoreRemoved> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(ViewId, evnt, 
                    x => x.Data.ViewStores.RemoveAll(es => es.Name == evnt.Payload.StoreId));
            }
        }

        public void When(Envelope<ComponentDeployed> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.CreateOrUpdate(ViewId, evnt,
                                       x => x.Data.Deployments.Add(new ComponentDeployment()
                                                                       {
                                                                           Component = evnt.Payload.Component,
                                                                           DeploymentId = evnt.Payload.DeploymentId,
                                                                           NodeId = evnt.Payload.NodeId,
                                                                           Service = evnt.Payload.Service,
                                                                           Version = evnt.Payload.Version
                                                                       }));
            }
        }
        
        public void When(Envelope<ComponentUndeployed> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(ViewId, evnt, 
                               x => x.Data.Deployments.RemoveAll(es => es.DeploymentId == evnt.Payload.DeploymentId));
            }
        }

        public class Storage : IViewStorage<EnvironmentView>
        {
            public string CollectionName
            {
                get { return "environment"; }
            }
        }

        public class Factory : IViewProjectionFactory
        {
            public object Create(Type viewProjectionImplementation, IViewWriters viewWriters)
            {
                return new EnvironmentViewProjection(viewWriters.GetWriter<EnvironmentView, Storage>());
            }

            public void Release(object projection)
            {
            }
        }
    }
}