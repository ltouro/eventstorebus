﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.EventStores.Handlers
{
    public class EventStoreState : AggregateState
    {
        private readonly List<string> databases = new List<string>(); 

        public IEnumerable<string> Databases
        {
            get { return databases; }
        }

        public void When(DatabaseCreated evnt)
        {
            databases.Add(evnt.DatabaseName);
        }
    }
}