﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;
using System.Linq;

namespace EventStoreBus.Supervisor.EventStores.Handlers
{
    public class EventStore : Aggregate<EventStoreState>
    {
        public void Create(string address, int port)
        {
            Apply(new EventStoreAdded
                      {
                          Address = address,
                          Port = port,
                          StoreId = Id
                      });
        }

        public void CreateDatabase(string name)
        {
            if (State.Databases.Contains(name))
            {
                return;
            }
            Apply(new DatabaseCreated
                {
                    DatabaseName = name,
                    StoreId = Id
                });
        }

        public void Remove()
        {
            Apply(new EventStoreRemoved
                      {
                          StoreId = Id
                      });
        }
    }
}