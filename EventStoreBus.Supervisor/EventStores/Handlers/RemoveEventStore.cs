﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.EventStores.Handlers
{
    [DataContract]
    [Version(1)]
    public class RemoveEventStore : Command
    {
        [DataMember]
        public string StoreId { get; set; }
    }
}