﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.EventStores
{
    [DataContract]
    [Version(1)]
    public class DatabaseCreated : IEvent
    {
        [DataMember]
        public string StoreId { get; set; }

        [DataMember]
        public string DatabaseName { get; set; }
    }
}