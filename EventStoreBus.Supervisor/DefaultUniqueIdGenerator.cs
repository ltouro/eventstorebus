﻿using System;

namespace EventStoreBus.Supervisor
{
    public class DefaultUniqueIdGenerator : IUniqueIdGenerator
    {
        public static readonly IUniqueIdGenerator Instance = new DefaultUniqueIdGenerator();

        public string Generate()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}