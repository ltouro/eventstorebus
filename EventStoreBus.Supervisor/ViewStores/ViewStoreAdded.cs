using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.ViewStores
{
    [DataContract]
    [Version(1)]
    public class ViewStoreAdded : IEvent
    {
        [DataMember]
        public string StoreId { get; set; }

        [DataMember]
        public string ConnectionString { get; set; }
    }
}