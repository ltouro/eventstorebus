using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.ViewStores.Handlers
{
    [DataContract]
    [Version(1)]
    public class AddViewStore : Command
    {
        [DataMember]
        public string StoreId { get; set; }

        [DataMember]
        public string ConnectionString { get; set; }
    }
}