﻿namespace EventStoreBus.Supervisor
{
    public interface IUniqueIdGenerator
    {
        string Generate();
    }
}