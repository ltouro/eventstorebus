﻿namespace EventStoreBus.Supervisor.RestEndpoint.Instance
{

    public enum InstanceState
    {
        Stopped,
        Running
    }
}