﻿using System;
using System.Web.Http;
using EventStoreBus.Api;
using Raven.Client;

namespace EventStoreBus.Supervisor.RestEndpoint
{
    public abstract class BaseController : ApiController
    {
        protected readonly Uri BaseUrl;
        protected readonly ICommandSender CommandSender;

        protected BaseController(Uri baseUrl, ICommandSender commandSender)
        {
            this.BaseUrl = baseUrl;
            this.CommandSender = commandSender;
        }
    }
}