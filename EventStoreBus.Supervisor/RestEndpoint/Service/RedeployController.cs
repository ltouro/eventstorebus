﻿using System;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    public class RedeployController : BaseController
    {
        public RedeployController(Uri baseUrl, IViewReaders viewReaders, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
        }

        public HttpResponseMessage Put()
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            var service = (string)ControllerContext.RouteData.Values["name"];

            CommandSender.SendTo<RedeploymentHandler>(new Redeploy
                                                          {
                                                              Id = operationId,
                                                              Service = service,
                                                          });

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}