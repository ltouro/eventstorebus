﻿using System;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Services.Handlers;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    public class VersionController : BaseController
    {
        public VersionController(Uri baseUrl, IViewReaders viewReaders, ICommandSender commandSender) 
            : base(baseUrl, commandSender)
        {
        }

        public HttpResponseMessage Put(CurrentVersion version)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            var service = (string)ControllerContext.RouteData.Values["name"];

            CommandSender.SendTo<ServicesHandler>(new SetCurrentVersion()
                                                          {
                                                              Id = operationId,
                                                              Service = service,
                                                              Version = version.Version
                                                          });

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}