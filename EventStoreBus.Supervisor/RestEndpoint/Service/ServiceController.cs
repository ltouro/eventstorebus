using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Services.Handlers;
using EventStoreBus.Supervisor.Services.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    public class ServiceController : BaseController
    {
        private readonly IViewReader<ServiceView> serviceReader;

        public ServiceController(Uri baseUrl, IViewReaders viewReaders, ICommandSender commandSender) 
            : base(baseUrl, commandSender)
        {
            serviceReader = viewReaders.GetReader<ServiceView, ServiceViewProjection.Storage>();
        }

        public Service Get(string name)
        {
            var session = serviceReader.OpenSession();
            var view = session.Load(name);

            return new Service(view, Url);
        }

        public HttpResponseMessage Put(NewService service)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            ServiceView existing;
            using (var session = serviceReader.OpenSession())
            {
                existing = session.Load(service.Name);
            }
            if (existing == null)
            {
                CommandSender.SendTo<ServicesHandler>(new RegisterService
                                                          {
                                                              Id = operationId,
                                                              Name = service.Name
                                                          });
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}