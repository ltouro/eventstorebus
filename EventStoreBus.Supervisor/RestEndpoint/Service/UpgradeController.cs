﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    public class UpgradeController : BaseController
    {
        public UpgradeController(Uri baseUrl, IViewReaders viewReaders, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
        }
    }
}