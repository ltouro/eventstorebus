﻿using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.Services.Views;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ServiceReference
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public HyperLink Details { get; set; }

        public ServiceReference(ServiceView view, UrlHelper url)
        {
            Name = view.Name;
            Details = url.LinkToGet<ServiceController>("details", new { name = view.Name});
        }
    }
}