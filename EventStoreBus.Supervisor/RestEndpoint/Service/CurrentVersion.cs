﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class CurrentVersion
    {
        [DataMember]
        public string Version { get; set; }
    }
}