﻿using System.Net.Http;
using System.Web.Http.Routing;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Supervisor.RestEndpoint
{
    public static class HyperLinkExtensions
    {
        public static HyperLink LinkTo<T>(this UrlHelper urlHelper, string relation, HttpMethod method, object parameters)
        {
            return new HyperLink(relation, urlHelper.Route<T>(method, parameters), method);
        }
        
        public static HyperLink LinkToGet<T>(this UrlHelper urlHelper, string relation, object parameters)
        {
            return LinkTo<T>(urlHelper, relation, HttpMethod.Get, parameters);
        }
        
        public static HyperLink LinkToPut<T>(this UrlHelper urlHelper, string relation, object parameters)
        {
            return LinkTo<T>(urlHelper, relation, HttpMethod.Put, parameters);
        }
        
        public static HyperLink LinkToDelete<T>(this UrlHelper urlHelper, string relation, object parameters)
        {
            return LinkTo<T>(urlHelper, relation, HttpMethod.Delete, parameters);
        }
    }
}