﻿using System.Net.Http;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class HyperLink
    {
        [DataMember]
        public string Relation { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Verb { get; set; }

        public HyperLink()
        {
        }

        public HyperLink(string relation, string url, HttpMethod verb)
        {
            Relation = relation;
            Url = url;
            Verb = verb.Method;
        }
    }
}