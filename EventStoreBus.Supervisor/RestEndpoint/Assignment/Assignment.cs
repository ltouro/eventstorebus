﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.RestEndpoint.Node;

namespace EventStoreBus.Supervisor.RestEndpoint.Assignment
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Assignment
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Component { get; set; }

        [DataMember]
        public HyperLink Parent { get; set; }
        [DataMember]
        public HyperLink Self { get; set; }
        [DataMember]
        public HyperLink Unassign { get; set; }

        public Assignment(Nodes.Views.Assignment assignment, string nodeId, UrlHelper url)
        {
            Id = assignment.AssignmentId;
            Service = assignment.Service;
            Component = assignment.Component;

            var routeValues = new
            {
                assignmentId = assignment.AssignmentId,
                nodeId = nodeId
            };

            Self = url.LinkTo<AssignmentController>("self", HttpMethod.Get, routeValues);
            Parent = url.LinkTo<NodeController>("parent", HttpMethod.Get, routeValues);
            Unassign = url.LinkTo<AssignmentController>("unassign", HttpMethod.Delete, routeValues);
        }
    }
}