using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.Deployment
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewDeployment
    {
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string Component { get; set; }
    }
}