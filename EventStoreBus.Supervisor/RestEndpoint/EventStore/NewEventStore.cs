﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewEventStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int Port { get; set; }
    }
}