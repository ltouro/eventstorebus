﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Config;
using EventStoreBus.Supervisor.RestEndpoint.Environment;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class EventStoreCollection
    {
        [DataMember]
        public List<EventStoreReference> Stores { get; set; }
        
        [DataMember]
        public HyperLink Register { get; set; }

        [DataMember]
        public HyperLink Parent { get; set; }

        public EventStoreCollection(IEnumerable<EventStoreNode> nodes, UrlHelper url)
        {
            Stores = nodes.Select(x => new EventStoreReference(x, url)).ToList();
            Register = url.LinkToPut<EventStoreController>("register", new {operationId = Guid.NewGuid().ToString()});
            Parent = url.LinkToGet<EnvironmentController>("parent", new { });
        }
    }
}