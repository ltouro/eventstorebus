﻿using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Config;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class EventStoreReference
    {
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public HyperLink Details { get; set; }

        public EventStoreReference(EventStoreNode node, UrlHelper url)
        {
            Name = node.Name;
            Details = url.LinkToGet<EventStoreController>("details", new { name = node.Name});
        }
    }
}