﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewDatabase
    {
        [DataMember]
        public string DatabaseName { get; set; }
    }
}