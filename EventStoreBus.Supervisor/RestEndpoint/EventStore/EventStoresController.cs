﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Environment;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    public class EventStoresController : BaseController
    {
        private readonly IViewReader<EnvironmentView> envReader;

        public EventStoresController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            envReader = viewStore.GetReader<EnvironmentView, EnvironmentViewProjection.Storage>();
        }

        public EventStoreCollection Get()
        {
            using (var session = envReader.OpenSession())
            {
                return new EventStoreCollection(session.LoadSingle().Data.EventStores, Url);
            }
        }
    }
}