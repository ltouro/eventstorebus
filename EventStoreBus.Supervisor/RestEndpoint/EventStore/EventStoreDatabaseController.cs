﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Environment;
using EventStoreBus.Supervisor.EventStores.Handlers;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    public class EventStoreDatabaseController : BaseController
    {
         private readonly IViewReader<EnvironmentView> envReader;

         public EventStoreDatabaseController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            envReader = viewStore.GetReader<EnvironmentView, EnvironmentViewProjection.Storage>();
        }

        public HttpResponseMessage Put(NewDatabase newDatabase)
        {
            var storeName = (string)ControllerContext.RouteData.Values["name"];
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            EnvironmentView view;
            using (var session = envReader.OpenSession())
            {
                view = session.LoadSingle();
            }
            var store = view.Data.EventStores.FirstOrDefault(x => x.Name == storeName);
            if (store == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            CommandSender.SendTo<EventStoresHandler>(new CreateDatabase()
                {
                    DatabaseName = newDatabase.DatabaseName,
                    StoreId = storeName,
                    Id = operationId
                });
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}