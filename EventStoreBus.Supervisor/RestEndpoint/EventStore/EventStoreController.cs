using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Environment;
using EventStoreBus.Supervisor.EventStores.Handlers;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    public class EventStoreController : BaseController
    {
        private readonly IViewReader<EnvironmentView> envReader;

        public EventStoreController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            envReader = viewStore.GetReader<EnvironmentView, EnvironmentViewProjection.Storage>();
        }

        public HttpResponseMessage Get(string name)
        {
            EnvironmentView view;
            using (var session = envReader.OpenSession())
            {
                view = session.LoadSingle();
            }
            var store = view.Data.EventStores.FirstOrDefault(x => x.Name == name);
            if (store == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, new EventStore(store, Url));
        }

        public HttpResponseMessage Put(NewEventStore store)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            EnvironmentView view;
            using (var session = envReader.OpenSession())
            {
                view = session.LoadSingle();
            }
            var storeWithSameName = view.Data.EventStores.FirstOrDefault(x => x.Name == store.Name);
            if (storeWithSameName != null)
            {
                if (storeWithSameName.Address != store.Address || storeWithSameName.TcpPort != store.Port)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            CommandSender.SendTo<EventStoresHandler>(new AddEventStore()
                                                         {
                                                             StoreId = store.Name,
                                                             Address = store.Address,
                                                             Port = store.Port,
                                                             Id = operationId
                                                         });
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}