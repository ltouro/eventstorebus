﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Config;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class EventStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int TcpPort { get; set; }
        [DataMember]
        public int HttpPort { get; set; }
        [DataMember]
        public List<EventStoreDatabase> Databases { get; set; }
        [DataMember]
        public HyperLink Remove { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
        [DataMember]
        public HyperLink CreateDatabase { get; set; }

        public EventStore(EventStoreNode node, UrlHelper url)
        {
            Name = node.Name;
            Address = node.Address;
            TcpPort = node.TcpPort;
            HttpPort = node.HttpPort;
            Manage = new HyperLink("manage", string.Format("http://{0}:{1}", node.Address, node.HttpPort), HttpMethod.Get);
            CreateDatabase = url.LinkToPut<EventStoreDatabaseController>("createDatabase", new
                {
                    operationId = Guid.NewGuid().ToString(),
                    name = node.Name
                });
            Databases = node.Databases.Select(x => new EventStoreDatabase()
                {
                    Name = x.Name,
                    Events = new HyperLink("events", string.Format("http://{0}:{1}/streams/$ce-{2}", node.Address, node.HttpPort, x.Name), HttpMethod.Get)
                }).ToList();
            //Remove = url.LinkTo<>()
        }

    }
}