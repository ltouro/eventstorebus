﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Supervisor.RestEndpoint.Assignment;
using EventStoreBus.Supervisor.RestEndpoint.Deployment;

namespace EventStoreBus.Supervisor.RestEndpoint.Node
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Node
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
        [DataMember]
        public HyperLink Deploy { get; set; } 
        [DataMember]
        public HyperLink Assign { get; set; }      
        [DataMember]
        public List<Deployment.Deployment> Deployments { get; set; }
        [DataMember]
        public List<Assignment.Assignment> Assignments { get; set; }

        public Node(NodeView nodeView, UrlHelper url)
        {
            Id = nodeView.NodeId;
            Url = nodeView.Url;
            Deployments = nodeView.Deployments.Select(x => new Deployment.Deployment(x, nodeView.NodeId, url)).ToList();
            Assignments = nodeView.Assignments.Select(x => new Assignment.Assignment(x, nodeView.NodeId, url)).ToList();

            Deploy = url.LinkToPut<DeployController>("deploy", GetRouteValues(nodeView));
            Assign = url.LinkToPut<AssignmentController>("assign", GetRouteValues(nodeView));
        }

        private static object GetRouteValues(NodeView nodeView)
        {
            return new
                       {
                           nodeId = nodeView.NodeId,
                           operationId = Guid.NewGuid().ToString()
                       };
        }
    }
}