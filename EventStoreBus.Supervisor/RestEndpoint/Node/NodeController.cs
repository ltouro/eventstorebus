using System;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Node
{
    public class NodeController : BaseController
    {
        private readonly IViewReader<NodeView> nodeReader;

        public NodeController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender) 
            : base(baseUrl, commandSender)
        {
            nodeReader = viewStore.GetReader<NodeView, NodeViewProjection.Storage>();
        }

        public HttpResponseMessage Get(string nodeId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            var result = new Node(nodeView, Url);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        public HttpResponseMessage Put(NewNode node)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            NodeView existing;
            using (var session = nodeReader.OpenSession())
            {
                existing = session.Load(node.Id);
            }

            if (existing != null)
            {
                return Request.CreateResponse(existing.Url != node.Url 
                    ? HttpStatusCode.Conflict 
                    : HttpStatusCode.OK);
            }
            CommandSender.SendTo<NodesHandler>(new AddNode()
                                                   {
                                                       NodeId = node.Id,
                                                       ListenUrl = node.Url,
                                                       Id = operationId
                                                   });
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}