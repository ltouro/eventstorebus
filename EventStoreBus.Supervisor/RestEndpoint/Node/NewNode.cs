﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.Node
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewNode
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
}