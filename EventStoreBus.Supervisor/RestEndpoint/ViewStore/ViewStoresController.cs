﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Environment;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.ViewStore
{
    public class ViewStoresController : BaseController
    {
        private readonly IViewReader<EnvironmentView> envReader;

        public ViewStoresController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            envReader = viewStore.GetReader<EnvironmentView, EnvironmentViewProjection.Storage>();
        }

        public ViewStoreCollection Get()
        {
            using (var session = envReader.OpenSession())
            {
                return new ViewStoreCollection(session.LoadSingle().Data.ViewStores, Url);
            }
        }
    }
}