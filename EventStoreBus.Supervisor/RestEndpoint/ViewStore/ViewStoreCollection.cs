﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Config;
using EventStoreBus.Supervisor.RestEndpoint.Environment;
using EventStoreBus.Supervisor.RestEndpoint.EventStore;

namespace EventStoreBus.Supervisor.RestEndpoint.ViewStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ViewStoreCollection
    {
        [DataMember]
        public List<ViewStoreReference> Stores { get; set; }
        
        [DataMember]
        public HyperLink Register { get; set; }

        [DataMember]
        public HyperLink Parent { get; set; }

        public ViewStoreCollection(IEnumerable<ViewStoreNode> nodes, UrlHelper url)
        {
            Stores = nodes.Select(x => new ViewStoreReference(x, url)).ToList();
            Register = url.LinkToPut<ViewStoreController>("register", new {operationId = Guid.NewGuid().ToString()});
            Parent = url.LinkToGet<EnvironmentController>("parent", new { });
        }
    }
}