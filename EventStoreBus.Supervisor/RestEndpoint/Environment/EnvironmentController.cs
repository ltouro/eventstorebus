using System;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Environment
{
    public class EnvironmentController : BaseController
    {
        public EnvironmentController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new Environment(Url));
        }
    }
}