﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes;
using EventStoreBus.Supervisor.Services.Handlers;

namespace EventStoreBus.Supervisor.Services.Receptors
{
    public class ServicesReceptor
    {
        private readonly ICommandSender commandSender;

        public ServicesReceptor(ICommandSender commandSender)
        {
            this.commandSender = commandSender;
        }

        public void When(ComponentDeployed evnt, Guid eventId)
        {
            commandSender.SendTo<ServicesHandler>(new RegisterDeployment
                                                      {
                                                          DeploymentId = evnt.DeploymentId,
                                                          NodeId = evnt.NodeId,
                                                          Component = evnt.Component,
                                                          Service = evnt.Service,
                                                          Version = evnt.Version,
                                                          Id = eventId.ToString()
                                                      });
        }

        public void When(ComponentUndeployed evnt, Guid eventId)
        {
            commandSender.SendTo<ServicesHandler>(new UnregisterDeployment
                                                      {
                                                          DeploymentId = evnt.DeploymentId,
                                                          Id = eventId.ToString(),
                                                          NodeId = evnt.NodeId,
                                                          Service = evnt.Service
                                                      });
        }
    }
}