﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Services.Receptors
{
    public class ServicesReceptorFactory : IReceptorFactory
    {
        public object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders)
        {
            return new ServicesReceptor(commandSender);
        }

        public void Release(object receptor)
        {
        }
    }
}