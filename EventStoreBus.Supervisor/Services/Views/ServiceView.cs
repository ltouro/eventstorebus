﻿using System.Collections.Generic;

namespace EventStoreBus.Supervisor.Services.Views
{
    public class ServiceView : EventStoreBus.Views.Api.ConcurrentViewBase
    {
        public string Name { get; set; }
        public List<string> AvailableVersions { get; set; }
        public string InstalledVersion { get; set; }
        public string InstalledVersionId { get; set; }
        public string LockId { get; set; }
        public List<ComponentDeployment> DeployedComponents { get; set; }

        public ServiceView()
        {
            DeployedComponents = new List<ComponentDeployment>();
        }
    }
}