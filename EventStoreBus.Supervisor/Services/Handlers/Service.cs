﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    public class Service : Aggregate<ServiceState>
    {
        public void Register()
        {
            Apply(new ServiceRegistered()
                      {
                          Name = Id
                      });
        }

        public void RegisterDeployment(string nodeId, string deploymentId, string component, string version)
        {
            Apply(new ServiceComponentDeployed()
                      {
                          Component = component,
                          DeploymentId = deploymentId,
                          NodeId = nodeId,
                          Service = Id,
                          Version = version
                      });
        }
        
        public void UnregisterDeployment(string deploymentId)
        {
            var deployment = State.ComponentDeployments.FirstOrDefault(x => x.Id == deploymentId);
            Apply(new ServiceComponentUndeployed()
                      {
                          Component = deployment.Component,
                          DeploymentId = deploymentId,
                          NodeId = deployment.NodeId,
                          Service = Id,
                          Version = deployment.Version
                      });
        }

        public void Lock(string lockId)
        {
            if (State.LockId != null)
            {
                Apply(new LockFailed()
                          {
                              LockId = lockId,
                          });
            }
            else
            {
                Apply(new Locked()
                          {
                              LockId = lockId,
                              ServiceName = Id
                          });
            }
        }

        public void Unlock(string lockId)
        {
            if (State.LockId == lockId)
            {
                Apply(new Unlocked()
                          {
                              LockId = lockId,
                              ServiceName = Id
                          });
            }
        }

        public void SetCurrentVersion(string version)
        {
            Apply(new ServiceVersionSet
                      {
                          ServiceName = Id,
                          Version = version
                      });
        }
    }
}