﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    public class ServicesHandlerFactory : IAggregateComponentFactory
    {
        public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
        {
            return new ServicesHandler(repositories.Get<Service>());
        }

        public void Release(object component)
        {
        }
    }
}