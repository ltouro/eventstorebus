﻿using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    public class ServicesHandler
    {
        private readonly IRepository<Service> services;

        public ServicesHandler(IRepository<Service> services)
        {
            this.services = services;
        }

        public void When(SetCurrentVersion command)
        {
            services.InvokeEnsuringExists(command.Service, command.Id,
                                        service => service.SetCurrentVersion(command.Version));
        }

        public void When(RegisterService command)
        {
            services.InvokeEnsuringNew(command.Name, command.Id,
                service => service.Register());
        }

        public void When(RegisterDeployment command)
        {
            services.InvokeEnsuringExists(command.Service, command.Id,
                service => service.RegisterDeployment(command.NodeId, command.DeploymentId, command.Component, command.Version));
        }
        
        public void When(UnregisterDeployment command)
        {
            services.InvokeEnsuringExists(command.Service, command.Id,
                                        service => service.UnregisterDeployment(command.DeploymentId));
        }

        public void When(Lock command)
        {
            services.InvokeEnsuringExists(command.Service, command.Id,
                                        service => service.Lock(command.LockId));
        }
        
        public void When(Unlock command)
        {
            services.InvokeEnsuringExists(command.Service, command.Id,
                                        service => service.Unlock(command.LockId));
        }
    }
}