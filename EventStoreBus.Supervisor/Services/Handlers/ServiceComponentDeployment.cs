﻿namespace EventStoreBus.Supervisor.Services.Handlers
{
    public class ServiceComponentDeployment
    {
        private readonly string id;
        private readonly string nodeId;
        private readonly string component;
        private readonly string version;

        public ServiceComponentDeployment(string id, string nodeId, string component, string version)
        {
            this.id = id;
            this.nodeId = nodeId;
            this.component = component;
            this.version = version;
        }

        public string Id
        {
            get { return id; }
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string Component
        {
            get { return component; }
        }

        public string Version
        {
            get { return version; }
        }
    }
}