﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services
{
    [DataContract]
    [Version(1)]
    public class ServiceVersionAdded : IEvent
    {
        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public string Version { get; set; }
    }
}