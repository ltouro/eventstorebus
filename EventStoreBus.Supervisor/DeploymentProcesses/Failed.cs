﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [Version(1)]
    [DataContract]
    public class Failed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string FailedNode { get; set; }

        [DataMember]
        public string FailedComponent { get; set; }

        [DataMember]
        public string FailedDeploymentId { get; set; }
    }
}