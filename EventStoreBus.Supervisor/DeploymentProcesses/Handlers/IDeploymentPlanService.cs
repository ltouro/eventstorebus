using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public interface IDeploymentPlanService
    {
        DeploymentPlan BuildRedeploymentPlan(string service, string lockId);
    }
}