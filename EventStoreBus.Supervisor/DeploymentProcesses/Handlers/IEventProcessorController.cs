﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public interface IEventProcessorController
    {
        void DisableGateways(string deploymentId, string eventProcessorName);
        void EnableGateways(string deploymentId, string eventProcessorName);
    }
}