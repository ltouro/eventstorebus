﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class FailedBuilder
    {
        public static Failed Build(string id, RedeploymentProcessState state)
        {
            return new Failed()
            {
                ProcessId = id,
                Service = state.Service,
            };
        }

        public static Failed Build(string id, PlannedDeployment plannedDeployment, RedeploymentProcessState state)
        {
            return new Failed()
                       {
                           ProcessId = id,
                           Service = state.Service,
                           FailedComponent = plannedDeployment.Component,
                           FailedNode = plannedDeployment.NodeId,
                           FailedDeploymentId = plannedDeployment.DeploymentId
                       };
        }
        
        public static Failed Build(string id, PlannedUndeployment plannedUndeployment, RedeploymentProcessState state)
        {
            return new Failed()
                       {
                           ProcessId = id,
                           Service = state.Service,
                           FailedComponent = plannedUndeployment.Component,
                           FailedNode = plannedUndeployment.NodeId,
                           FailedDeploymentId = plannedUndeployment.ExistingDeploymentId
                       };
        }
    }
}