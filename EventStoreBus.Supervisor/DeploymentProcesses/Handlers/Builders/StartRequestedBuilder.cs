﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class StartRequestedBuilder
    {
        public static StartRequested Build(string id, RedeploymentProcessState state)
        {
            return new StartRequested
                       {
                           DeploymentId = state.CurrentDeployment.DeploymentId,
                           NodeId = state.CurrentDeployment.NodeId,
                           ProcessId = id,
                       };
        }
    }
}