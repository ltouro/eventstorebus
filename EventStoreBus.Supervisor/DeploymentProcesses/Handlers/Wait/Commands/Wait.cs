﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Wait.Commands
{
    [DataContract]
    [Version(1)]
    public class Wait : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<WaitRequest> Requests { get; set; }

        public Wait(string commandId, string parentProcessId, IEnumerable<WaitRequest> requests) 
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            Requests = requests.ToList();
        }

        public Wait()
        {
        }
    }
}