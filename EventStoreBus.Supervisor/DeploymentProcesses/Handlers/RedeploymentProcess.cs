using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public class RedeploymentProcess : Aggregate<RedeploymentProcessState>
    {
        /*
         * Deploy to new locations                     Undeploy from new locations
         * Start new deployments (except receptors)    Stop new deployments
         * Reconfigure router                          Reconfigure router back
         * Stop old deployments                        Start old deployments 
         * Start new receptors                          
         */
        public void Create(string service, string version, IUniqueIdGenerator uniqueIdGenerator)
        {
            Apply(new Created()
                      {
                          Service = service,
                          RequestedVersion = version
                      });
            Apply(new LockRequested
                      {
                          ProcessId = Id,
                          Service = State.Service,
                          LockId = uniqueIdGenerator.Generate()
                      });
        }

        public void HandleLockSuccess()
        {
            Apply(new Locked
                      {
                          LockId = State.LockId
                      });
            Apply(DeploymentPlanRequestedBuilder.Build(Id, State));
        }

        public void HandleLockFailure()
        {
            Apply(new Failed()
            {
                ProcessId = Id,
                Service = State.Service
            });
        }

        public void LoadPlan(DeploymentPlan plan)
        {
            if (plan == null)
            {
                if (State.PlanAttempts < 5)
                {
                    Apply(DeploymentPlanRequestedBuilder.Build(Id, State));
                }
                else
                {
                    OnFailure();
                }
            }
            else
            {
                LoadPlanAndStart(plan);
            }
        }

        private void LoadPlanAndStart(DeploymentPlan plan)
        {
            var requestedVersion = State.Version;
            if (plan.ServiceVersion != null)
            {
                Apply(new VersionDetermined()
                          {
                              Version = plan.ServiceVersion
                          });
            }
            foreach (var deployAction in plan.ToDeploy)
            {
                Apply(new DeploymentPlanned()
                          {
                              Component = deployAction.Component,
                              NodeId = deployAction.NodeId
                          });
            }
            foreach (var undeployAction in plan.ToUndeploy)
            {
                Apply(new UndeploymentPlanned()
                          {
                              Component = undeployAction.Component,
                              NodeId = undeployAction.NodeId,
                              ExistingDeploymentId = undeployAction.ExistingDeploymentId
                          });
            }
            if (plan.ServiceVersion != null && requestedVersion != null)
            {
                //version conflict
                OnFailure();
                return;
            }
            if (plan.ServiceVersion == null && requestedVersion == null)
            {
                //no version specified
                OnFailure();
                return;
            }
            DeployNext();
        }

        public void HandleDeploySuccess(string operationId, string deploymentId)
        {
            Apply(new DeploymentSucceeded()
                      {
                          DeploymentId = deploymentId,
                          ProcessId = Id,
                          OperationId = operationId
                      });
            StartCurrent();
        }
        
        public void HandleDeployFailure(string deploymentId)
        {
            OnFailure(State.CurrentDeployment);
        }

        public void HandleStartSuccess(string operationId, string deploymentId)
        {
            Apply(new StartSucceeded()
                      {
                          DeploymentId = deploymentId,
                          OperationId = operationId,
                          ProcessId = Id
                      });
            DeployNext();
        }
        
        public void HandleStartFailure(string deploymentId)
        {
            OnFailure(State.CurrentDeployment);
        }

        public void HandleStopSuccess(string operationId, string deploymentId)
        {
            Apply(new StopSucceeded()
                      {
                          DeploymentId = deploymentId,
                          OperationId = operationId,
                          ProcessId = Id
                      });
            UndeployCurrent();
        }
        
        public void HandleStopFailure(string deploymentId)
        {
            OnFailure(State.CurrentUndeployment);
        }

        public void HandleUndeploySuccess(string operationId, string deploymentId)
        {
            Apply(new UndeploymentSucceeded()
                      {
                          DeploymentId = deploymentId,
                          OperationId = operationId,
                          ProcessId = Id
                      });
            StopNext();                
        }
        
        public void HandleUndeployFailure(string deploymentId)
        {
            OnFailure(State.CurrentUndeployment);
        }

        private void OnFailure(PlannedDeployment plannedDeployment)
        {
            Apply(UnlockRequestedBuilder.Build(Id, State));
            Apply(FailedBuilder.Build(Id, plannedDeployment, State));
        }
        
        private void OnFailure(PlannedUndeployment plannedUndeployment)
        {
            Apply(UnlockRequestedBuilder.Build(Id, State));
            Apply(FailedBuilder.Build(Id, plannedUndeployment, State));
        }
        
        private void OnFailure()
        {
            Apply(UnlockRequestedBuilder.Build(Id, State));
            Apply(FailedBuilder.Build(Id, State));
        }

        private void OnSuccess()
        {
            Apply(UnlockRequestedBuilder.Build(Id, State));
            Apply(new Succeeded()
                      {
                          ProcessId = Id,
                          Service = State.Service,
                          Version = State.Version
                      });
        }

        private void DeployNext()
        {
            if (State.NoMoreDeployments)
            {
                StopNext();
            }
            else
            {
                Apply(DeploymentRequestedBuilder.Build(Id, State));
            }
        }

        private void StartCurrent()
        {
            Apply(StartRequestedBuilder.Build(Id, State));
        }

        private void StopNext()
        {
            if (State.NoMoreUndeployments)
            {
                OnSuccess();
            }
            else
            {
                Apply(StopRequestedBuilder.Build(Id, State));                
            }
        }

        private void UndeployCurrent()
        {
            Apply(UndeploymentRequestedBuilder.Build(Id, State));
        }
    }
}