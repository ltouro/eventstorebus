﻿using System.Collections.Generic;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter
{
    public interface IRouterController
    {
        void SetWorkersForEndpoint(string endpointName, IEnumerable<string> workerUrls);
    }
}