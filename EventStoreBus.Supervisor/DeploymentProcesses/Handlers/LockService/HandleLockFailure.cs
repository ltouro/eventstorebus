﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    [DataContract]
    [Version(1)]
    public class HandleLockFailure : Command
    {
        [DataMember]
        public string ProcessId { get; set; }
    }
}