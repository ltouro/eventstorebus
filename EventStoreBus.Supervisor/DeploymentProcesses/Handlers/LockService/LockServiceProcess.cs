using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    public class LockServiceProcess : Aggregate<LockServiceProcessState>
    {
        public void RequestLock(string parentProcessId, string service, IUniqueIdGenerator uniqueIdGenerator)
        {
            Apply(new LockRequested
                {
                    ParentProcessId = parentProcessId,
                    ProcessId = Id,
                    Service = service,
                    LockId = uniqueIdGenerator.Generate()
                });
        }

        public void HandleLockSuccess()
        {
            Apply(new Locked
                {
                    ParentProcessId = State.ParentProcessId,
                    ProcessId = this.Id,
                    LockId = State.LockId,
                    Service = State.Service
                });
        }

        public void HandleLockFailure()
        {
            Apply(new Failed()
                {
                    ParentProcessId = State.ParentProcessId,
                    ProcessId = Id,
                    Service = State.Service
                });
        }
    }
}