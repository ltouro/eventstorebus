﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    [DataContract]
    [Version(1)]
    public class Lock : Command
    {
        [DataMember]
        public string Service { get; set; }
        
        [DataMember]
        public string ParentProcessId { get; set; }
    }
}