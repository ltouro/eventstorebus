﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Receptors;
using EventStoreBus.Supervisor.DeploymentProcesses.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    public class LockServiceProcessReceptor
    {
        private readonly IViewReader<CorrelationView> viewReader;
        private readonly ICommandSender commandSender;

        public LockServiceProcessReceptor(ICommandSender commandSender, IViewReader<CorrelationView> viewReader)
        {
            this.viewReader = viewReader;
            this.commandSender = commandSender;
        }

        public void When(Services.Locked evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.LockId,
                                               processId => new HandleLockSuccess()
                                                                {
                                                                    ProcessId = processId,
                                                                    Id = eventId.ToString()
                                                                });
        }

        private void SendIfRelated<T>(string operationId, Func<string, ICommand> commandCreator)
        {
            CorrelationView correlationView;
            using (var session = viewReader.OpenSession())
            {
                correlationView = session.Load(operationId);
            }
            if (correlationView != null)
            {
                commandSender.SendTo<T>(commandCreator(correlationView.ProcessId));
            }
        }

        public class Factory : IReceptorFactory
        {
            public object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders)
            {
                return new RedeploymentReceptor(commandSender,
                                                viewReaders.GetReader<CorrelationView, CorrelationViewProjection.Storage>());
            }

            public void Release(object receptor)
            {
            }
        }
    }
}