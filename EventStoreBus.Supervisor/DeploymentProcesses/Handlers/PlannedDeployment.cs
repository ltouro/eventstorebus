﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public class PlannedDeployment
    {
        private readonly string nodeId;
        private readonly string component;
        private string deploymentId;

        public PlannedDeployment(string nodeId, string component)
        {
            this.nodeId = nodeId;
            this.component = component;
        }

        public void On(DeploymentSucceeded evnt)
        {
            deploymentId = evnt.DeploymentId;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string Component
        {
            get { return component; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }
    }
}