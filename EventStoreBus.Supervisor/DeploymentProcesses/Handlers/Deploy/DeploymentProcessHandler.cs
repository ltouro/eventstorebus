﻿using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    public class DeploymentProcessHandler
    {
        private readonly IRepository<DeployProcess> repository;

        public DeploymentProcessHandler(IRepository<DeployProcess> repository)
        {
            this.repository = repository;
        }

        public void When(BeginDeploymentSequence command)
        {
            var requests = command.Requests.Select(x => new DeploymentRequest(x.Id, x.NodeId, x.Service, x.Component, x.Version));

            repository.InvokeEnsuringNew(command.Id, command.Id,
                process => process.Start(command.ParentProcessId, requests));
        }

        public void When(NotifyDeploymentSucceeded command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.HandleDeploySuccess(command.TaskId, command.DeploymentId));
        }

        public void When(NotifyDeploymentFailed command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.HandleDeployFailure(command.TaskId, command.DeploymentId, command.DeploymentUrl));
        }
    }
}