﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    public class DeployProcess : Aggregate<DeployProcessState>
    {
        private readonly int maxAttemptCount;

        public DeployProcess()
            : this(5)
        {
        }

        public DeployProcess(int maxAttemptCount)
        {
            this.maxAttemptCount = maxAttemptCount;
        }

        public void Start(string parentProcessId, IEnumerable<DeploymentRequest> requests)
        {
            Apply(new DeploymentSequenceStarted(Id, parentProcessId));
            foreach (var request in requests)
            {
                Apply(new DeploymentRequested(Id, request.Id, request.NodeId, request.Service, request.Version, request.Component, maxAttemptCount));
            }
        }

        public void HandleDeploySuccess(string taskId, string deploymentId)
        {
            Apply(new DeploymentSucceeded(Id, deploymentId, taskId));
            TryComplete();
        }

        public void HandleDeployFailure(string taskId, string deploymentId, string deploymentUrl)
        {
            var task = State.GetTaskById(taskId);
            var retry = NoFailedStartsSoFar() && task.CanBeRetried();

            Apply(new DeploymentFailed(Id, deploymentId, taskId, retry));

            if (retry)
            {
                var evnt = new DeploymentRequested(Id, taskId, task.Request.NodeId, task.Request.Service, task.Request.Version, task.Request.Component, 0)
                    {
                        DeploymentId = deploymentId,
                        DeploymentUrl = deploymentUrl
                    };
                Apply(evnt);
            }
            else
            {
                TryComplete();
            }
        }

        private bool NoFailedStartsSoFar()
        {
            return !State.FailedDeployments.Any();
        }

        private void TryComplete()
        {
            if (!State.HaveAllTasksCompleted)
            {
                return;
            }
            if (State.FailedDeployments.Any())
            {
                var successfulDeployments = State.SuccessfulDeployments.Select(x => new Deployment(x.Request.Id, x.DeploymentId));
                var failedDeployments = State.FailedDeployments.Select(x => new Deployment(x.Request.Id, x.DeploymentId));

                Apply(new DeploymentSequenceFailed(Id, State.ParentProcessId, successfulDeployments, failedDeployments));
            }
            else
            {
                var successfulDeployments = State.SuccessfulDeployments.Select(x => new Deployment(x.Request.Id, x.DeploymentId));
                Apply(new DeploymentSequenceSucceeded(Id, State.ParentProcessId, successfulDeployments));
            }
        }
    }
}