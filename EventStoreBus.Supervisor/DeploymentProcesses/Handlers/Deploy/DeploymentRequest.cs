﻿using System;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    public class DeploymentRequest
    {
        private readonly string id;
        private readonly string nodeId;
        private readonly string service;
        private readonly string component;
        private readonly string version;

        public DeploymentRequest(string id, string nodeId, string service, string component, string version)
        {
            this.nodeId = nodeId;
            this.service = service;
            this.component = component;
            this.version = version;
            this.id = id;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string Service
        {
            get { return service; }
        }

        public string Component
        {
            get { return component; }
        }

        public string Version
        {
            get { return version; }
        }

        public string Id
        {
            get { return id; }
        }

        public DeploymentRequest SetNodeId(Action<string> setter)
        {
            setter(NodeId);
            return this;
        }

        public DeploymentRequest SetService(Action<string> setter)
        {
            setter(Service);
            return this;
        }

        public DeploymentRequest SetComponent(Action<string> setter)
        {
            setter(Component);
            return this;
        }

        public DeploymentRequest SetVersion(Action<string> setter)
        {
            setter(Version);
            return this;
        }
    }
}