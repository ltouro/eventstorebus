﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyDeploymentSucceeded : Command
    {
        [DataMember]
        public string ProcessId { get; set; }
        [DataMember]
        public string TaskId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }

        public NotifyDeploymentSucceeded(string commandId, string processId, string taskId, string deploymentId)
            : base(commandId)
        {
            ProcessId = processId;
            TaskId = taskId;
            DeploymentId = deploymentId;
        }

        public NotifyDeploymentSucceeded()
        {
        }
    }
}