﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyDeploymentFailed : Command
    {
        [DataMember]
        public string ProcessId { get; set; }
        [DataMember]
        public string TaskId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }
        [DataMember]
        public string DeploymentUrl { get; set; }

        public NotifyDeploymentFailed(string commandId, string processId, string taskId, string deploymentId, string deploymentUrl)
            : base(commandId)
        {
            ProcessId = processId;
            TaskId = taskId;
            DeploymentId = deploymentId;
            DeploymentUrl = deploymentUrl;
        }

        public NotifyDeploymentFailed()
        {
        }
    }
}