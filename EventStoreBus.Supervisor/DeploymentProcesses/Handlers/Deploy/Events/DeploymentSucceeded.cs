﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    [DataContract]
    [Version(1)]
    public class DeploymentSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        [DataMember]
        public string TaskId { get; set; }

        public DeploymentSucceeded(string processId, string deploymentId, string taskId)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
            TaskId = taskId;
        }

        public DeploymentSucceeded()
        {
        }
    }
}