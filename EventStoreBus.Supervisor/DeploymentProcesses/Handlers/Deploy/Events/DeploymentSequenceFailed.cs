﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    [DataContract]
    [Version(1)]
    public class DeploymentSequenceFailed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<Deployment> FailedDeployments { get; set; }

        [DataMember]
        public List<Deployment> SuccessfulDeployments { get; set; }

        public DeploymentSequenceFailed(string processId, string parentProcessId, IEnumerable<Deployment> successfulDeployments, IEnumerable<Deployment> failedDeployments)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
            FailedDeployments = failedDeployments.ToList();
            SuccessfulDeployments = successfulDeployments.ToList();
        }

        public DeploymentSequenceFailed()
        {
            FailedDeployments = new List<Deployment>();
            SuccessfulDeployments = new List<Deployment>();
        }
    }
}