﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    [DataContract]
    [Version(1)]
    public class DeploymentFailed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        [DataMember]
        public string DeploymentUrl { get; set; }

        [DataMember]
        public string TaskId { get; set; }

        [DataMember]
        public bool RetryPending { get; set; }

        public DeploymentFailed(string processId, string deploymentId, string taskId, bool retryPending)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
            TaskId = taskId;
            RetryPending = retryPending;
        }

        public DeploymentFailed()
        {
        }
    }
}