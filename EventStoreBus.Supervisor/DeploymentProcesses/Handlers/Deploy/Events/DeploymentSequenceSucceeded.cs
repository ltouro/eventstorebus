﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    [DataContract]
    [Version(1)]
    public class DeploymentSequenceSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<Deployment> SuccessfulDeployments { get; set; }

        public DeploymentSequenceSucceeded(string processId, string parentProcessId, IEnumerable<Deployment> successfulDeployments)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
            SuccessfulDeployments = successfulDeployments.ToList();
        }

        public DeploymentSequenceSucceeded()
        {
            SuccessfulDeployments = new List<Deployment>();
        }
    }
}