﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    public class DeployProcessState : AggregateState
    {
        private readonly Dictionary<string, DeploymentTask> tasks = new Dictionary<string, DeploymentTask>();

        public string ParentProcessId { get; private set; }

        public bool HaveAllTasksCompleted
        {
            get { return tasks.Values.All(x => x.Completed); }
        }

        public IList<DeploymentTask> FailedDeployments
        {
            get { return tasks.Values.Where(x => x.Completed && !x.Succeeded).ToList(); }
        }
        
        public IList<DeploymentTask> SuccessfulDeployments
        {
            get { return tasks.Values.Where(x => x.Completed && x.Succeeded).ToList(); }
        }

        public void When(DeploymentSequenceStarted evnt)
        {
            ParentProcessId = evnt.ParentProcessId;
        }

        public void When(DeploymentRequested evnt)
        {
            if (!tasks.ContainsKey(evnt.DeploymentId))
            {
                tasks[evnt.TaskId] = new DeploymentTask(new DeploymentRequest(evnt.TaskId, evnt.NodeId, evnt.Service, evnt.Component, evnt.Version), evnt.MaxAttempts);
            }
        }

        public void When(DeploymentSucceeded evnt)
        {
            tasks[evnt.TaskId].When(evnt);
        }

        public void When(DeploymentFailed evnt)
        {
            tasks[evnt.TaskId].When(evnt);
        }

        public DeploymentTask GetTaskById(string taskId)
        {
            return tasks[taskId];
        }
    }
}