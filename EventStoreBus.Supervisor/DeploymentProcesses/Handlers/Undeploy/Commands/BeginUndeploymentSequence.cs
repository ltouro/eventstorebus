﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy.Commands
{
    [DataContract]
    [Version(1)]
    public class BeginUndeploymentSequence : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<UndeploymentRequest> Requests { get; set; }

        public BeginUndeploymentSequence(string commandId, string parentProcessId, IEnumerable<UndeploymentRequest> requests)
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            Requests = requests.ToList();
        }

        public BeginUndeploymentSequence()
        {
        }
    }
}