﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    [DataContract]
    [Version(1)]
    public class UndeploymentSequenceStarted : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        public UndeploymentSequenceStarted(string processId, string parentProcessId)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
        }

        public UndeploymentSequenceStarted()
        {
        }
    }
}