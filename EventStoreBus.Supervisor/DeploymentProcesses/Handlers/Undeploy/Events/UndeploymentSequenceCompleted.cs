﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    [DataContract]
    [Version(1)]
    public class UndeploymentSequenceCompleted : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<string> SucceededUndeployments { get; set; }

        [DataMember]
        public List<string> FailedUndeployments { get; set; }

        public UndeploymentSequenceCompleted(string processId, string parentProcessId, IEnumerable<string> succeeded, IEnumerable<string> failed)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
            SucceededUndeployments = succeeded.ToList();
            FailedUndeployments = failed.ToList();
        }

        public UndeploymentSequenceCompleted()
        {
            SucceededUndeployments = new List<string>();
            FailedUndeployments = new List<string>();
        }
    }
}