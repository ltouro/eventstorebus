﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    public class UndeploymentProcess : Aggregate<UndeploymentProcessState>
    {
        private readonly int maxAttempts;

        public UndeploymentProcess(int maxAttempts)
        {
            this.maxAttempts = maxAttempts;
        }

        public UndeploymentProcess()
            : this(5)
        {
        }

        public void Start(string parentProcessId, IEnumerable<UndeploymentRequest> undeploymentTasks)
        {
            Apply(new UndeploymentSequenceStarted()
                {
                    ParentProcessId = parentProcessId,
                    ProcessId = Id
                });
            foreach (var undeploymentTask in undeploymentTasks)
            {
                Apply(new UndeploymentRequested()
                    {
                        NodeId = undeploymentTask.NodeId,
                        DeploymentId = undeploymentTask.DeploymentId,
                        ProcessId = Id,
                        MaxAttempts = maxAttempts
                    });
            }
        }

        public void HandleUndeploySuccess(string deploymentId)
        {
            Apply(new UndeploymentSucceeded()
            {
                DeploymentId = deploymentId,
                ProcessId = Id,
            });
            TryComplete();
        }

        public void HandleUndeployFailure(string deploymentId)
        {
            Apply(new UndeploymentFailed()
            {
                DeploymentId = deploymentId,
                ProcessId = Id,
            });
            var task = State.GetTaskByDeploymentId(deploymentId);
            if (task.CanBeRetried())
            {
                Apply(new UndeploymentRequested()
                {
                    NodeId = task.Request.NodeId,
                    DeploymentId = task.Request.DeploymentId,
                    ProcessId = Id,
                    MaxAttempts = maxAttempts
                });
            }
            else
            {
                TryComplete();
            }
        }

        private void TryComplete()
        {
            if (State.HaveAllUndeploymentsCompleted)
            {
                Apply(new UndeploymentSequenceCompleted(Id, State.ParentProcessId, State.SucceededUndeployments, State.FailedUndeployments));
            }
        }
    }
}