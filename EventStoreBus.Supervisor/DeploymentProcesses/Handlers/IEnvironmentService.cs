using System.Collections.Generic;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public interface IEnvironmentService
    {
        IEnumerable<ComponentDeployment> GetServiceDeploymentMap(string serviceName);
    }

    public class ComponentDeployment
    {
        public string NodeId { get; set; }
        public string DeploymentId { get; set; }
        public ComponentType ComponentType { get; set; }
        public string Component { get; set; }
        public string Version { get; set; }
    }

    public enum ComponentType
    {
        Receptor,
        CommandProcessor,
        EventProcessor,
        Endpoint
    }
}