﻿using System;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public class RedeploymentHandler
    {
        private readonly IRepository<RedeploymentProcess> processes;
        private readonly IUniqueIdGenerator uniqueIdGenerator;

        public RedeploymentHandler(IRepository<RedeploymentProcess> processes, IUniqueIdGenerator uniqueIdGenerator)
        {
            this.processes = processes;
            this.uniqueIdGenerator = uniqueIdGenerator;
        }

        public void When(Redeploy command)
        {
            processes.Invoke(command.Id, command.Id,
                                        process => process.Create(command.Service, command.Version, uniqueIdGenerator));
        }

        public void When(HandleLockSuccess command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleLockSuccess());
        }

        public void When(HandleLockFailure command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleLockFailure());
        }

        public void When(LoadPlan command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id, 
                process => process.LoadPlan(command.Plan));
        }
        
        public void When(HandleDeploySuccess command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process =>
                                         process.HandleDeploySuccess(command.Id, command.DeploymentId));
        }

        public void When(HandleDeployFailure command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleDeployFailure(command.DeploymentId));
        }

        public void When(HandleStartSuccess command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.HandleStartSuccess(command.Id, command.DeploymentId));
        }

        public void When(HandleStartFailure command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleStartFailure(command.DeploymentId));
        }

        public void When(HandleStopSuccess command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process =>
                                         process.HandleStopSuccess(command.Id, command.DeploymentId));
        }

        public void When(HandleStopFailure command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleStopFailure(command.DeploymentId));
        }

        public void When(HandleUndeploySuccess command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process =>
                                         process.HandleUndeploySuccess(command.Id, command.DeploymentId));
        }

        public void When(HandleUndeployFailure command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleUndeployFailure(command.DeploymentId));
        }

        public class Factory : IAggregateComponentFactory
        {
            public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
            {
                return
                    new RedeploymentHandler(
                        repositories.Get<RedeploymentProcess, DefaultAggregateStorage<RedeploymentProcess>>(),
                        DefaultUniqueIdGenerator.Instance);
            }

            public void Release(object component)
            {
            }
        }
    }
}