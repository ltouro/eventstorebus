﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands
{
    [DataContract]
    public class DeploymentPlan
    {
        [DataMember]
        public string ServiceVersion { get; set; }

        [DataMember]
        public List<PlannedDeployment> ToDeploy { get; set; }

        [DataMember]
        public List<PlannedUndeployment> ToUndeploy { get; set; }

        public DeploymentPlan()
        {
            ToDeploy = new List<PlannedDeployment>();
            ToUndeploy = new List<PlannedUndeployment>();
        }
    }
}