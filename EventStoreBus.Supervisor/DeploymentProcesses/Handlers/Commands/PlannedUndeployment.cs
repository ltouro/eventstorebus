﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands
{
    [DataContract]
    public class PlannedUndeployment
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string ExistingDeploymentId { get; set; }
        [DataMember]
        public string Component { get; set; }
    }
}