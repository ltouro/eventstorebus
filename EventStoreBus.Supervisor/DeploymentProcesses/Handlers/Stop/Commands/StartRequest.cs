﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop.Commands
{
    [DataContract]
    public class StopRequest
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }

        public StopRequest(string nodeId, string deploymentId)
        {
            NodeId = nodeId;
            DeploymentId = deploymentId;
        }

        public StopRequest()
        {
        }
    }
}