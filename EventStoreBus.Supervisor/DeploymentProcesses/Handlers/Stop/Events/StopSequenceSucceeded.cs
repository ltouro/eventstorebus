﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    [DataContract]
    [Version(1)]
    public class StopSequenceSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        public StopSequenceSucceeded(string processId, string parentProcessId)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
        }

        public StopSequenceSucceeded()
        {
        }
    }
}