﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    [Version(1)]
    [DataContract]
    public class StopRequested : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }
        
        [DataMember]
        public string DeploymentId { get; set; } 
        
        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public int MaxAttempts { get; set; }

        public StopRequested(string processId, string deploymentId, string nodeId, int maxAttempts)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
            NodeId = nodeId;
            MaxAttempts = maxAttempts;
        }

        public StopRequested()
        {
        }
    }
}