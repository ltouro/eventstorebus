﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    [DataContract]
    [Version(1)]
    public class StopSequenceFailed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<string> FailedRequests { get; set; }

        public StopSequenceFailed(string processId, string parentProcessId, IEnumerable<string> failedRequests)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
            FailedRequests = failedRequests.ToList();
        }

        public StopSequenceFailed()
        {
            FailedRequests = new List<string>();
        }
    }
}