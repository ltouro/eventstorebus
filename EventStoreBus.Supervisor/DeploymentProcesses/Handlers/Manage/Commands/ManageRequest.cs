﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Manage.Commands
{
    [DataContract]
    [KnownType(typeof(string))]
    [KnownType(typeof(int))]
    [KnownType(typeof(bool))]
    public class ManageRequest
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }
        [DataMember]
        public string Attribute { get; set; }
        [DataMember]
        public object RequestedValue { get; set; }

        public ManageRequest(string id, string nodeId, string deploymentId, string attribute, object requestedValue)
        {
            Id = id;
            NodeId = nodeId;
            DeploymentId = deploymentId;
            Attribute = attribute;
            RequestedValue = requestedValue;
        }

        public ManageRequest()
        {
        }
    }
}