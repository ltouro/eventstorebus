﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Manage.Commands
{
    [DataContract]
    [Version(1)]
    public class BeginManagementSequence : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<ManageRequest> Requests { get; set; }        

        public BeginManagementSequence(string commandId, string parentProcessId, IEnumerable<ManageRequest> requests) 
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            Requests = requests.ToList();
        }

        public BeginManagementSequence()
        {
            Requests = new List<ManageRequest>();
        }
    }
}