﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    public class StartProcessState : AggregateState
    {
        private readonly Dictionary<string, StartTask> tasks = new Dictionary<string, StartTask>();

        public string ParentProcessId { get; private set; }

        public bool HaveAllTasksCompleted
        {
            get { return tasks.Values.All(x => x.Completed); }
        }

        public IList<string> FailedStarts
        {
            get { return tasks.Values.Where(x => x.Completed && !x.Succeeded).Select(x => x.Request.DeploymentId).ToList(); }
        }

        public StartTask GetTaskByDeploymentId(string taskId)
        {
            return tasks[taskId];
        }

        public void When(StartSequenceStarted evnt)
        {
            ParentProcessId = evnt.ParentProcessId;
        }

        public void When(StartRequested evnt)
        {
            if (!tasks.ContainsKey(evnt.DeploymentId))
            {
                tasks[evnt.DeploymentId] = new StartTask(new StartRequest(evnt.NodeId, evnt.DeploymentId), evnt.MaxAttempts);
            }
        }

        public void When(StartSucceeded evnt)
        {
            tasks[evnt.DeploymentId].When(evnt);
        }
        
        public void When(StartFailed evnt)
        {
            tasks[evnt.DeploymentId].When(evnt);
        }
    }
}