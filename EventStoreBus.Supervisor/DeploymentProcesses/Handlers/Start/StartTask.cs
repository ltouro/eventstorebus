﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    public class StartTask
    {
        private readonly int maxAttempts;
        private readonly StartRequest request;
        private int failedAttempts;
        private bool succeeded;
        private bool completed;

        public StartTask(StartRequest request, int maxAttempts)
        {
            this.request = request;
            this.maxAttempts = maxAttempts;
        }

        public bool Succeeded
        {
            get { return succeeded; }
        }

        public StartRequest Request
        {
            get { return request; }
        }

        public bool Completed
        {
            get { return completed; }
        }

        public bool CanBeRetried()
        {
            return failedAttempts + 1 < maxAttempts;
        }

        public void When(StartSucceeded evnt)
        {
            succeeded = true;
            completed = true;
        }

        public void When(StartFailed evnt)
        {
            failedAttempts++;
            if (!evnt.RetryPending)
            {
                completed = true;
            }
        }
    }
}