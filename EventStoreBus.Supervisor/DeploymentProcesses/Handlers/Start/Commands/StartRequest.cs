﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start.Commands
{
    [DataContract]
    public class StartRequest
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }

        public StartRequest(string nodeId, string deploymentId)
        {
            NodeId = nodeId;
            DeploymentId = deploymentId;
        }

        public StartRequest()
        {
        }
    }
}