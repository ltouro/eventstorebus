﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    [DataContract]
    [Version(1)]
    public class StartSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        public StartSucceeded(string processId, string deploymentId)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
        }

        public StartSucceeded()
        {
        }
    }
}