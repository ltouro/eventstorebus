﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    [DataContract]
    [Version(1)]
    public class StartSequenceSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        public StartSequenceSucceeded(string processId, string parentProcessId)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
        }

        public StartSequenceSucceeded()
        {
        }
    }
}