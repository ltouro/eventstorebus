﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    public class StartProcess : Aggregate<StartProcessState>
    {
        private readonly int maxAttempts;

        public StartProcess(int maxAttempts)
        {
            this.maxAttempts = maxAttempts;
        }

        public StartProcess()
            : this(5)
        {
        }

        public void Start(string parentProcessId, IEnumerable<StartRequest> startRequests)
        {
            Apply(new StartSequenceStarted(Id, parentProcessId));
            foreach (var request in startRequests)
            {
                Apply(new StartRequested(Id, request.DeploymentId, request.NodeId, maxAttempts));
            }
        }

        public void HandleStartSuccess(string deploymentId)
        {
            Apply(new StartSucceeded(Id, deploymentId));
            TryComplete();
        }

        public void HandleStartFailure(string deploymentId)
        {
            var task = State.GetTaskByDeploymentId(deploymentId);
            var retry = NoFailedStartsSoFar() && task.CanBeRetried();

            Apply(new StartFailed(Id, deploymentId, retry));

            if (retry)
            {
                Apply(new StartRequested(Id, task.Request.DeploymentId, task.Request.NodeId, maxAttempts));
            }
            else
            {
                TryComplete();
            }
        }

        private bool NoFailedStartsSoFar()
        {
            return !State.FailedStarts.Any();
        }

        private void TryComplete()
        {
            if (!State.HaveAllTasksCompleted)
            {
                return;
            }
            var failures = State.FailedStarts;
            if (failures.Any())
            {
                Apply(new StartSequenceFailed(Id, State.ParentProcessId, failures));
            }
            else
            {
                Apply(new StartSequenceSucceeded(Id, State.ParentProcessId));
            }
        }
    }
}