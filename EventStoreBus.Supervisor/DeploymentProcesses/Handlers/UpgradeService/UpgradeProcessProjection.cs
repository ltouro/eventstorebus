﻿using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcessViewProjection
    {
        private readonly IViewWriter<UpgradeProcessView> viewWriter;

        public UpgradeProcessViewProjection(IViewWriter<UpgradeProcessView> viewWriter)
        {
            this.viewWriter = viewWriter;
        }

        public void When(Envelope<Events.ServiceDeploymentStateCaptured> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(evnt.Payload.ProcessId, evnt,
                               view =>
                                   {
                                       foreach (var point in evnt.Payload.DeploymentPoints)
                                       {
                                           view.AddTask(point.Id, point.NodeId, point.DeploymentId, point.ComponentType, point.Component);
                                       }
                                   });
            }
        }

        public void When(Envelope<Events.FullDeploymentSucceeded> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(evnt.Payload.ProcessId, evnt,
                               view =>
                                   {
                                       foreach (var confirmation in evnt.Payload.Confirmations)
                                       {
                                           view.ConfirmNewVersionDeployed(confirmation.RequestId, confirmation.DeploymentId);
                                       }
                                   });
            }
        }

        public void When(Envelope<Events.EventProcessorsDeployed> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Update(evnt.Payload.ProcessId, evnt,
                               view =>
                                   {
                                       foreach (var confirmation in evnt.Payload.Confirmations)
                                       {
                                           view.ConfirmNewVersionDeployed(confirmation.RequestId, confirmation.DeploymentId);
                                       }
                                   });
            }
        }
    }
}