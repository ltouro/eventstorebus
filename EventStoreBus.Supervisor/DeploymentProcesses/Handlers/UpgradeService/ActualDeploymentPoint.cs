﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class ActualDeploymentPoint
    {
        private readonly string nodeId;
        private readonly string deploymentId;
        private readonly string component;
        private readonly ComponentType componentType;

        public ActualDeploymentPoint(string nodeId, string deploymentId, string component, ComponentType componentType)
        {
            this.nodeId = nodeId;
            this.deploymentId = deploymentId;
            this.component = component;
            this.componentType = componentType;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }

        public string Component
        {
            get { return component; }
        }

        public ComponentType ComponentType
        {
            get { return componentType; }
        }
    }
}