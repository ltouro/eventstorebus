﻿using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcessState : AggregateState
    {
        public bool HaveEventProcessorsBeenDeployed { get; private set; }
        public bool HaveOldEventProcessorsBeenStopped { get; private set; }
        public bool HaveViewsBeenRebuilt { get; private set; }
        public bool HaveOldReceptorsBeenStopped { get; private set; }
        public bool HaveEventProcessorsBeenStarted { get; private set; }
        public bool HaveReceptorsBeenStarted { get; private set; }
        public bool HaveGatewaysBeenDisabled { get; private set; }

        public string Service { get; private set; }
        public string BaseVersion { get; private set; }
        public string TargetVersion { get; private set; }

        public void When(ServiceUpgradeStarted evnt)
        {
            Service = evnt.Service;
            TargetVersion = evnt.TargetVersion;
        }

        public void When(ServiceDeploymentStateCaptured evnt)
        {
            BaseVersion = evnt.Version;
        }

        public void When(ViewsRebuilt evnt)
        {
            HaveViewsBeenRebuilt = true;
        }

        public void When(EventProcessorsDeployed evnt)
        {
            HaveEventProcessorsBeenDeployed = true;
        }

        public void When(OldEventProcessorsStopped evnt)
        {
            HaveOldEventProcessorsBeenStopped = true;
        }

        public void When(OldReceptorsStopped evnt)
        {
            HaveOldReceptorsBeenStopped = true;
        }

        public void When(EventProcessorsStarted evnt)
        {
            HaveEventProcessorsBeenStarted = true;
        }
        
        public void When(ReceptorsStarted evnt)
        {
            HaveReceptorsBeenStarted = true;
        }

        public void When(GatewaysDisabled evnt)
        {
            HaveGatewaysBeenDisabled = true;
        }
    }
}