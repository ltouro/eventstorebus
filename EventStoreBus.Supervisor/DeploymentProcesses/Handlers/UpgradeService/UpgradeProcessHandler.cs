﻿using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcessHandler
    {
        private readonly IRepository<UpgradeProcess> repository;

        public UpgradeProcessHandler(IRepository<UpgradeProcess> repository)
        {
            this.repository = repository;
        }

        public void When(UpgradeService command)
        {
            repository.InvokeEnsuringNew(command.Id, command.Id, 
                process => process.Start(command.Service, command.TargetVersion));
        }

        public void When(NotifyServiceLocked command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnLocked(command.LockId, command.CurrentVersion, null /*TODO*/, DefaultUniqueIdGenerator.Instance));
        }

        public void When(NotifyDeploymentSucceeded command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnDeploymentSuccess(command.PositiveConfirmations));
        }

        public void When(NotifyManagementTaskSucceeded command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnManagementTaskSucceeded());
        }

        public void When(NotifyStartSucceeded command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnStartSuccess());
        }

        public void When(NotifyWaitCompleted command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnWaitCompleted());
        }

        public void When(NotifyStopSucceeded command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnStopSuccess());
        }
        
        public void When(NotifyEndpointConfigured command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnRouterConfigured());
        }
        
        public void When(NotifyUndeploymentSucceeded command)
        {
            repository.InvokeEnsuringExists(command.ProcessId, command.Id,
                process => process.OnUndeploymentSuccess());
        }
    }
}