﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter.Events;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Manage.Events;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Wait.Events;
using EventStoreBus.Views.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcessReceptor
    {
        private readonly ICommandSender commandSender;
        private readonly IViewReader<UpgradeProcessView> viewReader; 

        public UpgradeProcessReceptor(ICommandSender commandSender, IViewReader<UpgradeProcessView> viewReader)
        {
            this.commandSender = commandSender;
            this.viewReader = viewReader;
        }

        public void When(LockService.Locked evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyServiceLocked(eventId.ToString(), evnt.ParentProcessId, evnt.LockId, evnt.Version);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(DeploymentSequenceSucceeded evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var confirmations = evnt.SuccessfulDeployments.Select(x => new DeploymentConfirmation(x.TaskId, x.Id));
                var command = new NotifyDeploymentSucceeded(eventId.ToString(), evnt.ParentProcessId, confirmations);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(ManagementSequenceSucceeded evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyManagementTaskSucceeded(eventId.ToString(), evnt.ParentProcessId);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(StartSequenceSucceeded evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyStartSucceeded(eventId.ToString(), evnt.ParentProcessId);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(WaitCompleted evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyWaitCompleted(eventId.ToString(), evnt.ParentProcessId);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(StopSequenceSucceeded evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyStopSucceeded(eventId.ToString(), evnt.ParentProcessId);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(EndpointConfigured evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyEndpointConfigured(eventId.ToString(), evnt.ParentProcessId);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        public void When(Undeploy.UndeploymentSucceeded evnt, Guid eventId)
        {
            if (Recognize(evnt.ParentProcessId))
            {
                var command = new NotifyUndeploymentSucceeded(eventId.ToString(), evnt.ParentProcessId);
                commandSender.SendTo<UpgradeProcessHandler>(command);
            }
        }

        private bool Recognize(string processId)
        {
            var session = viewReader.OpenSession();
            var view = session.Load(processId);
            return view != null;
        }
    }
}