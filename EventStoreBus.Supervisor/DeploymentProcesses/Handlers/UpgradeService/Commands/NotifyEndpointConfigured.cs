﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyEndpointConfigured : Command
    {
        [DataMember]
        public string ProcessId { get; set; }

        public NotifyEndpointConfigured(string commandId, string processId) : base(commandId)
        {
            ProcessId = processId;
        }

        public NotifyEndpointConfigured()
        {
        }
    }
}