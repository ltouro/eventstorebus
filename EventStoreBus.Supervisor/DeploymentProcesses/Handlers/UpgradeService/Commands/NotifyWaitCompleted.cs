﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyWaitCompleted : Command
    {
        [DataMember]
        public string ProcessId { get; set; }

        public NotifyWaitCompleted(string commandId, string processId) : base(commandId)
        {
            ProcessId = processId;
        }

        public NotifyWaitCompleted()
        {
        }
    }
}