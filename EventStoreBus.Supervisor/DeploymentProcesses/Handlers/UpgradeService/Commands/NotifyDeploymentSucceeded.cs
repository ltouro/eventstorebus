﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyDeploymentSucceeded : Command
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public List<DeploymentConfirmation> PositiveConfirmations { get; set; }

        public NotifyDeploymentSucceeded(string commandId, string processId, IEnumerable<DeploymentConfirmation> positiveConfirmations)
            : base(commandId)
        {
            ProcessId = processId;
            PositiveConfirmations = positiveConfirmations.ToList();
        }

        public NotifyDeploymentSucceeded()
        {
            PositiveConfirmations = new List<DeploymentConfirmation>();
        }
    }
}