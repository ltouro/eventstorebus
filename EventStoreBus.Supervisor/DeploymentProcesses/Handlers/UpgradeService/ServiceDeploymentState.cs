﻿using System.Collections.Generic;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class ServiceDeploymentState
    {
        private readonly List<ActualDeploymentPoint> actualPoints;
        private readonly List<PlannedDeploymentPoint> plannedPoints;

        public ServiceDeploymentState(IEnumerable<ActualDeploymentPoint> actualPoints, IEnumerable<PlannedDeploymentPoint> plannedPoints)
        {
            this.actualPoints = actualPoints.ToList();
            this.plannedPoints = plannedPoints.ToList();
        }

        public IEnumerable<ActualDeploymentPoint> ActualPoints
        {
            get { return actualPoints; }
        }

        public IEnumerable<PlannedDeploymentPoint> PlannedPoints
        {
            get { return plannedPoints; }
        }
    }
}