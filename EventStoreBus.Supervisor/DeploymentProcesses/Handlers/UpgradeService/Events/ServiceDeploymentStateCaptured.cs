using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class ServiceDeploymentStateCaptured : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public List<DeploymentPoint> DeploymentPoints { get; set; }

        public ServiceDeploymentStateCaptured(string processId, string version, IEnumerable<DeploymentPoint> deploymentPoints)
        {
            ProcessId = processId;
            Version = version;
            DeploymentPoints = deploymentPoints.ToList();
        }

        public ServiceDeploymentStateCaptured()
        {
        }
    }
}