﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class GatewaysDisabled : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public GatewaysDisabled(string processId)
        {
            ProcessId = processId;
        }

        public GatewaysDisabled()
        {
        }
    }
}