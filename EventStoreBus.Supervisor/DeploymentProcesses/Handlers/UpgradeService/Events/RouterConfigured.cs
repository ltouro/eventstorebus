﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    public class RouterConfigured : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        public RouterConfigured(string processId)
        {
            ProcessId = processId;
        }

        public RouterConfigured()
        {
        }
    }
}