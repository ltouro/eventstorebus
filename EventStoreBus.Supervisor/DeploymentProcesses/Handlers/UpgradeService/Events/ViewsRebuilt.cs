﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class ViewsRebuilt : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public ViewsRebuilt(string processId)
        {
            ProcessId = processId;
        }

        public ViewsRebuilt()
        {
        }
    }
}