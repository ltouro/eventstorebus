﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class OldEventProcessorsStopped : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public OldEventProcessorsStopped(string processId)
        {
            ProcessId = processId;
        }

        public OldEventProcessorsStopped()
        {
        }
    }
}