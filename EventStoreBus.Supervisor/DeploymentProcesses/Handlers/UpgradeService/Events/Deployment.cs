﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    public class Deployment
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }

        public Deployment(string nodeId, string deploymentId)
        {
            NodeId = nodeId;
            DeploymentId = deploymentId;
        }

        public Deployment()
        {
        }
    }
}