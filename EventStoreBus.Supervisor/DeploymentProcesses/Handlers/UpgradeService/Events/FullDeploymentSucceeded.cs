using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class FullDeploymentSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        public List<DeploymentConfirmation> Confirmations { get; set; }

        public FullDeploymentSucceeded(string processId, IEnumerable<DeploymentConfirmation> confirmations)
        {
            ProcessId = processId;
            Confirmations = confirmations.ToList();
        }

        public FullDeploymentSucceeded()
        {
        }
    }
}