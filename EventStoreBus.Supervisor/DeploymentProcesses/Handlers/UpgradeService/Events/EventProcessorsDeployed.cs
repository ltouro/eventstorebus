using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class EventProcessorsDeployed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        public List<DeploymentConfirmation> Confirmations { get; set; }

        public EventProcessorsDeployed(string processId, IEnumerable<DeploymentConfirmation> confirmations)
        {
            ProcessId = processId;
            Confirmations = confirmations.ToList();
        }

        public EventProcessorsDeployed()
        {
        }
    }
}