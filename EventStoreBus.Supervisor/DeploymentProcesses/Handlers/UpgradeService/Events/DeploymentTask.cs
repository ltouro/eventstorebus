using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    public class DeploymentTask
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Component { get; set; }
        [DataMember]
        public string Version { get; set; }
    }
}