﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class ServiceUpgradeSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string PreviousVersion { get; set; }

        [DataMember]
        public string CurrentVersion { get; set; }

        public ServiceUpgradeSucceeded(string processId, string service, string previousVersion, string currentVersion)
        {
            ProcessId = processId;
            Service = service;
            PreviousVersion = previousVersion;
            CurrentVersion = currentVersion;
        }
    }
}