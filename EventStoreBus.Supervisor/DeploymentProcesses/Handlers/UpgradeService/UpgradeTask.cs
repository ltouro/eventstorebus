﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeTask
    {
        private readonly string id;
        private readonly string nodeId;
        private readonly string component;
        private readonly string oldDeploymentId;
        private string newDeploymentId;
        private readonly ComponentType componentType;

        public UpgradeTask(string id, string nodeId, string component, ComponentType componentType, string oldDeploymentId)
        {
            this.nodeId = nodeId;
            this.component = component;
            this.componentType = componentType;
            this.oldDeploymentId = oldDeploymentId;
            this.id = id;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string Component
        {
            get { return component; }
        }

        public ComponentType ComponentType
        {
            get { return componentType; }
        }

        public string OldDeploymentId
        {
            get { return oldDeploymentId; }
        }

        public string Id
        {
            get { return id; }
        }

        public string NewDeploymentId
        {
            get { return newDeploymentId; }
        }

        public void NewVersionDeployed(string deploymentId)
        {
            newDeploymentId = deploymentId;
        }
    }
}