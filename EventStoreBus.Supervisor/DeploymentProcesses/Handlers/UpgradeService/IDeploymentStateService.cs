﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public interface IDeploymentStateService
    {
        ServiceDeploymentState CaptureState(string service);
    }
}