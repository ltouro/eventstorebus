﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    /*
     * Deploy new event processors side by side                         - Undeploy
             * Disable gateways in all new event processors                     -
             * Start new event processors                                       - Stop
             * Wait till they are finished (up to date) (poll)                  -
             * Deploy all other (non-processors) components side by side        - Undeploy
             * Stop old event processors                                        - Start
             * Stop old receptors                                               - Start
             * Switch endpoints (in load balancer)                              - Switch back       - some new commands
             * Wait for old command processors to process all commands (poll)   - 
             * Stop old command handlers                                        - Start  
             * Enable gateways in new event processors                          -             * - 
             * Start new receptors                                              - Stop              - some new commands
             * Start new command processors                                     - Stop              - some new events       - no automatic rollback
             * Undeploy all old components
     * 
     */
    public enum UpgradeProcessStage
    {
        NewProcessorsDeployed,
        GatewaysDisabled,
        NewEventProcessorsStarted,
        FullDeploymentSucceeded,
        OldEventProcessorsStopped,
        OldReceptorsStopped,
        RouterConfigured,
        OldCommandHandlersStopped,
        NewReceptorsStarted,
        NewCommandProcessorsStarted
    }
}