﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Views
{
    public class CorrelationViewProjection
    {
        private readonly IViewWriter<CorrelationView> viewWriter;

        public CorrelationViewProjection(IViewWriter<CorrelationView> viewWriter)
        {
            this.viewWriter = viewWriter;
        }

        public void When(Envelope<DeploymentRequested> evnt)
        {
            CreateCorrelationView(evnt.EventId.ToString(), evnt.Payload.ProcessId, evnt);
        }

        private void CreateCorrelationView(string operationId, string processId, IEnvelope envelope)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Create(operationId, envelope, new CorrelationView()
                                                                  {
                                                                      CorrelationId = operationId,
                                                                      ProcessId = processId
                                                                  });
            }
        }

        public void When(Envelope<DeploymentSucceeded> evnt)
        {
            DeleteCorrelationView(evnt.Payload.OperationId, evnt);
        }

        private void DeleteCorrelationView(string operationId, IEnvelope envelope)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.Delete(operationId, envelope);
            }
        }

        public void When(Envelope<StartRequested> evnt)
        {
            CreateCorrelationView(evnt.EventId.ToString(), evnt.Payload.ProcessId, evnt);
        }

        public void When(Envelope<StartSucceeded> evnt)
        {
            DeleteCorrelationView(evnt.Payload.OperationId, evnt);
        }
        
        public void When(Envelope<StopRequested> evnt)
        {
            CreateCorrelationView(evnt.EventId.ToString(), evnt.Payload.ProcessId, evnt);
        }

        public void When(Envelope<StopSucceeded> evnt)
        {
            DeleteCorrelationView(evnt.Payload.OperationId, evnt);
        }
        
        public void When(Envelope<UndeploymentRequested> evnt)
        {
            CreateCorrelationView(evnt.EventId.ToString(), evnt.Payload.ProcessId, evnt);
        }

        public void When(Envelope<UndeploymentSucceeded> evnt)
        {
            DeleteCorrelationView(evnt.Payload.OperationId, evnt);
        }

        public void When(Envelope<LockRequested> evnt)
        {
            CreateCorrelationView(evnt.Payload.LockId, evnt.Payload.ProcessId, evnt);
        }

        public void When(Envelope<Locked> evnt)
        {
            DeleteCorrelationView(evnt.Payload.LockId, evnt);
        }

        public class Storage : IViewStorage<CorrelationView>
        {
            public string CollectionName
            {
                get { return "redeploymentProcessCorrelation"; }
            }
        }

        public class Factory : IViewProjectionFactory
        {
            public object Create(Type viewProjectionImplementation, IViewWriters viewWriters)
            {
                return new CorrelationViewProjection(viewWriters.GetWriter<CorrelationView, Storage>());
            }

            public void Release(object projection)
            {
            }
        }
    }
}