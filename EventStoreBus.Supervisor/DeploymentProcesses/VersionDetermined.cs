﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [DataContract]
    [Version(1)]
    public class VersionDetermined : IEvent
    {
        [DataMember]
        public string Version { get; set; }
    }
}