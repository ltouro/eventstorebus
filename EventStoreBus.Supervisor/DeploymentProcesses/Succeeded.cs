﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [Version(1)]
    [DataContract]
    public class Succeeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; } 
        
        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Service { get; set; }
    }
}