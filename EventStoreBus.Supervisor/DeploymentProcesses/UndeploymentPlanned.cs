﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [DataContract]
    [Version(1)]
    public class UndeploymentPlanned : IEvent
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string Component { get; set; } 
        [DataMember]
        public string ExistingDeploymentId { get; set; }
    }
}