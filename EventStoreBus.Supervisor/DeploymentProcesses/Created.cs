﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [DataContract]
    [Version(1)]
    public class Created : IEvent
    {
        [DataMember]
        public string Service { get; set; } 
        
        [DataMember(IsRequired = false)]
        public string RequestedVersion { get; set; }
    }
}