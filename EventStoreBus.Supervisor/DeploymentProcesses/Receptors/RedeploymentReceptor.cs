﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Views;
using EventStoreBus.Supervisor.Nodes;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Receptors
{
    public class RedeploymentReceptor
    {
        private readonly IViewReader<CorrelationView> viewReader;
        private readonly ICommandSender commandSender;

        public RedeploymentReceptor(ICommandSender commandSender, IViewReader<CorrelationView> viewReader)
        {
            this.viewReader = viewReader;
            this.commandSender = commandSender;
        }

        public void When(ComponentDeployed evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleDeploySuccess
                                                                {
                                                                    DeploymentId = evnt.DeploymentId,
                                                                    OperationId = evnt.OperationId,
                                                                    ProcessId = processId,
                                                                    Id = evnt.OperationId
                                                                });
        }

        

        public void When(ComponentDeploymentFailed evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleDeployFailure()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        }
        
        public void When(ComponentStarted evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleStartSuccess()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        }
        
        public void When(ComponentStartFailed evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleStartFailure()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        }
        
        public void When(ComponentStopped evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleStopSuccess()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        } 
        
        public void When(ComponentStopFailed evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleStartFailure()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        }
        
        public void When(ComponentUndeployed evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleUndeploySuccess()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        }
        
        public void When(ComponentUndeploymentFailed evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.OperationId,
                                               processId => new HandleUndeployFailure()
                                               {
                                                   DeploymentId = evnt.DeploymentId,
                                                   ProcessId = processId,
                                                   Id = evnt.OperationId
                                               });
        }

        public void When(Services.Locked evnt, Guid eventId)
        {
            SendIfRelated<RedeploymentHandler>(evnt.LockId,
                                               processId => new HandleLockSuccess()
                                                                {
                                                                    ProcessId = processId,
                                                                    Id = eventId.ToString()
                                                                });
        }

        private void SendIfRelated<T>(string operationId, Func<string, ICommand> commandCreator)
        {
            CorrelationView correlationView;
            using (var session = viewReader.OpenSession())
            {
                correlationView = session.Load(operationId);
            }
            if (correlationView != null)
            {
                commandSender.SendTo<T>(commandCreator(correlationView.ProcessId));
            }
        }

        public class Factory : IReceptorFactory
        {
            public object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders)
            {
                return new RedeploymentReceptor(commandSender,
                                                viewReaders.GetReader<CorrelationView, CorrelationViewProjection.Storage>());
            }

            public void Release(object receptor)
            {
            }
        }
    }
}