﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes
{
    [DataContract]
    [Version(1)]
    public class NodeRemoved : IEvent
    {
        [DataMember]
        public string NodeId { get; set; }
    }
}