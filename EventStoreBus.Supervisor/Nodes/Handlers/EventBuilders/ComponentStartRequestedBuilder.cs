﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentStartRequestedBuilder
    {
        public static ComponentStartRequested Build(string nodeId, string operationId, NodeState nodeState, Deployment deployment)
        {
            return new ComponentStartRequested()
                       {
                           OperationId = operationId,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId,
                           NodeUrl = nodeState.ListenUrl
                       };
        }
    }
}