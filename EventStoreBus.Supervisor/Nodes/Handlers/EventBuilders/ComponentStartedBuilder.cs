﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentStartedBuilder
    {
        public static ComponentStarted Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentStarted()
                       {
                           OperationId = operationId,
                           Component = deployment.Component,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId,
                           Service = deployment.Service,
                           Version = deployment.Version
                       };
        }
    }
}