﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentStopFailedBuilder
    {
        public static ComponentStopFailed Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentStopFailed()
                       {
                           OperationId = operationId,
                           NodeId = nodeId,
                           DeploymentId = deployment.Id
                       };
        }
    }
}