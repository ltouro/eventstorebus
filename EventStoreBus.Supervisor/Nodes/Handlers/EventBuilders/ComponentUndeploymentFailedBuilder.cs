﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentUndeploymentFailedBuilder
    {
        public static ComponentUndeploymentFailed Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentUndeploymentFailed()
                       {
                           OperationId = operationId,
                           NodeId = nodeId,
                           DeploymentId = deployment.Id
                       };
        }
    }
}