﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentStopRequestedBuilder
    {
        public static ComponentStopRequested Build(string nodeId, string operationId, NodeState nodeState, Deployment deployment)
        {
            return new ComponentStopRequested()
                       {
                           OperationId = operationId,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId,
                           NodeUrl = nodeState.ListenUrl
                       };
        }
    }
}