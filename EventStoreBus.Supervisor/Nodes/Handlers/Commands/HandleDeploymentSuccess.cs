﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class HandleDeploymentSuccess : Command
    {
        [DataMember]
        public string DeploymentId { get; set; }
        
        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string OperationId { get; set; }

        [DataMember]
        public string WSManagementUrl { get; set; }

        [DataMember]
        public string WebManagementUrl { get; set; }
    }
}