﻿using System.Runtime.Serialization;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Gateway;

namespace EventStoreBus.Supervisor.Nodes.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class HandleOperationResult : Command
    {
        [DataMember]
        public string DeploymentId { get; set; }
        
        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public string OperationId { get; set; }
    }
}