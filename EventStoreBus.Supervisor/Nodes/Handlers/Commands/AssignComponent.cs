﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class AssignComponent : Command
    {
        [DataMember]
        public string NodeId { get; set; }
        
        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Component { get; set; }
    }
}