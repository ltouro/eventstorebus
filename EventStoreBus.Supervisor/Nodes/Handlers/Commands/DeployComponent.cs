﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class DeployComponent : Command
    {
        [DataMember]
        public string NodeId { get; set; }
        
        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Component { get; set; }

        [DataMember(IsRequired = false)]
        public string DeploymentId { get; set; }

        [DataMember(IsRequired = false)]
        public string DeploymentUrl { get; set; }
    }
}