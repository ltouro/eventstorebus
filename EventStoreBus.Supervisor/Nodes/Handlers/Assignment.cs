﻿using System;

namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class Assignment
    {
        private readonly string id;
        private readonly string service;
        private readonly string component;

        public Assignment(string id, string service, string component)
        {
            this.id = id;
            this.service = service;
            this.component = component;
        }

        public string Component
        {
            get { return component; }
        }

        public string Service
        {
            get { return service; }
        }

        public string Id
        {
            get { return id; }
        }
    }
}