﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class NodeState : AggregateState
    {
        public string ListenUrl { get; private set; }
        public bool Active { get; private set; }

        private readonly List<Deployment> deployments = new List<Deployment>();
        public IEnumerable<Deployment> Deployments
        {
            get { return deployments; }
        }

        private readonly List<Assignment> assignments = new List<Assignment>();

        public Assignment GetFirstAssignment(string service, string component)
        {
            return assignments.FirstOrDefault(x => x.Service == service && x.Component == component);
        }
        
        public Assignment GetAssignment(string id)
        {
            return assignments.FirstOrDefault(x => x.Id == id);
        }

        public void When(NodeAdded evnt)
        {
            ListenUrl = evnt.ListenUrl;
            Active = true;
        }

        public void When(NodeRemoved evnt)
        {
            Active = false;
        }

        public void When(ComponentAssigned evnt)
        {
            assignments.Add(new Assignment(evnt.AssignmentId, evnt.Service, evnt.Component));
        }

        public void When(ComponentUnassigned evnt)
        {
            assignments.RemoveAll(x => x.Id == evnt.AssignmentId);
        }
        
        public void When(ComponentDeploymentRequested evnt)
        {
            var deployment = new Deployment(evnt.DeploymentId, evnt.DeploymentUrl, evnt.OperationId, evnt.Service, evnt.Component, evnt.Version);
            deployments.Add(deployment);
        }

        public void When(ComponentDeployed evnt)
        {
            deployments.First(x => x.Id == evnt.DeploymentId).On(evnt);
        } 
        
        public void When(ComponentStartRequested evnt)
        {
            deployments.First(x => x.Id == evnt.DeploymentId).On(evnt);
        }
        
        public void When(ComponentStarted evnt)
        {
            deployments.First(x => x.Id == evnt.DeploymentId).On(evnt);
        }
        
        public void When(ComponentStopRequested evnt)
        {
            deployments.First(x => x.Id == evnt.DeploymentId).On(evnt);
        }
        
        public void When(ComponentStopped evnt)
        {
            deployments.First(x => x.Id == evnt.DeploymentId).On(evnt);
        }
        
        public void When(ComponentUndeploymentRequested evnt)
        {
            deployments.First(x => x.Id == evnt.DeploymentId).On(evnt);
        }

        public void When(ComponentUndeployed evnt)
        {
            deployments.RemoveAll(x => x.Id == evnt.DeploymentId);
        }
    }
}