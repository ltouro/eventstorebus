﻿namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public interface INodeController
    {
        DeploymentSlot ClaimDeploymentSlot(string nodeUrl);
        DeploymentConfirmation Deploy(string nodeUrl, string deploymentUrl, string service, string component, string version);
        void Undeploy(string nodeUrl, string deploymentId);
        void Start(string nodeUrl, string deploymentId);
        void Stop(string nodeUrl, string deploymentId);
    }
}