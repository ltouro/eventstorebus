using System;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;
using EventStoreBus.Supervisor.Nodes.Gateway;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Views.Api;
using Deploy = EventStoreBus.Supervisor.Nodes.Gateway.Deploy;

namespace EventStoreBus.Supervisor.Nodes.Receptors
{
    public class NodesReceptor
    {
        private readonly ICommandSender commandSender;

        public NodesReceptor(ICommandSender commandSender)
        {
            this.commandSender = commandSender;
        }

        public void When(OperationFailed evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new HandleOperationResult()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       Success = false,
                                                       Operation = evnt.Operation,
                                                       OperationId = evnt.OperationId,
                                                       Id = "OperationFailed/"+evnt.OperationId
                                                   });
        }
        
        public void When(DeploymentFailed evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new HandleOperationResult()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       Success = false,
                                                       Operation = Operation.Deploy,
                                                       OperationId = evnt.OperationId,
                                                       Id = "OperationFailed/"+evnt.OperationId
                                                   });
        }
        
        public void When(OperationSucceeded evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new HandleOperationResult()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       Success = true,
                                                       Operation = evnt.Operation,
                                                       OperationId = evnt.OperationId,
                                                       Id = "OperationSucceeded/"+evnt.OperationId
                                                   });
        }
        
        public void When(DeploymentSucceeded evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new HandleDeploymentSuccess()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       OperationId = evnt.OperationId,
                                                       Id = "OperationSucceeded/"+evnt.OperationId,
                                                       WSManagementUrl = evnt.WSManagementUrl,
                                                       WebManagementUrl = evnt.WebManagementUrl
                                                   });
        }

        public void When(ComponentDeploymentRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodeGateway>(new Deploy
            {
                OperationId = evnt.OperationId,
                Component = evnt.Component,
                DeploymentId = evnt.DeploymentId,
                DeploymentUrl = evnt.DeploymentUrl,
                Id = eventId.ToString(),
                NodeId = evnt.NodeId,
                NodeUrl = evnt.NodeUrl,
                Service = evnt.Service,
                Version = evnt.Version
            });
        }

        public void When(ComponentUndeploymentRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodeGateway>(new Undeploy()
            {
                OperationId = evnt.OperationId,
                DeploymentId = evnt.DeploymentId,
                Id = eventId.ToString(),
                NodeId = evnt.NodeId,
                NodeUrl = evnt.NodeUrl
            });
        }

        public void When(ComponentStartRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodeGateway>(new Start()
            {
                OperationId = evnt.OperationId,
                DeploymentId = evnt.DeploymentId,
                Id = eventId.ToString(),
                NodeId = evnt.NodeId,
                NodeUrl = evnt.NodeUrl
            });
        }

        public void When(ComponentStopRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodeGateway>(new Stop()
            {
                OperationId = evnt.OperationId,
                DeploymentId = evnt.DeploymentId,
                Id = eventId.ToString(),
                NodeId = evnt.NodeId,
                NodeUrl = evnt.NodeUrl
            });
        }

        public class Factory : IReceptorFactory
        {
            public object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders)
            {
                return new NodesReceptor(commandSender);
            }

            public void Release(object receptor)
            {
            }
        }
    }
}