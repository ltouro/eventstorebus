﻿namespace EventStoreBus.Supervisor.Nodes.Views
{
    public class Deployment
    {
        public string Id { get; set; }
        public string DeploymentOperationId { get; set; }
        public DeploymentState State { get; set; }
        public string Service { get; set; }
        public string Component { get; set; }
        public string Version { get; set; }
        public string WSManagementUrl { get; set; }
        public string WebManagementUrl { get; set; }
    }
}