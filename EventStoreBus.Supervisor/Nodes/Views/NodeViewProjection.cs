﻿using System;
using System.Linq;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Nodes.Views
{
    public class NodeViewProjection
    {
        private readonly IViewWriter<NodeView> writer;

        public NodeViewProjection(IViewWriter<NodeView> writer)
        {
            this.writer = writer;
        }

        public void When(Envelope<NodeAdded> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Create(evnt.Payload.NodeId, evnt, new NodeView()
                                                              {
                                                                  NodeId = evnt.Payload.NodeId,
                                                                  Url = evnt.Payload.ListenUrl
                                                              });
            }
        }

        public void When(Envelope<NodeRemoved> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Delete(evnt.Payload.NodeId, evnt);
            }
        }

        public void When(Envelope<ComponentAssigned> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Assignments.Add(new Assignment()
                                                                {
                                                                    AssignmentId = evnt.Payload.AssignmentId,
                                                                    Component = evnt.Payload.Component,
                                                                    Service = evnt.Payload.Service
                                                                }));
            }
        }

        public void When(Envelope<ComponentUnassigned> evnt)
        {
            var session = writer.OpenSession();
            session.Update(evnt.Payload.NodeId, evnt,
                           view => view.Assignments.RemoveAll(x => x.AssignmentId == evnt.Payload.AssignmentId));
        }

        public void When(Envelope<ComponentDeploymentRequested> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.Add(new Deployment()
                                                                {
                                                                    DeploymentOperationId = evnt.Payload.OperationId,
                                                                    Component = evnt.Payload.Component,
                                                                    Id = evnt.Payload.DeploymentId,
                                                                    Service = evnt.Payload.Service,
                                                                    Version = evnt.Payload.Version,
                                                                    State = DeploymentState.DeployPending
                                                                }));
            }
        }

        public void When(Envelope<ComponentDeployed> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view =>
                                   {
                                       var deployment = view.Deployments.First(d => d.Id == evnt.Payload.DeploymentId);
                                       deployment.State = DeploymentState.Stopped;
                                       deployment.WSManagementUrl = evnt.Payload.WSManagementUrl;
                                       deployment.WebManagementUrl = evnt.Payload.WebManagementUrl;
                                   });
            }
        }

        public void When(Envelope<ComponentUndeploymentRequested> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.First(d => d.Id == evnt.Payload.DeploymentId).State = DeploymentState.UndeployPending);
            }
        }

        public void When(Envelope<ComponentUndeployed> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.RemoveAll(x => x.Id == evnt.Payload.DeploymentId));
            }
        }

        public void When(Envelope<ComponentStartRequested> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.First(d => d.Id == evnt.Payload.DeploymentId).State = DeploymentState.StartPending);
            }
        }

        public void When(Envelope<ComponentStarted> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.First(d => d.Id == evnt.Payload.DeploymentId).State = DeploymentState.Running);
            }
        }

        public void When(Envelope<ComponentStopRequested> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.First(d => d.Id == evnt.Payload.DeploymentId).State = DeploymentState.StopPending);
            }
        }

        public void When(Envelope<ComponentStopped> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.NodeId, evnt,
                               view => view.Deployments.First(d => d.Id == evnt.Payload.DeploymentId).State = DeploymentState.Stopped);
            }
        }

        public class Storage : IViewStorage<NodeView>
        {
            public string CollectionName
            {
                get { return "nodes"; }
            }
        }

        public class Factory : IViewProjectionFactory
        {
            public object Create(Type viewProjectionImplementation, IViewWriters viewWriters)
            {
                return new NodeViewProjection(viewWriters.GetWriter<NodeView, Storage>());
            }

            public void Release(object projection)
            {
            }
        }
    }
}