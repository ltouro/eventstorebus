﻿using System;
using DurableSubscriber.Diagnostics;

namespace DurableSubscriber.Tests
{
    public class FakePushSource : IPushSource<object >
    {
        public event EventHandler<RecordedEventEventArgs<object >> Event;
        public event EventHandler<SubscriptionDroppedEventArgs<object>> SubscriptionDropped;

        public bool IsStarted { get; private set; }

        public void Start()
        {
            IsStarted = true;
            if (Started != null)
            {
                Started(this, new EventArgs());
            }
        }

        public void Stop()
        {
            IsStarted = false;
            if (Stopped != null)
            {
                Stopped(this, new EventArgs());
            }
        }

        public void PushEvent(object evnt)
        {
            if (Event != null)
            {
                Event(this, new RecordedEventEventArgs<object>(evnt, null));
            }
        }

        public event EventHandler Started;
        public event EventHandler Stopped;

        public State State
        {
            get { return null; }
        }
    }
}