﻿using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet("Deploy", "Service", SupportsShouldProcess = false)]
    public class DeployServiceCommand : BaseCommand
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Service { get; set; }
        
        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            client.RedeployAsync(Service).Wait();
            WriteVerbose(string.Format("Reuested deployment of service {0}.", Service));
        }
    }
}