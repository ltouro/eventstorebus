﻿using System;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public abstract class BaseCommand : PSCmdlet
    {
        [Alias("ServiceUrl")]
        [Parameter(Mandatory = true, Position = 1, ValueFromPipelineByPropertyName = false)]
        public string SupervisorUrl { get; set; }

        protected abstract void ProcessRecordWithSupervisorClient(SupervisorClient client);

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            try
            {
                ProcessRecordWithSupervisorClient(new SupervisorClient(SupervisorUrl));
            }
            catch (AggregateException ex)
            {
                ThrowTerminatingError(new ErrorRecord(ex.Flatten().InnerException, "", ErrorCategory.NotSpecified, null));
            }
            catch (Exception ex)
            {
                ThrowTerminatingError(new ErrorRecord(ex, "", ErrorCategory.NotSpecified, null));
            }
        }
    }
}