﻿using System.Linq;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Get, "Service", SupportsShouldProcess = false)]
    public class GetServiceCommand : BaseCommand
    {
        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            var references = client.GetServiceRepositoryAsync().Result;
            var services = references.Services.Select(x => client.GetServiceInfoAsync(x.Name).Result);

            foreach (var service in services)
            {
                WriteObject(new ServiceInfo(service));
            }
        }
    }
}