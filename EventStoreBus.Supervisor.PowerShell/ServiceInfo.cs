﻿using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public class ServiceInfo
    {
        public string Name { get; set; }
        public string CurrentVersion { get; set; }

        public ServiceInfo(Service service)
        {
            Name = service.Name;
            CurrentVersion = service.CurrentVersion;
        }
    }
}