﻿using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public class ViewStoreInfo
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }

        public ViewStoreInfo(ViewStore store)
        {
            Name = store.Name;
            ConnectionString = store.ConnectionString;
        }
    }
}