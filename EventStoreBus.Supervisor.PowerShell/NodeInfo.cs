﻿using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public class NodeInfo
    {
        public string Id { get; set; }
        public string Url { get; set; }

        public NodeInfo(Node node)
        {
            Id = node.Id;
            Url = node.Url;
        }
    }
}