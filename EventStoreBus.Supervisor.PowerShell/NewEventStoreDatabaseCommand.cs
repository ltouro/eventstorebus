﻿using System.Linq;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.New, "EventStoreDatabase", SupportsShouldProcess = false)]
    public class NewEventStoreDatabaseCommand : CommandWithTimeout
    {
        [Alias("StoreName")]
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string StoreId { get; set; }

        [Alias("Database")]
        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string DatabaseName { get; set; }
      
        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            client.CreateEventStoreDatabaseAsync(StoreId, DatabaseName).Wait();
            WriteVerbose(string.Format("Requested creating new database {0} on event store node {1}.", DatabaseName, StoreId));
            DoWaitForCompletion(() => client.GetEventStoreInfoAsync(StoreId).Result.Databases.Any(x => x.Name == DatabaseName), string.Format("event database {0}", DatabaseName));
        }
    }
}