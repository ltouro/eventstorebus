﻿using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public class DeploymentInfo
    {
        public string Id { get; set; }
        public string Service { get; set; }
        public string Version { get; set; }
        public string Component { get; set; }
        public DeploymentState State { get; set; }

        public DeploymentInfo(Deployment deployment, Instance instance)
        {
            Id = deployment.Id;
            Service = deployment.Service;
            Version = deployment.Version;
            Component = deployment.Component;
            State = instance.State;
        }
    }
}