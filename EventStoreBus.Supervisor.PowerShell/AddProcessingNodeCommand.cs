﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Add, "ProcessingNode", SupportsShouldProcess = false)]
    public class AddProcessingNodeCommand : CommandWithTimeout
    {
        [Alias("Name")]
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string Url { get; set; }

        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            client.AddNodeAsync(Id, Url).Wait();
            WriteVerbose(string.Format("Node {0} at {1} attached.", Id, Url));
            DoWaitForCompletion(() => client.GetNodeInfoAsync(Id).Result != null, string.Format("node {0}", Id));
        }
    }
}
