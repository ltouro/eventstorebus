﻿using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet("Stop", "Component", SupportsShouldProcess = false)]
    public class StopComponentServiceCommand : BaseCommand
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Node { get; set; }

        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string Deployment { get; set; }
        
        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            var deploymentId = client.FindDeploymentIdByPrefix(Node, Deployment);
            client.StopAsync(Node, deploymentId).Wait();
            WriteVerbose(string.Format("Stopped deployment {0} on node {1}.", Deployment, Node));
        }
    }
}