﻿using System;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Add, "EventStore", SupportsShouldProcess = false)]
    public class AddEventStoreNodeCommand : CommandWithTimeout
    {
        [Alias("Name")]
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        [Alias("IP")]
        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string Address { get; set; }
        
        [Parameter(Mandatory = true, Position = 4, ValueFromPipelineByPropertyName = true)]
        public int Port { get; set; }

        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            client.AddEventStoreAsync(Id, Address, Port).Wait();
            WriteVerbose(string.Format("Requested attaching event store node {0} at {1}:{2}.", Id, Address, Port));
            DoWaitForCompletion(() => client.GetEventStoreInfoAsync(Id).Result != null, string.Format("event store {0}", Id));
        }
    }
}