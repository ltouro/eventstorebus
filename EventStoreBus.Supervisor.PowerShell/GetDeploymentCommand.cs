﻿using System.Linq;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Get, "Deployment", SupportsShouldProcess = false)]
    public class GetDeploymentCommand : BaseCommand
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string NodeId { get; set; }

        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            var references = client.GetNodeCollectionAsync().Result;
            var node = references.Nodes.FirstOrDefault(x => x.Id == NodeId);
            if (node == null)
            {
                ThrowTerminatingError(new ErrorRecord(null, "", ErrorCategory.ObjectNotFound, null));
                return;
            }
            var details = client.GetNodeInfoAsync(node.Id).Result;
            foreach (var deployment in details.Deployments)
            {
                var state = client.GetInstanceInfoAsync(node.Id, deployment.Id).Result;
                WriteObject(new DeploymentInfo(deployment, state));
            }
        }
    }
}