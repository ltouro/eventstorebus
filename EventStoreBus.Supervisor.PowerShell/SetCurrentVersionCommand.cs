﻿using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Set, "CurrentVersion", SupportsShouldProcess = false)]
    public class SetCurrentVersionCommand : BaseCommand
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Service { get; set; }
        
        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string Version { get; set; }

        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            client.SetVersionAsync(Service, Version).Wait();
            WriteVerbose(string.Format("Set current version of service {0} to {1}.", Service, Version));
        }
    }
}