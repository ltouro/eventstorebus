﻿using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public class EventStoreInfo
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int TcpPort { get; set; }
        public int HttpPort { get; set; }

        public EventStoreInfo(EventStore store)
        {
            Name = store.Name;
            Address = store.Address;
            TcpPort = store.TcpPort;
            HttpPort = store.HttpPort;
        }
    }
}