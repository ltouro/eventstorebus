﻿namespace EventStoreBus.Logging
{
    public interface IFormat
    {
        void Args(params object[] formatArguments);
    }
}