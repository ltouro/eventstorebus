﻿namespace EventStoreBus
{
    public class DefaultFailureHandlingResult : IFailureHandlingResult
    {
        private readonly FailureHandlingAction action;
        private readonly int failedAttempts;

        public DefaultFailureHandlingResult(FailureHandlingAction action, int failedAttempts)
        {
            this.action = action;
            this.failedAttempts = failedAttempts;
        }

        public DefaultFailureHandlingResult Increment(FailureHandlingAction newAction)
        {
            return new DefaultFailureHandlingResult(newAction, failedAttempts + 1);
        }

        public FailureHandlingAction Action
        {
            get { return action; }
        }

        public int FailedAttempts
        {
            get { return failedAttempts; }
        }
    }
}