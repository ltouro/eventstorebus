﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Api;
using EventStoreBus.Config;
using Environment = EventStoreBus.Config.Environment;

namespace EventStoreBus
{
    public class ServiceBuilder
    {
        private readonly string deploymentId;
        private readonly IEnumerable<string> components;
        private readonly Service service;
        private readonly string version;
        private readonly Environment environment;
        private readonly IEnumerable<IExtension> extensions;
        private readonly TypeResolver typeResolver;

        public ServiceBuilder(string deploymentId, IEnumerable<string> components, Service service, string version, Environment environment, IEnumerable<IExtension> extensions)
        {
            this.deploymentId = deploymentId;
            this.components = components;
            this.service = service;
            this.version = version;
            this.environment = environment;
            this.extensions = extensions.ToList();
            typeResolver = new TypeResolver(service.MainAssembly, 
                this.extensions.SelectMany(x => x.TypeWrappers),
                this.extensions.SelectMany(x => x.AbbreviationProviders));
        }

        public IStartable BuildService()
        {
            var serviceType = service.Implementation != null
                                  ? Type.GetType(service.Implementation, true)
                                  : typeof (CompositionRoot);

            var compositionRoot = (CompositionRoot)Activator.CreateInstance(serviceType);

            var connectionManager = new EventStoreConnectionManager(service, environment);            

            var runtimeModules = service.Modules.Select(x => BuildRuntimeModule(x, connectionManager, compositionRoot));

            return new RuntimeService(service.Name, compositionRoot, runtimeModules);
        }

        private RuntimeModule BuildRuntimeModule(Module module, EventStoreConnectionManager connectionManager, CompositionRoot compositionRoot)
        {
            var componentBuilder = new ComponentBuilder(deploymentId, connectionManager, compositionRoot, module, service, version, environment, typeResolver);

            var receptorModules = BuildReceptorComponents(module, componentBuilder);
            var autonomousComponentModules = BuildCommandProcessorComponents(module, componentBuilder);
            var processorModules = BuildEventProcessorComponents(module, componentBuilder);
            var endpointModules = BuildEndpointComponents(module, componentBuilder);

            return new RuntimeModule(module.Name, receptorModules
                                                      .Concat(autonomousComponentModules)
                                                      .Concat(processorModules)
                                                      .Concat(endpointModules));
        }

        private IEnumerable<IStartable> BuildEventProcessorComponents(Module module, ComponentBuilder componentBuilder)
        {
            return  module.Components
                .OfType<EventProcessor>()
                .Join(components, x => x.Name, x => x, (comp, name) => comp)
                .Select(componentBuilder.BuildEventProcessorComponent);
        }

        private IEnumerable<IStartable> BuildEndpointComponents(Module module, ComponentBuilder componentBuilder)
        {
            return  module.Components
                .OfType<Config.Endpoint>()
                .Join(components, x => x.Name, x => x, (comp, name) => comp)
                .Select(componentBuilder.BuildEndpointComponent);
        }

        private IEnumerable<IStartable> BuildCommandProcessorComponents(Module module, ComponentBuilder componentBuilder)
        {
            return module.Components
                .OfType<Config.CommandProcessor>()
                .Join(components, x => x.Name, x => x, (comp, name) => comp)
                .Select(componentBuilder.BuildCommandProcessorComponent);
        }

        private IEnumerable<IStartable> BuildReceptorComponents(Module module, ComponentBuilder componentBuilder)
        {
            return module.Components
                .OfType<Config.Receptor>()
                .Join(components, x => x.Name, x => x, (comp, name) => comp)
                .Select(componentBuilder.BuildReceptorComponent);
        }
    }
}