﻿using System;
using EventStore.ClientAPI;

namespace EventStoreBus
{
    public class ViewProjectionFailureHandlingStrategy : IFailureHandlingStrategy
    {
        private readonly int maxFailedAttempts;

        public ViewProjectionFailureHandlingStrategy(int maxFailedAttempts)
        {
            this.maxFailedAttempts = maxFailedAttempts;
        }

        public IFailureHandlingResult HandleFailure(Exception exception, IFailureHandlingResult previousResult, RecordedEvent evnt)
        {
            var typedResult = (DefaultFailureHandlingResult)previousResult ??
                              new DefaultFailureHandlingResult(FailureHandlingAction.Retry, 0);

            if (typedResult.FailedAttempts < maxFailedAttempts)
            {
                return typedResult.Increment(FailureHandlingAction.Retry);
            }
            return typedResult.Increment(FailureHandlingAction.Escalate);
        }
    }
}