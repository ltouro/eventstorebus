﻿using System.Collections.Generic;

namespace EventStoreBus
{
    public interface IExtension
    {
        IEnumerable<ITypeWrapper> TypeWrappers { get; }
        IEnumerable<ITypeAbbreviationProvider> AbbreviationProviders { get; } 
    }
}