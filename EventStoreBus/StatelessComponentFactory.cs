﻿using System;
using DurableSubscriber;
using EventStoreBus.Api;

namespace EventStoreBus
{
    public class StatelessComponentFactory<T> : IAutonomousComponentFactory
        where T: IStatelessComponentFactory, new()
    {
        private static readonly T factoryImpl = new T();

        public object Create(Type componentImplementation, IEventStoreConnectionAccessor domainStoreConnection, ISerializer domainStoreEventSerializer, ICommandSender commandSender)
        {
            return factoryImpl.Create(componentImplementation,
                                      new EventPublisher(domainStoreConnection, domainStoreEventSerializer));
        }

        public void Release(object component)
        {
            factoryImpl.Release(component);
        }
    }
}