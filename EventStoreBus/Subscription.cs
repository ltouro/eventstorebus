﻿using System;
using System.Diagnostics;
using DurableSubscriber;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;
using ILogger = EventStoreBus.Logging.ILogger;
using LogManager = EventStoreBus.Logging.LogManager;

namespace EventStoreBus
{
    public class Subscription<T> : IInstrumentable
    {
        private const int EventProcessingRateWindowSize = 100;

        private readonly IEventStoreConnectionAccessor eventStoreConnection;
        private readonly string sourceName;
        private readonly EventSourceSelector<T> selector;
        private readonly Action onEscalateFailure;
        private readonly Action<RecordedEvent, string, Func<RecordedEvent, PersistentEvent>, IFailureHandlingResult> handleCallback;
        private readonly ICheckpointingStrategy<T> checkpointingStrategy;
        private readonly ISerializer serializer;
        private readonly ILogger logger;
        private Scheduler scheduler;
        private bool stopping;
        private long sequence;
        private int counter;
        private readonly Stopwatch watch = new Stopwatch();

        public Subscription(IEventStoreConnectionAccessor eventStoreConnection, 
            string componentName,
            string sourceName, 
            EventSourceSelector<T> selector, 
            Action onEscalateFailure, 
            Action<RecordedEvent, string, Func<RecordedEvent, PersistentEvent>, IFailureHandlingResult> handleCallback,
            ICheckpointingStrategy<T> checkpointingStrategy, 
            ISerializer serializer)
        {
            this.eventStoreConnection = eventStoreConnection;
            this.serializer = serializer;
            this.sourceName = sourceName;
            this.selector = selector;
            this.onEscalateFailure = onEscalateFailure;
            this.handleCallback = handleCallback;
            this.checkpointingStrategy = checkpointingStrategy;
            logger = LogManager.GetLogger(typeof(Subscription<T>).Name + "(" + componentName + "/" + sourceName + ")");
            selector.Event += OnEvent;
        }

        private void OnEvent(object sender, RecordedEventEventArgs<T> e)
        {           
            if (stopping)
            {
                return;
            }
            if (((RecordedEvent)e.Event).EventType.StartsWith("$"))
            {
                return;
            }
            if (sequence % EventProcessingRateWindowSize == 0)
            {
                if (sequence != 0)
                {
                    watch.Stop();
                    logger.Info("Processed {0} events in {1} ms", counter, watch.ElapsedMilliseconds);
                    counter = 0;
                    watch.Reset();
                }
                watch.Start();
            }
            sequence++;
            counter++;
            try
            {
                scheduler.ScheduleExecution(sequence,
                                        () => handleCallback((RecordedEvent)e.Event, sourceName, serializer.Deserialize, null),
                                        () => NotifyEventsProcessed(e.Position));
            }
            catch (EscalateFailureException)
            {
                onEscalateFailure();
            }
        }

        private void NotifyEventsProcessed(T until)
        {
            checkpointingStrategy.NotifyEventsProcessed(until);
        }

        public void Start()
        {
            sequence = 0;
            scheduler = new Scheduler();
            selector.Start();
        }

        public void Stop()
        {
            stopping = true;
            selector.Stop();
            scheduler.WaitToFinish();
            stopping = false;
        }

        public State State
        {
            get
            {
                return new State("Subscription from " + sourceName, GetType())
                    .WithDomainPrefix(sourceName)
                    .WithChild(eventStoreConnection.State)
                    .WithChild(selector.State);
            }
        }
    }
}