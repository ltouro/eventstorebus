﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DurableSubscriber;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;
using EventStoreBus.Logging;
using ILogger = EventStoreBus.Logging.ILogger;

namespace EventStoreBus
{
    public class EventHandlerHost : IStartable
    {
        private readonly ILogger logger;

        private readonly List<IEventHandler> handlers = new List<IEventHandler>();
        private readonly Dictionary<string, Subscription<int>> subscriptions = new Dictionary<string, Subscription<int>>();
        private readonly string name;
        private readonly IFailureHandlingStrategy failureHandlingStrategy;
        private bool running;
        private bool restarting;

        public EventHandlerHost(string name, IFailureHandlingStrategy failureHandlingStrategy)
        {
            this.name = name;
            this.failureHandlingStrategy = failureHandlingStrategy;
            logger = LogManager.GetLogger(typeof(EventHandlerHost).Name + "("+name+")");
        }

        private void HandleEvent(RecordedEvent recordedEvent, string source, Func<RecordedEvent, PersistentEvent> deserializeCallback, IFailureHandlingResult failureHandlingResult)
        {
            try
            {
                logger.Trace("Handling event {0}/{1}.", x => x.Args(recordedEvent.EventType, recordedEvent.EventId));
                if (CallHandlers(recordedEvent, source, deserializeCallback))
                {
                    logger.Debug("Successfully handled event {0}/{1}", x => x.Args(recordedEvent.EventType, recordedEvent.EventId));
                }
                else
                {
                    logger.Trace("Skipped handling event {0}/{1}", x => x.Args(recordedEvent.EventType, recordedEvent.EventId));
                }
            }
            catch (Exception ex)
            {
                var result = failureHandlingStrategy.HandleFailure(ex, failureHandlingResult, recordedEvent);
                logger.Debug("Error handling event {0}/{1}. Action: {2}", x => x.Args(recordedEvent.EventType, recordedEvent.EventId, result.Action));
                if (result.Action == FailureHandlingAction.Retry)
                {
                    HandleEvent(recordedEvent, source, deserializeCallback, result);
                }
                else if (result.Action == FailureHandlingAction.Escalate)
                {
                    logger.ErrorException(ex, "Escalating failure of handling event {0}/{1}", recordedEvent.EventType,
                                 recordedEvent.EventId);
                    Pause();
                }
                else if (result.Action == FailureHandlingAction.Skip)
                {
                    //Do nothing
                    logger.ErrorException(ex, "Skipping event {0}/{1} due to persistent processing failure", recordedEvent.EventType,
                                 recordedEvent.EventId);
                }
            }
        }

        private bool CallHandlers(RecordedEvent recordedEvent, string source, Func<RecordedEvent, PersistentEvent> deserializeCallback)
        {
            var result = false;
            var evnt = deserializeCallback(recordedEvent);
            if (evnt == null)
            {
                logger.Warning("Not deserializable event type encountered: {0}.", recordedEvent.EventType);
                return false;
            }
            foreach (var receptor in handlers.Where(receptor => receptor.CanHandle(evnt.Payload)))
            {
                result = true;
                receptor.Handle(new HandledEvent(source, recordedEvent.EventStreamId, recordedEvent.EventId, recordedEvent.EventNumber, evnt.Payload));
            }
            return result;
        }

        private void Pause()
        {
            if (restarting)
            {
                return;
            }
            logger.Info("Pausing.");
            restarting = true;
            Task.Factory.StartNew(() =>
                                      {
                                          Stop();
                                          Thread.Sleep(5000);
                                          restarting = false;
                                          Start();
                                      });
        }

        public void Start()
        {
            logger.Info("Starting.");
            foreach (var handler in handlers)
            {
                handler.Start();
            }
            foreach (var subscription in subscriptions.Values)
            {
                subscription.Start();
            }
            running = true;
            logger.Info("Started.");
        }

        public void Stop()
        {
            logger.Info("Stopping.");
            foreach (var subscription in subscriptions.Values)
            {
                subscription.Stop();
            }
            foreach (var handler in handlers)
            {
                handler.Stop();
            }
            running = false;
            logger.Info("Stopped.");
        }

        public void Add(IEventHandler receptor)
        {
            handlers.Add(receptor);
        }

        public void SubscribeTo(string subscriptionName, IEventStoreConnectionAccessor connection, ISerializer serializer, ICheckpointingStrategy<int> checkpointingStrategy)
        {
            var pullSource = new SingleEventStreamPullSource(connection, "", checkpointingStrategy);
            var pushSource = new SingleEventStreamPushSource(connection, "");

            var selector = new EventSourceSelector<int>(pullSource, pushSource, new RecordedEventIdentityExtractor());

            subscriptions.Add(subscriptionName,
                new Subscription<int>(connection, name, subscriptionName, selector, Pause, HandleEvent, checkpointingStrategy, serializer));
        }

        public State State
        {
            get
            {
                return new State("Event handler host " + name, GetType())
                    .WithDomainPrefix(name)
                    .WithAttribute("Running", () => running)
                    .WithChildStates(subscriptions.Values)
                    .WithChildStates(handlers);
            }
        }
    }
}