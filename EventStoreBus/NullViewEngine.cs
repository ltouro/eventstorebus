﻿using System;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Views.Api;

namespace EventStoreBus
{
    public class NullViewEngine : IViewEngine
    {
        private readonly IViewReaders readers = new Readers();
        private readonly IViewWriters writers = new Writers();

        public State State
        {
            get { return new State("Null view engine", GetType()); }
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public IViewReaders CreateReaders()
        {
            return readers;
        }

        public IViewWriters CreateWriters()
        {
            return writers;
        }

        private class Readers : IViewReaders
        {
            public IViewReader<TView> GetReader<TView, TViewStorage>() where TView : class where TViewStorage : IViewStorage<TView>, new()
            {
                throw new NotImplementedException();
            }
        }

        private class Writers : IViewWriters
        {
            public IViewWriter<TView> GetWriter<TView, TViewStorage>() where TView : class where TViewStorage : IViewStorage<TView>, new()
            {
                throw new NotImplementedException();
            }
        }
    }
}