﻿using System;
using System.Net;
using DurableSubscriber;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;
using EventStore.ClientAPI.Exceptions;
using EventStoreBus.Logging;

namespace EventStoreBus
{
    public class PersistentEventStoreConnectionAccessor : IEventStoreConnectionAccessor
    {
        private readonly IPEndPoint tcpEndpoint;
        private readonly IPEndPoint httpEndpoint;
        private readonly string databaseName;
        private readonly IEventStoreConnection connection;
        private bool connected;
        private int reconnectionAttempts;
        private readonly Logging.ILogger logger;

        public PersistentEventStoreConnectionAccessor(IPEndPoint tcpEndpoint, IPEndPoint httpEndpoint, string databaseName, ConnectionSettingsBuilder connectionSettings)
        {
            logger = LogManager.GetLogger(typeof(PersistentEventStoreConnectionAccessor).Name + "(" + tcpEndpoint + "/" + databaseName + ")");
            var settings = connectionSettings
                .LimitReconnectionsTo(int.MaxValue)
                .SetOperationTimeoutTo(TimeSpan.FromSeconds(5));

            this.tcpEndpoint = tcpEndpoint;
            this.httpEndpoint = httpEndpoint;
            this.databaseName = databaseName;

            var nativeConnection = EventStoreConnection.Create(settings);
            nativeConnection.Connected += OnConnected;
            nativeConnection.Disconnected += OnDisconnected;
            nativeConnection.Reconnecting += OnReconnecting;
            nativeConnection.ConnectAsync(tcpEndpoint);

            connection = new DefualtEventStoreConnection(nativeConnection);
            if (!string.IsNullOrEmpty(databaseName))
            {
                connection = new MultiDatabaseEventStoreConnection(databaseName, connection);
            }         
        }

        private void OnReconnecting(object s, EventArgs e)
        {
            logger.Debug("Reconnecting.");
            reconnectionAttempts++;
        }

        private void OnDisconnected(object s, EventArgs e)
        {
            logger.Error("Disconnected.");
            connected = false;
        }

        private void OnConnected(object s, EventArgs e)
        {
            logger.Info("Connected.");


            //EnsureDatabaseProjectionExists(projectionName);
            reconnectionAttempts = 0;
            connected = true;
        }

        private void EnsureDatabaseProjectionExists()
        {
            logger.Debug("Ensuring 'databases' projection is deployed");

            string projectionName = "database-" + databaseName;
            var projManager = new ProjectionsManager(httpEndpoint);
            try
            {
                projManager.CreateContinuous(projectionName,
                                             @"fromAll() 
.whenAny(function (s, e) {
    if (e.streamId.indexOf('$') === 0) {
        return;
    }
    var databaseSeparatorIndex = e.streamId.indexOf('+');
    if (databaseSeparatorIndex < 0) {
        return;
    }
    var databaseName = e.streamId.substr(0, databaseSeparatorIndex);
    if (databaseName === '" + databaseName + @"') {
        linkTo(databaseName, e);
    }
})");
            }
            catch (Exception ex)
            {
                logger.DebugException(ex, "Unable to create projection for the database.");
            }
        }

        public bool TryInvoke(Action<IEventStoreConnection> action)
        {
            try
            {
                action(connection);
                return true;
            }
            catch (OperationTimedOutException ex)
            {
                return false;
            }
            catch (AggregateException ex)
            {
                var exceptionType = ex.InnerExceptions[0].GetType();
                if (exceptionType == typeof(OperationTimedOutException)
                    || exceptionType == typeof(RetriesLimitReachedException)
                    || exceptionType == typeof(CannotEstablishConnectionException))
                {
                    return false;
                }
                throw ex.InnerExceptions[0];
            }
            catch (RetriesLimitReachedException ex)
            {
                return false;
            }
            catch (CannotEstablishConnectionException ex)
            {
                return false;
            }
        }

        public void Invoke(Action<IEventStoreConnection> action)
        {
            try
            {
                action(connection);
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

        public T Invoke<T>(Func<IEventStoreConnection, T> func)
        {
            try
            {
                return func(connection);
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }


        public bool TryInvoke<T>(Func<IEventStoreConnection, T> func, out T result)
        {            
            try
            {
                result = func(connection);
                return true;
            }
            catch (OperationTimedOutException ex)
            {
                result = default(T);
                return false;
            }
            catch (AggregateException ex)
            {
                var exceptionType = ex.InnerExceptions[0].GetType();
                if (exceptionType == typeof(OperationTimedOutException)
                    || exceptionType == typeof(RetriesLimitReachedException)
                    || exceptionType == typeof(CannotEstablishConnectionException))
                {
                    result = default(T);
                    return false;
                }
                throw ex.InnerExceptions[0];
            }
            catch (RetriesLimitReachedException ex)
            {
                result = default(T);
                return false;
            }
            catch (CannotEstablishConnectionException ex)
            {
                result = default(T);
                return false;
            }
        }

        public State State
        {
            get
            {
                return new State("Connection to " + tcpEndpoint, GetType())
                    .WithProperty("Endpoint", tcpEndpoint.ToString())
                    .WithProperty("Database", databaseName)
                    .WithAttribute("Connected", () => connected)
                    .WithAttribute("ReconnectionAttempts", () => reconnectionAttempts);
            }
        }
    }
}