﻿using System;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus
{
    public class Gateway : IEventHandler
    {
        private readonly ILogger logger;

        private readonly Type implementationType;
        private readonly IGatewayFactory gatewayFactory;
        private readonly ICommandSender commandSender;
        private readonly IViewEngine viewEngine;
        private readonly IDispatcher dispatcher;

        public Gateway(Type implementationType, IGatewayFactory gatewayFactory, IDispatcher dispatcher, ICommandSender commandSender, IViewEngine viewEngine)
        {
            this.implementationType = implementationType;
            this.commandSender = commandSender;
            this.viewEngine = viewEngine;
            this.gatewayFactory = gatewayFactory;
            this.dispatcher = dispatcher;
            logger = LogManager.GetLogger(typeof(Gateway).Name + "(" + implementationType.Name + ")");
        }

        public bool CanHandle(object payload)
        {
            return dispatcher.CanHandle(payload);
        }

        public void Handle(HandledEvent evnt)
        {
            logger.Debug("Creating instance to handle {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
            var gateway = gatewayFactory.Create(implementationType, commandSender, viewEngine.CreateReaders());
            try
            {
                logger.Debug("Handling {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
                dispatcher.Handle(gateway, evnt.Payload, evnt.EventId);
                logger.Debug("{0} event successfully handled.", x => x.Args(evnt.Payload.GetType().FullName));
            }
            finally
            {
                logger.Debug("Releasing instance after handling {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
                gatewayFactory.Release(gateway);
            }
        }

        public void Start()
        {
            viewEngine.Start();
        }

        public void Stop()
        {
            viewEngine.Stop();
        }

        public State State
        {
            get
            {
                return new State("Gateway (" + implementationType.Name + ")", GetType())
                    .WithProperty("Type", implementationType.Name);
            }
        }
    }
}