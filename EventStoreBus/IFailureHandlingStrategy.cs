﻿using System;
using EventStore.ClientAPI;

namespace EventStoreBus
{
    public interface IFailureHandlingStrategy
    {
        IFailureHandlingResult HandleFailure(Exception exception, IFailureHandlingResult previousResult, RecordedEvent evnt);
    }
}