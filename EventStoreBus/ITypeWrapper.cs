﻿using System;

namespace EventStoreBus
{
    public interface ITypeWrapper
    {
        bool CanWrap(Type canidate);
        Type Wrap(Type bareType);
    }

    public interface ITypeAbbreviationProvider
    {
        bool KnowsAbbreviationFor(string typeName);
        string GetFullName(string abbreviation);
    }
}