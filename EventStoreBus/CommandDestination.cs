﻿using System;
using DurableSubscriber;
using EventStore.ClientAPI;

namespace EventStoreBus
{
    public class CommandDestination
    {
        private readonly IEventStoreConnectionAccessor connection;
        private readonly ISerializer serializer;
        private readonly string streamName;

        public CommandDestination(IEventStoreConnectionAccessor connection, ISerializer serializer, string streamName)
        {
            this.connection = connection;
            this.serializer = serializer;
            this.streamName = streamName;
        }

        public bool Send(object command)
        {
            var commandEvent = serializer.Serialize(Guid.NewGuid(), command, null);
            return connection.TryInvoke(x => x.AppendToStream(streamName, ExpectedVersion.Any, new[] {commandEvent}));
        }

        public override string ToString()
        {
            return streamName;
        }
    }
}