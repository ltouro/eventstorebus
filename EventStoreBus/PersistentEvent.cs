﻿using System;

namespace EventStoreBus
{
    public class PersistentEvent
    {
        public readonly object Payload;
        public readonly EventMetadata Metadata;
        public readonly Guid Id;        

        public PersistentEvent(object payload, EventMetadata metadata, Guid id)
        {
            Payload = payload;
            Id = id;
            Metadata = metadata;
        }
    }
}