﻿namespace EventStoreBus
{
    public interface ICommandRouter
    {
        CommandDestination DetermineTargetStream(object command);
    }
}