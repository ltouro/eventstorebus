using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EventStoreBus
{
    public class DynamicDispatcher : IDispatcher
    {
        private readonly Dictionary<Type, MethodInfo> handlingMethods;

        public DynamicDispatcher(Type targetType)
            : this(targetType, "When")
        {
        }

        public DynamicDispatcher(Type targetType, string handlerMethodName)
        {
            handlingMethods = targetType.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.Name == handlerMethodName)
                .ToDictionary(x => x.GetParameters()[0].ParameterType, x => x);
        }

        public bool CanHandle(object payload)
        {
            return handlingMethods.ContainsKey(payload.GetType());
        }

        public object Handle(object target, params object[] arguments)
        {
            var method = handlingMethods[arguments[0].GetType()];
            return method.Invoke(target, arguments);
        }
    }
}