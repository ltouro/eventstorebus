﻿using DurableSubscriber.Diagnostics;

namespace EventStoreBus
{
    public interface IStartable : IInstrumentable
    {
        void Start();
        void Stop();
    }
}