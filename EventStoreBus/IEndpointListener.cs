﻿using DurableSubscriber.Diagnostics;

namespace EventStoreBus
{
    public interface IEndpointListener : IInstrumentable
    {
        void Start();
        void Stop();
    }
}