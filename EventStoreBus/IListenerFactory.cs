﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus
{
    public interface IListenerFactory
    {
        IEndpointListener Create(Type gatewayImplementation, string name, string endpointAddress, ICommandSender commandSender, IViewEngine viewEngine);
    }
}