﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus
{
    [Serializable]
    public class EscalateFailureException : Exception
    {
        public EscalateFailureException()
        {
        }

        public EscalateFailureException(string message) : base(message)
        {
        }

        public EscalateFailureException(string message, Exception inner) : base(message, inner)
        {
        }

        protected EscalateFailureException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}