﻿using System.Linq;
using DurableSubscriber.Diagnostics;
using NetMX;

namespace EventStoreBus
{
    public static class StateToBeanConverter
    {
        private static int id;

        public static void RegisterStateBeansRecursive(IMBeanServer server, State state, string domainPrefix = "")
        {
            var properties = state.Properties.ToDictionary(
                property => property.Name, 
                property => property.Value.Replace(":","-"));
            //properties.Add("Id", id.ToString());
            id++;
            var cleanTypeName = state.Type.Name;
            var genericPartStartIndex = cleanTypeName.IndexOf("`");
            if (genericPartStartIndex >= 0)
            {
                cleanTypeName = cleanTypeName.Substring(0, genericPartStartIndex);
            }
            domainPrefix = JoinIfNotEmpty(domainPrefix, state.DomainPrefix);
            var domain = JoinIfNotEmpty(domainPrefix, cleanTypeName);
            
            var name = new ObjectName(domain, properties);

            var bean = new StateMBean(state);
            server.RegisterMBean(bean, name);

            foreach (var childState in state.ChildStates)
            {
                RegisterStateBeansRecursive(server, childState, domainPrefix);
            }
        }

        private static string JoinIfNotEmpty(params string[] strings)
        {
            return string.Join(".", strings.Where(x => !string.IsNullOrEmpty(x)));
        }
    }
}