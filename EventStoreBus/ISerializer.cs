﻿using System;
using System.Reflection;
using EventStore.ClientAPI;

namespace EventStoreBus
{
    public interface ISerializer
    {       
        EventData Serialize(Guid uniqueId, object payload, string relatesTo);
        PersistentEvent Deserialize(RecordedEvent recordedEvent);
        void RegisterEvents(Assembly eventAssembly);
    }
}