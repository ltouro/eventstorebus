﻿using System.Runtime.Serialization;

namespace EventStoreBus.Node.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class Instance
    {
        [DataMember]
        public InstanceState State { get; set; }
        [DataMember]
        public HyperLink Deployment { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
    }
}