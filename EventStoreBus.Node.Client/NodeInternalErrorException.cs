﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus.Node.Client
{
    [Serializable]
    public class NodeInternalErrorException : Exception
    {
        public NodeInternalErrorException()
        {
        }

        public NodeInternalErrorException(string message) : base(message)
        {
        }

        public NodeInternalErrorException(string message, Exception inner) : base(message, inner)
        {
        }

        protected NodeInternalErrorException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}