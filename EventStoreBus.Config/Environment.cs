﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlRoot(ElementName = "environment", Namespace = Const.Namespace)]
    [XmlType(TypeName = "environment", Namespace = Const.Namespace)]
    public class Environment
    {
        [XmlArray(ElementName = "eventStores", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "eventStore", Namespace = Const.Namespace)]
        public List<EventStoreNode> EventStores { get; set; }

        [XmlArray(ElementName = "viewStores", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "viewStore", Namespace = Const.Namespace)]
        public List<ViewStoreNode> ViewStores { get; set; }

        [XmlArray(ElementName = "deployments", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "deployment", Namespace = Const.Namespace)]
        public List<ComponentDeployment> Deployments { get; set; }

        public Environment()
        {
            EventStores = new List<EventStoreNode>();
            ViewStores = new List<ViewStoreNode>();
            Deployments = new List<ComponentDeployment>();
        }
    }
}