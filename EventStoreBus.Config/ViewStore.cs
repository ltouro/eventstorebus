﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "viewStore", Namespace = Const.Namespace)]
    public class ViewStore
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "engineImplementation")]
        public string EngineImplementation { get; set; }
    }
}