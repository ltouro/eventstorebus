﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "subscription", Namespace = Const.Namespace)]
    public class Subscription
    {
        [XmlAttribute(AttributeName = "sourceStore")]
        public string SourceStore { get; set; }        

        [XmlElement(ElementName = "checkpointing", Namespace = Const.Namespace)]
        public Checkpointing Checkpointing { get; set; }
    }
}