﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "processor", Namespace = Const.Namespace)]
    [XmlRoot(ElementName = "processor", Namespace = Const.Namespace)]
    public class EventProcessor : Component
    {
        [XmlAttribute(AttributeName = "dataStore")]
        public string DataStore { get; set; }

        [XmlArray(ElementName = "subscriptions", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "subscription", Namespace = Const.Namespace)]
        public List<Subscription> Subscriptions { get; set; }

        [XmlArray(ElementName = "stages", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "view", Namespace = Const.Namespace, Type = typeof(View))]
        [XmlArrayItem(ElementName = "gateway", Namespace = Const.Namespace, Type = typeof(Gateway))]
        public List<ProcessorStage> Stages { get; set; }

        [XmlElement(ElementName = "failureHandling", Namespace = Const.Namespace)]
        public FailureHandling FailureHandling { get; set; }

        public EventProcessor()
        {
            Subscriptions = new List<Subscription>();
            Stages = new List<ProcessorStage>();
        }
    }
}