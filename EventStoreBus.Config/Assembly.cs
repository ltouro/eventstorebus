﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "assembly", Namespace = Const.Namespace)]
    public class Assembly
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}