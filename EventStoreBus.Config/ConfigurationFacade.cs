﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using EventStoreBus.Config.Conventions;

namespace EventStoreBus.Config
{
    public static class ConfigurationFacade
    {
        private static readonly XmlSerializer serializer = new XmlSerializer(typeof(Service));
        private static readonly List<IConvention> conventions
            = new List<IConvention>
                 {
                     new ServiceNameConvention(),
                     new CommandProcessorDefaultCheckpointingConvention(),
                     new CommandProcessorDefaultFailureHandlingConvention(),
                     new CommandProcessorDefaultNameConvention(),
                     new EventProcessorDefaultCheckpointingConvention(),
                     new EventProcessorDefaultFailureHandlingConvention(),
                     new EventProcessorStageDefaultNameConvention(),
                     new ReceptorDefaultCheckpointingConvention(),
                     new ReceptorDefaultFailureHandlingConvention(),
                     new ReceptorDefaultNameConvention()
                 };

        public static Service ReadServiceDescriptionFromStream(Stream stream)
        {
            var service = (Service) serializer.Deserialize(stream);
            ApplyConventions(service);
            return service;
        }

        private static void ApplyConventions(Service service)
        {
            foreach (var convention in conventions)
            {
                convention.Apply(service);
            }
        }
    }
}