﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    public abstract class ProcessorStage
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "implementation")]
        public string Implementation { get; set; }

        [XmlAttribute(AttributeName = "factory")]
        public string Factory { get; set; } 
    }
}