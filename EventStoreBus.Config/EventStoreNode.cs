﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "eventStore", Namespace = Const.Namespace)]
    public class EventStoreNode
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "tcpPort")]
        public int TcpPort { get; set; }
        
        [XmlAttribute(AttributeName = "httpPort")]
        public int HttpPort { get; set; }

        [XmlAttribute(AttributeName = "address")]
        public string Address { get; set; }

        [XmlArray(ElementName = "databases", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "database", Namespace = Const.Namespace)]
        public List<EventStoreDatabase> Databases { get; set; }

        public EventStoreNode()
        {
            Databases = new List<EventStoreDatabase>();
        }
    }
}