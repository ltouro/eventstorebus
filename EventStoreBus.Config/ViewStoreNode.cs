﻿using System;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "viewStore", Namespace = Const.Namespace)]
    public class ViewStoreNode
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "connectionString")]
        public string ConnectionString { get; set; }
    }
}