﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "module", Namespace = Const.Namespace)]
    public class Module
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "domainStore", Namespace = Const.Namespace)]
        public EventStore DomainStore { get; set; }

        [XmlArray(ElementName = "components", Namespace = Const.Namespace)]
        [XmlArrayItem(Type = typeof(Receptor), ElementName = "receptor", Namespace = Const.Namespace)]
        [XmlArrayItem(Type = typeof(CommandProcessor), ElementName = "commandProcessor", Namespace = Const.Namespace)]
        [XmlArrayItem(Type = typeof(EventProcessor), ElementName = "eventProcessor", Namespace = Const.Namespace)]
        [XmlArrayItem(Type = typeof(Endpoint), ElementName = "endpoint", Namespace = Const.Namespace)]
        public List<Component> Components { get; set; }

        public Module()
        {
            Components = new List<Component>();
        }
    }
}