﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    public abstract class Component
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}