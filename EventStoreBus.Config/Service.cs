﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlRoot(ElementName = "service", Namespace = Const.Namespace)]
    [XmlType(TypeName = "service", Namespace = Const.Namespace)]
    public class Service
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "mainAssembly")]
        public string MainAssembly { get; set; }

        [XmlAttribute(AttributeName = "implementation")]
        public string Implementation { get; set; }
        
        [XmlArray(ElementName = "externalEventStores", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "store", Namespace = Const.Namespace)]
        public List<EventStore> ExternalEventStores { get; set; }
        
        [XmlArray(ElementName = "viewStores", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "store", Namespace = Const.Namespace)]
        public List<ViewStore> ViewStores { get; set; }

        [XmlArray(ElementName = "modules", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "module", Namespace = Const.Namespace)]
        public List<Module> Modules { get; set; }

        public Service()
        {
            ExternalEventStores = new List<EventStore>();
            Modules = new List<Module>();
        }
    }
}