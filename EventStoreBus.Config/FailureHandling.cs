﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "failureHandling", Namespace = Const.Namespace)]
    public class FailureHandling
    {
        [XmlElement(ElementName = "default", Namespace = Const.Namespace)]
        public DefaultFailureHandling Default { get; set; }

        [XmlElement(ElementName = "noSkip", Namespace = Const.Namespace)]
        public NoSkipFailureHandling NoSkip { get; set; }
    }
}