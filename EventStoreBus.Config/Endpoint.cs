﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "endpoint", Namespace = Const.Namespace)]
    public class Endpoint : Component
    {
        [XmlAttribute(AttributeName = "address")]
        public string Address { get; set; }

        [XmlAttribute(AttributeName = "factory")]
        public string Factory { get; set; }

        [XmlAttribute(AttributeName = "implementation")]
        public string Implementation { get; set; }

        [XmlAttribute(AttributeName = "viewStore")]
        public string ViewStore { get; set; }
    }
}