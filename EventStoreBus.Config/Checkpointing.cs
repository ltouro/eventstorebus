﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "checkpointing", Namespace = Const.Namespace)]
    public class Checkpointing
    {
        [XmlAttribute(AttributeName = "batchSize")]
        public int BatchSize { get; set; }
    }
}