using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class EventProcessorDefaultFailureHandlingConvention : IConvention
    {
        private const int Retries = 3;

        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<EventProcessor>())
            {
                if (component.FailureHandling == null)
                {
                    component.FailureHandling = new FailureHandling
                        {
                            NoSkip = new NoSkipFailureHandling
                                {
                                    Retries = Retries
                                }
                        };
                }
            }
        }
    }
}