using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class ReceptorDefaultCheckpointingConvention : IConvention
    {
        private const int BatchSize = 5;

        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<Receptor>())
            {
                foreach (var subscription in component.Subscriptions)
                {
                    if (subscription.Checkpointing == null)
                    {
                        subscription.Checkpointing = new Checkpointing()
                        {
                            BatchSize = BatchSize
                        };
                    }
                }
            }
        }
    }
}