using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class EventProcessorStageDefaultNameConvention : DefaultNameConvention, IConvention
    {
        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<EventProcessor>())
            {
                foreach (var stage in component.Stages)
                {
                    if (stage.Name == null)
                    {
                        stage.Name = DeriveNameFromImplementation(stage.Implementation);
                    }
                }
            }
        }
    }
}