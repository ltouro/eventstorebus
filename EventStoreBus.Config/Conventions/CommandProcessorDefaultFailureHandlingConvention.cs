using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class CommandProcessorDefaultFailureHandlingConvention : IConvention
    {
        private const int Retries = 3;

        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<CommandProcessor>())
            {
                if (component.FailureHandling == null)
                {
                    component.FailureHandling = new FailureHandling
                        {
                            Default = new DefaultFailureHandling
                                {
                                    Retries = Retries
                                }
                        };
                }
            }
        }
    }
}