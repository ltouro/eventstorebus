﻿namespace EventStoreBus.Config.Conventions
{
    public interface IPreConventionValidator
    {
        void Validate(Service service);
    }
}