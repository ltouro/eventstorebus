using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class ReceptorDefaultNameConvention : DefaultNameConvention, IConvention
    {
        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<Receptor>())
            {
                if (component.Name == null)
                {                    
                    component.Name = DeriveNameFromImplementation(component.Implementation);
                }
            }
        }
    }
}