using System;
using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public abstract class DefaultNameConvention
    {
        protected string DeriveNameFromImplementation(string implementationType)
        {
            var indexOfFirstComma = implementationType.IndexOf(",", StringComparison.Ordinal);

            var fullName = indexOfFirstComma > 0
                               ? implementationType.Substring(0, indexOfFirstComma)
                               : implementationType;

            var bareName = fullName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Last();
            return bareName;
        }
    }
}