using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class CommandProcessorDefaultNameConvention : DefaultNameConvention, IConvention
    {
        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<CommandProcessor>())
            {
                if (component.Name == null)
                {                    
                    component.Name = DeriveNameFromImplementation(component.Implementation);
                }
            }
        }
    }
}