using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlRoot(ElementName = "deployment", Namespace = Const.Namespace)]
    [XmlType(TypeName = "deployment", Namespace = Const.Namespace)]
    public class Deployment
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlArray(ElementName = "components", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "component", Namespace = Const.Namespace)]
        public List<string> Components { get; set; }

        [XmlAttribute(AttributeName = "baseUrl")]
        public string BaseUrl { get; set; }

        [XmlAttribute(AttributeName = "service")]
        public string Service { get; set; }

        [XmlAttribute(AttributeName = "logDirectory")]
        public string LogDirectory { get; set; }

        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }

        public Deployment()
        {
            Components = new List<string>();
        }
    }
}