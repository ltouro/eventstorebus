﻿<?xml version="1.0"?>
<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
  <metadata>
    <version>1.0.0</version>
    <authors></authors>
    <owners></owners>
    <licenseUrl>http://example.com/</licenseUrl>
    <projectUrl>http://example.com/</projectUrl>
    <id>$rootnamespace$</id>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>
      $rootnamespace$ service
    </description>
    <dependencies>
    </dependencies>
  </metadata>
  <files>
    <file src="..\..\bin\debug\service.xml"/>
    <file src="..\..\bin\debug\$assemblyname$.dll"/>
  </files>
</package>