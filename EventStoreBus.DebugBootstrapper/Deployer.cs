﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using EventStoreBus.Config;
using NuGet;
using Environment = System.Environment;

namespace EventStoreBus.DebugBootstrapper
{
    public class Deployer
    {
        
        private const string LogsDirectoryName = "Logs";
        private const string HostPackageName = "EventStoreBus.Host";
        private const string BaseUrl = "http://localhost:4499/";

        private readonly IFileSystem fileSystem;
        private readonly IPackageRepository remoteRepository;
        private static readonly XmlSerializer deploymentDescriptorSerializer = new XmlSerializer(typeof(Deployment));
        private static readonly XmlSerializer environmentSerializer = new XmlSerializer(typeof(Config.Environment));
        private static readonly XmlSerializer serviceSerializer = new XmlSerializer(typeof(Service));

        public Deployer()
        {
            remoteRepository = new LocalPackageRepository(@"C:\NuGetServerTest");
            fileSystem = new PhysicalFileSystem(".");
        }

        public void Deploy()
        {
            const string version = "1.0.0";
            Service service = ReadServiceDescription();
            var deploymentId = service.Name + "DebugDeployment";
            var node = service.Name + "Node";

            var components = service.Modules.SelectMany(x => x.Components).Select(x => x.Name).ToList();
            
            var hostPackage = remoteRepository.FindPackage(HostPackageName);
            InstallPackage(hostPackage);
            SaveDeploymentDescriptor(deploymentId, service.Name, version, components);
            UpdateEnvironment(node, deploymentId, service.Name, version, components);

            
        }

        private static void SaveDeploymentDescriptor(string deploymentId, string service, string version, IEnumerable<string> components)
        {
            var logDirectory = Path.Combine(Environment.CurrentDirectory, LogsDirectoryName);
            var deploymentDescriptor = new Deployment
                                           {
                                               Id = deploymentId,
                                               BaseUrl = BaseUrl,
                                               Service = service,
                                               Version = version,
                                               LogDirectory = logDirectory
                                           };
            deploymentDescriptor.Components.AddRange(components);
            using (var deploymentDescriptorStream = File.OpenWrite("deployment.xml"))
            {
                deploymentDescriptorSerializer.Serialize(deploymentDescriptorStream, deploymentDescriptor);
                deploymentDescriptorStream.Flush();
            }
        }

        private void InstallPackage(IPackage package)
        {
            foreach (var file in package.GetLibFiles())
            {
                var fileName = Path.GetFileName(file.Path);
                fileSystem.AddFile(Path.Combine(Environment.CurrentDirectory, fileName), file.GetStream());                
            }
        }


        public void UpdateEnvironment(string node, string deploymentId, string service, string version, IEnumerable<string> components)
        {
            var template = ReadEnvironmentDebugTemplate();
            foreach (var component in components)
            {
                template.Deployments.Add(new ComponentDeployment()
                                             {
                                                 DeploymentId = deploymentId,
                                                 Component = component,
                                                 NodeId = node,
                                                 Service = service,
                                                 Version = version
                                             });
            }
            SaveEnvironment(template);
        }

        private static void SaveEnvironment(Config.Environment environment)
        {
            using (var environmentFile = File.OpenWrite("environment.xml"))
            {
                environmentSerializer.Serialize(environmentFile, environment);
                environmentFile.Flush();
            }
        }

        private static Config.Environment ReadEnvironmentDebugTemplate()
        {
            using (var environmentFile = File.OpenRead("environment.debug.xml"))
            {
                return (Config.Environment)environmentSerializer.Deserialize(environmentFile);
            }
        }

        private static Service ReadServiceDescription()
        {
            Service service;
            using (var stream = new FileStream("service.xml", FileMode.Open))
            {
                service = (Service)serviceSerializer.Deserialize(stream);
            }
            return service;
        }
    }
}