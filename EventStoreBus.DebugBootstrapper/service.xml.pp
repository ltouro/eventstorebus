﻿<?xml version="1.0" encoding="utf-8"?>
<service xmlns="http://eventstorebus.com" name="$rootnamespace$" implementation="EventStoreBus.Api.CompositionRoot, EventStoreBus.Api">
  <eventStores>
    <store name="Primary" serializerImplementation="EventStoreBus.DefaultSerializer, EventStoreBus">
      <events>
        <assembly name="$AssemblyName$.Events"/>
        <assembly name="$AssemblyName$"/>
      </events>
    </store>
  </eventStores>
  <viewStores>
    <store name="Primary" engineImplementation="EventStoreBus.Views.RavenViewEngine, EventStoreBus.Views"/>
  </viewStores>
  <components>
    <receptor name="SomeReceptor"
              implementation="EventStoreBus.Demo.Receptors.SomeReceptor, ServiceA"
              factory="EventStoreBus.Demo.Receptors.SomeReceptorFactory, ServiceA">
      <failureHandling>
        <default retries="3" failedStore="Primary" failedStream="failed-events"/>
      </failureHandling>
      <subscriptions>
        <subscription sourceStore="External" checkpointingStore="Primary">
          <checkpointing batchSize="5"/>
        </subscription>
      </subscriptions>
    </receptor>
    <commandProcessor name="SomeComponent" commandStore="Primary" domainStore="Primary"
               implementation="EventStoreBus.Demo.Aggregates.SomeComponent, ServiceA"
               factory="EventStoreBus.Aggregates.AggregateComponentFactory`1[[EventStoreBus.Demo.Aggregates.SomeComponentFactory, ServiceA]], EventStoreBus.Aggregates">
      <failureHandling>
        <default retries="3" failedStore="Primary" failedStream="failed-commands"/>
      </failureHandling>
      <checkpointing batchSize="5"/>
    </commandProcessor>

    <eventProcessor name="SomeViewGroup" dataStore="Primary" retries="3">
      <stages>
        <view name="SomeView"
              implementation="EventStoreBus.Demo.Views.SomeViewProjection, ServiceA"
              factory="EventStoreBus.Demo.Views.SomeViewProjectionFactory, ServiceA"/>
      </stages>
      <subscriptions>
        <subscription sourceStore="Primary" checkpointingStore="Primary">
          <checkpointing batchSize="5"/>
        </subscription>
      </subscriptions>
    </eventProcessor>
  </components>
</service>
