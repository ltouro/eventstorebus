﻿param($installPath, $toolsPath, $package, $project)

$solution2 = Get-Interface $dte.Solution ([EnvDTE80.Solution2])

function Add-SolutionFolder {
    param(
       [string]$Name
    )    
    $solution2.AddSolutionFolder($Name)
}
 
function Get-SolutionFolder {
    param (
        [string]$Name
    )
    $solution2.Projects | ?{ $_.Kind -eq [EnvDTE80.ProjectKinds]::vsProjectKindSolutionFolder -and $_.Name -eq $Name }
}

$folder_name = ".packaging"
$folder_path = "$((pwd).Path)\${folder_name}"
$file_name = "Packaging.targets"
$destination = "${folder_path}\${file_name}"

New-Item -ItemType Directory -Path $folder_path -Force
Copy-Item "${toolsPath}\${file_name}" -Destination $destination -Force

$folder = Get-SolutionFolder $folder_name
if ($folder -eq $null) {
	$folder = Add-SolutionFolder $folder_name
}

$folder.ProjectItems.AddFromFile($destination)
