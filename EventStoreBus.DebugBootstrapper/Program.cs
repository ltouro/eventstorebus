﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EventStoreBus.DebugBootstrapper
{
    class Program
    {
        static void Main(string[] args)
        {
            File.Delete("EventStoreBus.Config.dll");
            AppDomain.CurrentDomain.AssemblyResolve
                += (s, a) =>
                       {
                           var assemblyName = new AssemblyName(a.Name);
                           var resName = "EventStoreBus.DebugBootstrapper." + assemblyName.Name + ".dll";
                           var thisAssembly = Assembly.GetExecutingAssembly();
                           using (var input = thisAssembly.GetManifestResourceStream(resName))
                           {
                               return input != null
                                          ? Assembly.Load(StreamToBytes(input))
                                          : null;
                           }
                       };

            var deployer = new Deployer();
            deployer.Deploy();

            AppDomain domain = AppDomain.CreateDomain("Host");
            var runner = (Runner)domain.CreateInstanceAndUnwrap("EventStoreBus.DebugBootstrapper",
                                           "EventStoreBus.DebugBootstrapper.Runner");

            runner.Run();
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
            runner.Stop();
        }

        static byte[] StreamToBytes(Stream input)
        {
            var capacity = input.CanSeek ? (int)input.Length : 0;
            using (var output = new MemoryStream(capacity))
            {
                int readLength;
                var buffer = new byte[4096];

                do
                {
                    readLength = input.Read(buffer, 0, buffer.Length);
                    output.Write(buffer, 0, readLength);
                }
                while (readLength != 0);

                return output.ToArray();
            }
        }
    }
}
