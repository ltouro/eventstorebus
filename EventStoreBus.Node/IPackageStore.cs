﻿using System.Collections.Generic;
using System.IO;

namespace EventStoreBus.Node
{
    public interface IPackageStore
    {
        PackageDeployment DeployPackage(string deploymentId, PackageId packageId, string component);
        PackageDeployment GetDeployment(string deploymentId);
        IEnumerable<PackageDeployment> Deployments { get; }
        void UndeployPackage(PackageDeployment packageDeployment);
        IEnumerable<PackageDeployment> UpdateEnvironment(Stream newEnvironmentData);
    }
}