﻿using System;
using NuGet;

namespace EventStoreBus.Node
{
    public class PackageId
    {
        public readonly string UniqueName;
        public readonly SemanticVersion Version;

        public PackageId(string uniqueName, SemanticVersion version)
        {
            if (uniqueName == null) throw new ArgumentNullException("uniqueName");
            if (version == null) throw new ArgumentNullException("version");
            UniqueName = uniqueName;
            Version = version;
        }

        public override string ToString()
        {
            return UniqueName + " " + Version;
        }

        public bool Equals(PackageId other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.UniqueName, UniqueName) && Equals(other.Version, Version);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (PackageId)) return false;
            return Equals((PackageId) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((UniqueName != null ? UniqueName.GetHashCode() : 0)*397) ^ (Version != null ? Version.GetHashCode() : 0);
            }
        }

        public static bool operator ==(PackageId left, PackageId right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PackageId left, PackageId right)
        {
            return !Equals(left, right);
        }
    }
}