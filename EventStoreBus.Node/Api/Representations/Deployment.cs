﻿using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;

namespace EventStoreBus.Node.Api.Representations
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class Deployment
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string Component { get; set; }
        [DataMember]
        public string WSManagementUrl { get; set; }
        [DataMember]
        public string WebManagementUrl { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Status { get; set; }
        [DataMember]
        public HyperLink Undeploy { get; set; }
        [DataMember]
        public HyperLink Parent { get; set; }
        [DataMember]
        public HyperLink Self { get; set; }

        public Deployment()
        {
        }

        public Deployment(PackageDeployment deployment, UrlHelper url)
        {
            Component = deployment.Component;
            Id = deployment.Id;
            Service = deployment.PackageId.UniqueName;
            Version = deployment.PackageId.Version.ToString();
            Self = new HyperLink("self", url.Route("deployment", new {id = deployment.Id}), HttpMethod.Get);
            Start = new HyperLink("start", url.Route("instance", new {id = deployment.Id}), HttpMethod.Put);
            Stop = new HyperLink("stop", url.Route("instance", new {id = deployment.Id}), HttpMethod.Delete);
            Status = new HyperLink("status", url.Route("instance", new {id = deployment.Id}), HttpMethod.Get);
            Parent = new HyperLink("parent", url.Route("node", new {}), HttpMethod.Get);
            Undeploy = new HyperLink("undeploy", url.Route("deployment", new {id = deployment.Id}), HttpMethod.Delete);
            WebManagementUrl = string.Format("http://localhost:12345/{0}/rest", deployment.Id);
            WSManagementUrl = string.Format("http://localhost:12345/{0}/ws", deployment.Id);
        }
    }
}