﻿namespace EventStoreBus.Node.Api.Representations
{

    public enum InstanceState
    {
        Stopped,
        Running
    }
}