﻿using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;

namespace EventStoreBus.Node.Api.Representations
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class HyperLink
    {
        [DataMember]
        public string Relation { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Verb { get; set; }

        public HyperLink()
        {
        }

        public static HyperLink Put(string relation, UrlHelper urlHelper, string route, object routeParams)
        {
            return new HyperLink(relation, urlHelper.Route(route, routeParams), HttpMethod.Put);
        }

        public HyperLink(string relation, string url, HttpMethod verb)
        {
            Relation = relation;
            Url = url;
            Verb = verb.Method;
        }
    }
}