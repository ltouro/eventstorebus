﻿using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;

namespace EventStoreBus.Node.Api.Representations
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class Instance
    {
        [DataMember]
        public InstanceState State { get; set; }
        [DataMember]
        public HyperLink Deployment { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }

        public Instance()
        {            
        }

        public Instance(InstanceState state, string deploymentId, UrlHelper url)
        {
            State = state;
            Deployment = new HyperLink("parent", url.Route("deployment", new {id = deploymentId}), HttpMethod.Get);
            if (State == InstanceState.Running)
            {
                Stop = new HyperLink("stop", url.Route("instance", new { id = deploymentId }), HttpMethod.Delete);                
            }
            else
            {
                Start = new HyperLink("start", url.Route("instance", new { id = deploymentId }), HttpMethod.Put);
            }
            Manage = new HyperLink("manage", string.Format("http://localhost:12345/{0}/rest", deploymentId), HttpMethod.Get);
        }
    }
}