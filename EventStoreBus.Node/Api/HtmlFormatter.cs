﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using RazorEngine;
using RazorEngine.Templating;

namespace EventStoreBus.Node.Api
{
    public class HtmlFormatter : MediaTypeFormatter
    {
        public HtmlFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/xhtml+xml"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }

        public override Task WriteToStreamAsync(Type type, object value, System.IO.Stream writeStream, System.Net.Http.HttpContent content, System.Net.TransportContext transportContext)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var streamWriter = new StreamWriter(writeStream, System.Text.Encoding.UTF8);
                        var result = Razor.Resolve(type.Name + ".cshtml", value).Run(new ExecuteContext());
                        streamWriter.Write(result);
                        streamWriter.Flush();
                    });
        }
    }
}