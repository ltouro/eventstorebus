﻿using System;
using System.Web.Http.Filters;

namespace EventStoreBus.Node.Api.Resources
{
    public class UnhandledExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Console.WriteLine(context.Exception);
        }
    }
}