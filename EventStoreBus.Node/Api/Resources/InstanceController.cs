﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Web.Http;
using EventStoreBus.Node.Api.Representations;

namespace EventStoreBus.Node.Api.Resources
{
    public class InstanceController : BaseController
    {
        public InstanceController(IPackageStore packageStore, Uri baseUrl) : base(packageStore, baseUrl)
        {
        }

        public HttpResponseMessage Post(string id, string verb)
        {
            if (verb.ToLowerInvariant() == "put")
            {
                var originalResponse = Put(id);
                if (!originalResponse.IsSuccessStatusCode)
                {
                    return originalResponse;
                }
                var response = Request.CreateResponse(HttpStatusCode.SeeOther);
                response.Headers.Location = new Uri(BaseUrl, Url.Route<InstanceController>(new {id }));
                return response;
            }
            if (verb.ToLowerInvariant() == "delete")
            {
                var originalResponse = Delete(id);
                if (!originalResponse.IsSuccessStatusCode)
                {
                    return originalResponse;
                }
                var response = Request.CreateResponse(HttpStatusCode.SeeOther);
                response.Headers.Location = new Uri(BaseUrl, Url.Route<DeploymentController>(new {id }));
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        public HttpResponseMessage Put(string id)
        {
            var deployment = PackageStore.GetDeployment(id);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            if (deployment.GetState() != InstanceState.Stopped)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            deployment.Start();
            var instance = new Instance(InstanceState.Stopped, id, Url);
            return Request.CreateResponse(HttpStatusCode.Created, instance);
        }

        public HttpResponseMessage Get(string id)
        {
            var deployment = PackageStore.GetDeployment(id);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var instance = new Instance(deployment.GetState(), id, Url);
               
            return Request.CreateResponse(HttpStatusCode.OK, instance);
        }

        public HttpResponseMessage Delete(string id)
        {
            var deployment = PackageStore.GetDeployment(id);
            if (deployment == null || deployment.GetState() != InstanceState.Running)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            deployment.Stop();
            var responseMessage = Request.CreateResponse(HttpStatusCode.OK);
            return responseMessage;
        }
    }
}