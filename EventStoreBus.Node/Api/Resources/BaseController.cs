﻿using System;
using System.Web.Http;

namespace EventStoreBus.Node.Api.Resources
{
    public abstract class BaseController : ApiController
    {
        protected readonly IPackageStore PackageStore;
        protected readonly Uri BaseUrl;

        protected BaseController(IPackageStore packageStore, Uri baseUrl)
        {
            this.PackageStore = packageStore;
            this.BaseUrl = baseUrl;
        }
    }
}