﻿namespace EventStoreBus.Supervisor.Tests
{
    public class FakeUniqueIdGenerator : IUniqueIdGenerator
    {
        private readonly string[] values;
        private int index;

        public FakeUniqueIdGenerator(params string[] values)
        {
            this.values = values;
        }

        public string Generate()
        {
            var value = values[index%values.Length];
            index++;
            return value;
        }
    }
}