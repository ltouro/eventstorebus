﻿using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Supervisor.Tests
{
    public class StartProcessTests : TestCase<StartProcess>
    {
        [Test]
        public void When_starting_it_requests_start_of_all_deployments_immediately()
        {
            Given("Self")
                .When(x => x.Start("Parent", new[]
                    {
                        new StartRequest("N1", "D1"),
                        new StartRequest("N2", "D2")
                    }))
                .Then(
                    new StartSequenceStarted("Self","Parent"),
                    new StartRequested("Self", "D1", "N1", 5),
                    new StartRequested("Self", "D2", "N2", 5)
                );
        }

        [Test]
        public void When_one_of_many_tasks_succeeds_process_is_not_yet_considered_complete()
        {
            Given("Self",
                  new StartSequenceStarted("Self", "Parent"),
                  new StartRequested("Self", "D1", "N1", 5),
                  new StartRequested("Self", "D2", "N2", 5)
                )
                .When(x => x.HandleStartSuccess("D1"))
                .Then(
                    new StartSucceeded("Self", "D1"));
        }

        [Test]
        public void When_one_of_many_tasks_fails_it_is_retried()
        {
            Given("Self",
                  new StartSequenceStarted("Self", "Parent"),
                  new StartRequested("Self", "D1", "N1", 5),
                  new StartRequested("Self", "D2", "N2", 5)
                )
                .When(x => x.HandleStartFailure("D1"))
                .Then(
                    new StartFailed("Self", "D1", true),
                    new StartRequested("Self", "D1", "N1", 5));
        }

        [Test]
        public void When_all_tasks_complete_with_either_success_or_failure_process_is_considered_failed()
        {
            Given("Self", new StartProcess(2),
                  new StartSequenceStarted("Self", "Parent"),
                  new StartRequested("Self", "D1", "N1", 2),
                  new StartRequested("Self", "D2", "N2", 2),
                  new StartSucceeded("Self", "D2"),
                  new StartFailed("Self", "D1", true),
                  new StartRequested("Self", "D1", "N1", 2)
                )
                .When(x => x.HandleStartFailure("D1"))
                .Then(
                    new StartFailed("Self", "D1", false),
                    new StartSequenceFailed("Self", "Parent", new []{"D1"}));
        }

        [Test]
        public void When_a_task_fails_all_attempts_all_subsequent_failures_are_not_retried()
        {
            Given("Self", new StartProcess(2),
                  new StartSequenceStarted("Self", "Parent"),
                  new StartRequested("Self", "D1", "N1", 2),
                  new StartRequested("Self", "D2", "N2", 2),
                  new StartFailed("Self", "D1", true),
                  new StartRequested("Self", "D1", "N1", 2),
                  new StartFailed("Self", "D1", false)
                )
                .When(x => x.HandleStartFailure("D2"))
                .Then(
                    new StartFailed("Self", "D2", false),
                    new StartSequenceFailed("Self", "Parent", new[] { "D1", "D2" }));
        }

        [Test]
        public void When_all_tasks_complete_successfully_process_is_considered_successful()
        {
            Given("Self",
                  new StartSequenceStarted("Self", "Parent"),
                  new StartRequested("Self", "D1", "N1", 5),
                  new StartRequested("Self", "D2", "N2", 5),
                  new StartSucceeded("Self", "D2")
                )
                .When(x => x.HandleStartSuccess("D1"))
                .Then(
                    new StartSucceeded("Self", "D1"),
                    new StartSequenceSucceeded("Self", "Parent"));
        }
    }
}
// ReSharper restore InconsistentNaming
