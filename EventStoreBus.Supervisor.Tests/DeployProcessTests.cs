﻿using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Supervisor.Tests
{
    public class DeployProcessTests : TestCase<DeployProcess>
    {
        [Test]
        public void When_starting_it_requests_deployment_of_all_components_immediately()
        {
            var uniqueGenerator = new FakeUniqueIdGenerator("T1", "T2");
            Given("Self")
                .When(x => x.Start("Parent", new[]
                    {
                        new DeploymentRequest("N1", "S1", "C1", "1.0.0"),
                        new DeploymentRequest("N1", "S1", "C2", "1.0.0"),
                    }, uniqueGenerator))
                .Then(
                    new DeploymentSequenceStarted("Self", "Parent"),
                    new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1"),
                    new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2")
                );
        }

        [Test]
        public void When_one_of_many_tasks_succeeds_process_is_not_yet_considered_complete()
        {
            Given("Self",
                  new DeploymentSequenceStarted("Self", "Parent"),
                  new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1"),
                  new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2")
                )
                .When(x => x.HandleDeploySuccess("T1", "D1"))
                .Then(
                    new DeploymentSucceeded("Self", "D1", "T1")
                );
        }

        [Test]
        public void When_all_tasks_complete_successfully_process_is_considered_complete()
        {
            Given("Self",
                  new DeploymentSequenceStarted("Self", "Parent"),
                  new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1"),
                  new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2"),
                  new DeploymentSucceeded("Self", "D1", "T1")
                )
                .When(x => x.HandleDeploySuccess("T2", "D2"))
                .Then(
                    new DeploymentSucceeded("Self", "D2", "T2"),
                    new DeploymentSequenceSucceeded("Self", "Parent")
                );
        }

        [Test]
        public void When_deployment_task_fails_it_is_retried()
        {
            Given("Self",
                  new DeploymentSequenceStarted("Self", "Parent"),
                  new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1"),
                  new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2")
                )
                .When(x => x.HandleDeployFailure("T1", "D1", "U1"))
                .Then(
                    new DeploymentFailed("Self", "D1", "T1"),
                    new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1")
                        {
                            DeploymentId = "D1",
                            DeploymentUrl = "U1"
                        }
                );
        }

        [Test]
        public void When_deployment_task_failure_count_is_reached_the_process_is_rolled_back()
        {
            Given("Self", new DeployProcess(2),
                  new DeploymentSequenceStarted("Self", "Parent"),
                  new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1"),
                  new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2"),
                  new DeploymentRequested("Self", "T3", "N1", "S1", "1.0.0", "C3"),
                  new DeploymentSucceeded("Self", "D1", "T1"),
                  new DeploymentFailed("Self", "D2", "T2"),
                  new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2")
                )
                .When(x => x.HandleDeployFailure("T2", "D2", "U2"))
                .Then(
                    new DeploymentFailed("Self", "D2", "T2"),
                    new DeploymentSequenceRollbackInitiated("Self","Parent","C2","S1","N1","1.0.0"),
                    new UndeploymentRequested("Self", "D1", "N1"),
                    new UndeploymentRequested("Self", "D2", "N1")
                );
        }

        [Test]
        public void When_deployment_task_succeeds_but_sequence_is_rolling_back_the_successful_task_is_also_rolled_back()
        {
            Given("Self", new DeployProcess(1),
                  new DeploymentSequenceStarted("Self", "Parent"),
                  new DeploymentRequested("Self", "T1", "N1", "S1", "1.0.0", "C1"),
                  new DeploymentRequested("Self", "T2", "N1", "S1", "1.0.0", "C2"),
                  new DeploymentFailed("Self", "D2", "T2"),
                  new DeploymentFailed("Self", "D2", "T2"),
                  new DeploymentSequenceRollbackInitiated("Self", "Parent", "C2", "S1", "N1", "1.0.0"),
                  new UndeploymentRequested("Self", "D1", "N1"),
                  new UndeploymentRequested("Self", "D2", "N1")
                )
                .When(x => x.HandleDeploySuccess("T1", "D1"))
                .Then(
                    new DeploymentSucceeded("Self", "D1", "T1"),
                    new UndeploymentRequested("Self", "D1", "N1")
                );
        }
    }
}
// ReSharper restore InconsistentNaming
