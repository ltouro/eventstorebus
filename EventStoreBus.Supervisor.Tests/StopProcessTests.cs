﻿using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Supervisor.Tests
{
    public class StopProcessTests : TestCase<StopProcess>
    {
        [Test]
        public void When_starting_it_requests_start_of_all_deployments_immediately()
        {
            Given("Self")
                .When(x => x.Start("Parent", new[]
                    {
                        new StopRequest("N1", "D1"),
                        new StopRequest("N2", "D2")
                    }))
                .Then(
                    new StopSequenceStarted("Self","Parent"),
                    new StopRequested("Self", "D1", "N1", 5),
                    new StopRequested("Self", "D2", "N2", 5)
                );
        }

        [Test]
        public void When_one_of_many_tasks_succeeds_process_is_not_yet_considered_complete()
        {
            Given("Self",
                  new StopSequenceStarted("Self", "Parent"),
                  new StopRequested("Self", "D1", "N1", 5),
                  new StopRequested("Self", "D2", "N2", 5)
                )
                .When(x => x.HandleStopSuccess("D1"))
                .Then(
                    new StopSucceeded("Self", "D1"));
        }

        [Test]
        public void When_one_of_many_tasks_fails_it_is_retried()
        {
            Given("Self",
                  new StopSequenceStarted("Self", "Parent"),
                  new StopRequested("Self", "D1", "N1", 5),
                  new StopRequested("Self", "D2", "N2", 5)
                )
                .When(x => x.HandleStopFailure("D1"))
                .Then(
                    new StopFailed("Self", "D1", true),
                    new StopRequested("Self", "D1", "N1", 5));
        }

        [Test]
        public void When_all_tasks_complete_with_either_success_or_failure_process_is_considered_failed()
        {
            Given("Self", new StopProcess(2),
                  new StopSequenceStarted("Self", "Parent"),
                  new StopRequested("Self", "D1", "N1", 2),
                  new StopRequested("Self", "D2", "N2", 2),
                  new StopSucceeded("Self", "D2"),
                  new StopFailed("Self", "D1", true),
                  new StopRequested("Self", "D1", "N1", 2)
                )
                .When(x => x.HandleStopFailure("D1"))
                .Then(
                    new StopFailed("Self", "D1", false),
                    new StopSequenceFailed("Self", "Parent", new []{"D1"}));
        }

        [Test]
        public void When_a_task_fails_all_attempts_subsequent_failures_are_retried_normally()
        {
            Given("Self", new StopProcess(2),
                  new StopSequenceStarted("Self", "Parent"),
                  new StopRequested("Self", "D1", "N1", 2),
                  new StopRequested("Self", "D2", "N2", 2),
                  new StopFailed("Self", "D1", true),
                  new StopRequested("Self", "D1", "N1", 2),
                  new StopFailed("Self", "D1", false)
                )
                .When(x => x.HandleStopFailure("D2"))
                .Then(
                    new StopFailed("Self", "D2", true),
                    new StopRequested("Self", "D2", "N2", 2));
        }

        [Test]
        public void When_all_tasks_complete_successfully_process_is_considered_successful()
        {
            Given("Self",
                  new StopSequenceStarted("Self", "Parent"),
                  new StopRequested("Self", "D1", "N1", 5),
                  new StopRequested("Self", "D2", "N2", 5),
                  new StopSucceeded("Self", "D2")
                )
                .When(x => x.HandleStopSuccess("D1"))
                .Then(
                    new StopSucceeded("Self", "D1"),
                    new StopSequenceSucceeded("Self", "Parent"));
        }
    }
}
// ReSharper restore InconsistentNaming
