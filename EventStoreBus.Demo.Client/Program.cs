﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStoreBus.Demo.Views;
using Raven.Client.Document;

namespace EventStoreBus.Demo.Client
{
    class Program
    {
        private static DocumentStore documentStore;

        static void Main(string[] args)
        {
            //NonAdminHttp.EnsureCanListenToWhenInNonAdminContext(8080);
            //var embeddedRavenStore = new EmbeddableDocumentStore()
            //{
            //    RunInMemory = true,
            //    UseEmbeddedHttpServer = true,

            //};
            //embeddedRavenStore.Initialize();

            //documentStore = new DocumentStore()
            //                    {
            //                        Url = "http://localhost:8080"
            //                    };
            //documentStore.Initialize();

            Console.WriteLine("Type R<number> to generate <number> events or 'exit' to close.");            

            var tcpEndpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1113);
            using (var connection = EventStoreConnection.Create(ConnectionSettings.Create().LimitReconnectionsTo(int.MaxValue)))
            {
                connection.Connect(tcpEndpoint);
                while (true)
                {
                    var line = Console.ReadLine();
                    if (string.IsNullOrEmpty(line))
                    {
                        InspectView();
                    }
                    else if (line.StartsWith("R"))
                    {
                        GenerateEvents(connection, line);
                    }
                    else if (line.ToLowerInvariant() == "exit")
                    {
                        break;
                    }
                }
            }
        }

        private static void GenerateEvents(EventStoreConnection connection, string line)
        {
            var count = int.Parse(line.Substring(1));
            var random = new Random();
            var aggregateCount = count/10;
            var aggregateIds = Enumerable.Range(0, aggregateCount).Select(x => Guid.NewGuid().ToString()).ToList();

            Console.Write("Generating {0} events...", count);
            DateTime now = DateTime.Now;
            for (int i = 0; i < count; i ++)
            {
                var aggregateId = aggregateIds[random.Next(aggregateCount)];
                AppendEvent(connection, i.ToString(), aggregateId, now, i == count - 1);
            }
            Console.WriteLine("Done.");
        }

        private static void InspectView()
        {
            using (var sessoin = documentStore.OpenSession())
            {
                var someView = sessoin.Load<SomeView>("someView/single");
                if (someView != null)
                {
                    Console.WriteLine("The vlaues are " + string.Join(", ", someView.Values));
                }
                else
                {
                    Console.WriteLine("No value yet");
                }
            }
        }

        private static Task AppendEvent(EventStoreConnection connection, string value, string id, DateTime initialTimestamp, bool printTime)
        {
            var evnt = new DefaultSerializer().Serialize(Guid.NewGuid(), new SomeExternalEvent()
            {
                Value = value,
                Id = id,
                InitialTimestamp = initialTimestamp,
                PrintTime = printTime
            }, null);

            return connection.AppendToStreamAsync("External+TestEventStream", ExpectedVersion.Any, new[] { evnt });
        }
    }
}
