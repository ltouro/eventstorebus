﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using EventStoreBus.Config;
using NLog;
using Topshelf;
using Topshelf.Runtime;
using Environment = System.Environment;
using LogManager = EventStoreBus.Logging.LogManager;

namespace EventStoreBus.Host
{
    public class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            HostFactory.Run(x =>
            {
                x.Service<Host>(s =>
                {
                    s.ConstructUsing(hs => new Host());
                    s.WhenStarted(svc => svc.Start());
                    s.WhenStopped(svc => svc.Stop());
                });
                TryInitLogging();
                x.UseNLog(new LogFactory(NLog.LogManager.Configuration));
                x.RunAsLocalSystem();
                x.SetDescription("EventStoreBus host");
                x.SetDisplayName("EventStoreBus host service");
                x.SetServiceName("EventStoreBus.Host");
            });
        }

        public static void TryInitLogging()
        {
            try
            {
                Deployment deployment;
                var deploymentDescriptorSerializer = new XmlSerializer(typeof(Deployment));
                using (var deploymentDescriptorStream = File.OpenRead("deployment.xml"))
                {
                    deployment = (Deployment)deploymentDescriptorSerializer.Deserialize(deploymentDescriptorStream);
                }
                LogManager.Init(deployment.Service, deployment.LogDirectory);
            }
            catch (Exception ex)
            {
                LogManager.Init("FailedInit", Environment.CurrentDirectory);
                LogManager.GetLogger("GLOBAL").FatalException(ex, "Failure initializing logging configuration.");
            }
            
        }
    }
}
