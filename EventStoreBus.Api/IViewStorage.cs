﻿namespace EventStoreBus.Views.Api
{
    public interface IViewStorage<TView>
    {
        string CollectionName { get; }
    }
}