﻿using System;

namespace EventStoreBus.Api
{
    public interface IEnvelope
    {
        string Store { get; }
        string StreamId { get; }
        Guid EventId { get; }
        int Sequence { get; }
    }
}