﻿using System;
using System.Linq;

namespace EventStoreBus.Views.Api
{
    public interface IViewReaderSession<out TView> : IDisposable
        where TView : class 
    {
        TView Load(object viewId);
        IQueryable<TView> Query();
    }
}