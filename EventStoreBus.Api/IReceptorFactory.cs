﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Receptors.Api
{
    public interface IReceptorFactory
    {
        object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders);
        void Release(object receptor);
    }
}