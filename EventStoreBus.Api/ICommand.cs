﻿using System;

namespace EventStoreBus.Api
{
    public interface ICommand
    {
        string Id { get; }
    }
}