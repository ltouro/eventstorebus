﻿using System;
using EventStoreBus.Api;

namespace EventStoreBus.Views.Api
{
    public interface IViewWriterSession<TView> : IDisposable
        where TView : class 
    {
        void CreateOrUpdate(object viewId, IEnvelope envelope, Func<TView> create, Action<TView> update);
        void Update(object viewId, IEnvelope envelope, Action<TView> update);
        void Create(object viewId, IEnvelope envelope, TView view);
        void Delete(object viewId, IEnvelope envelope);
    }
}