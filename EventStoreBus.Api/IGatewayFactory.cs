﻿using System;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Api
{
    public interface IGatewayFactory
    {
        object Create(Type gatewayImplementation, ICommandSender commandSender, IViewReaders documentStore);
        void Release(object projection);
    }
}