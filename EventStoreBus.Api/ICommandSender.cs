﻿namespace EventStoreBus.Api
{
    public interface ICommandSender
    {
        void SendTo(string componentName, object command);
        void SendTo<T>(object command);
    }
}