﻿using System;
using EventStoreBus.Api;

namespace EventStoreBus.Views.Api
{
    public static class ViewWriterSessionExtensions
    {
        public static void CreateOrUpdateSingle<TView>(this IViewWriterSession<TView> writer, IEnvelope envelope, Action<TView> update)
            where TView : class, new()
        {
            writer.CreateOrUpdate("single", envelope,
                                  () =>
                                      {
                                          var newView = new TView();
                                          update(newView);
                                          return newView;
                                      }, update);
        }

        public static void CreateOrUpdate<TView>(this IViewWriterSession<TView> writer, object viewId, IEnvelope envelope, Action<TView> update) 
            where TView : class, new()
        {
            writer.CreateOrUpdate(viewId, envelope,
                                  () =>
                                      {
                                          var newView = new TView();
                                          update(newView);
                                          return newView;
                                      }, update);
        }
    }
}