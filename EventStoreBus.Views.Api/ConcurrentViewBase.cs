﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventStoreBus.Views.Api
{
    public abstract class ConcurrentViewBase
    {
        public List<Guid> ProcessedEvents = new List<Guid>();

        public virtual int HisotrySize
        {
            get { return 20; }
        }

        public bool ShouldHandle(Guid eventId)
        {
            return !ProcessedEvents.Contains(eventId);
        }

        public void OnHandled(Guid eventId)
        {
            ProcessedEvents.Add(eventId);
            if (ProcessedEvents.Count > HisotrySize)
            {
                ProcessedEvents.RemoveAt(0);
            }
        }
    }
}