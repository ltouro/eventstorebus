﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Demo.Aggregates
{
    public class SomeComponentFactory : IAggregateComponentFactory
    {
        public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
        {
            return new SomeComponent(repositories.Get<SomeAggregate>());
        }

        public void Release(object component)
        {
        }
    }
}