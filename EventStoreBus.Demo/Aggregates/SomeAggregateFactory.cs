﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;

namespace EventStoreBus.Demo.Aggregates
{
    public class SomeAggregateFactory : IAggregateFactory<SomeAggregate>
    {
        public SomeAggregate CreateRoot()
        {
            return new SomeAggregate();
        }

        public IEnumerable<IPort> CreatePorts(ICommandSender commandSender)
        {
            return Enumerable.Empty<IPort>();
        }

        public void ReleasePort(IPort port)
        {
        }
    }
}