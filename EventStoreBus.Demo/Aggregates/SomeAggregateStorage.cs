﻿using System;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;

namespace EventStoreBus.Demo.Aggregates
{
    public class SomeAggregateStorage : IAggregateStorage<SomeAggregate>
    {
        public string MakeStreamId(string aggregateId)
        {
            return aggregateId;
        }
    }
}