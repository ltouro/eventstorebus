﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Demo.Aggregates
{
    public class SomeAggregate : Aggregate<SomeAggregateState>
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger(); 

        public void CountCharacters(string text, DateTime initialTimestamp, bool printTime)
        {
            if (text == "bad command")
            {
                logger.Error("Bad command");
                throw new InvalidOperationException(text);
            }
            Apply(new SomeEvent()
                      {
                          SourceId = Id,
                          InitialTimestamp = initialTimestamp,
                          PrintTime = printTime,
                          Value = text
                      });
        }
    }
}