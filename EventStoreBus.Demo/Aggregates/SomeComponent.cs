﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Demo.Aggregates
{
    public class SomeComponent
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly IRepository<SomeAggregate> repository;

        public SomeComponent(IRepository<SomeAggregate> repository)
        {
            this.repository = repository;
        }

        public void When(SomeCommand someCommand)
        {
            repository.Invoke(someCommand.AggregateId, someCommand.Id,
                x => x.CountCharacters(someCommand.Value, someCommand.InitialTimestamp, someCommand.PrintTime));

            if (someCommand.PrintTime)
            {
                var diff = DateTime.Now - someCommand.InitialTimestamp;
                logger.Info("Command handlers processed batch in {0} ms", diff.TotalMilliseconds);
            }
        }
    }
}