﻿using System.Collections.Generic;
using EventStoreBus.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Demo.Views
{
    public class SomeView : ConcurrentViewBase
    {
        public List<string> Values = new List<string>();
    }
}