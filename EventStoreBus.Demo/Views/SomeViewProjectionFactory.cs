﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Demo.Views
{
    public class SomeViewProjectionFactory : IViewProjectionFactory
    {
        public object Create(Type viewProjectionImplementation, IViewWriters viewWriters)
        {
            return new SomeViewProjection(viewWriters.GetWriter<SomeView, SomeViewStorage>());
        }

        public void Release(object projection)
        {
        }
    }
}