﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Demo.Views
{
    public class SomeViewProjection
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger(); 

        private readonly IViewWriter<SomeView> viewWriter;

        public SomeViewProjection(IViewWriter<SomeView> viewWriter)
        {
            this.viewWriter = viewWriter;
        }

        public void When(Envelope<SomeEvent> evnt)
        {
            using (var session = viewWriter.OpenSession())
            {
                session.CreateOrUpdate(evnt.Payload.SourceId, evnt,
                                       () => new SomeView(),
                                       x => x.Values.Add(evnt.Payload.Value));

                if (evnt.Payload.PrintTime)
                {
                    var diff = DateTime.Now - evnt.Payload.InitialTimestamp;
                    logger.Info("Views processed batch in {0} ms", diff.TotalMilliseconds);
                }
            }

            
        }
    }
}