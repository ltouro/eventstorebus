﻿using System;
using System.Runtime.Serialization;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;

namespace EventStoreBus.Demo
{
    [DataContract]
    [Version(1)]
    public class SomeEvent : IEvent
    {
        [DataMember]
        public string SourceId { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public DateTime InitialTimestamp { get; set; }
        [DataMember]
        public bool PrintTime { get; set; }
    }
}