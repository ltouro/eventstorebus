﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Instance
    {
        [DataMember]
        public DeploymentState State { get; set; }
        [DataMember]
        public HyperLink Deployment { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
    }
}