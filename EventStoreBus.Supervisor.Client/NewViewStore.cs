﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewViewStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ConnectionString { get; set; }
    }
}