﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class HyperLink
    {
        [DataMember]
        public string Relation { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Verb { get; set; }

    }
}