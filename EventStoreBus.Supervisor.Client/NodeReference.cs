﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NodeReference
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public HyperLink Details { get; set; }
    }
}