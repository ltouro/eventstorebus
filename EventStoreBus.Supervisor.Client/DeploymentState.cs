﻿namespace EventStoreBus.Supervisor.Client
{
    public enum DeploymentState
    {
        DeployPending,
        Stopped,
        StartPending,
        Running,
        StopPending,
        UndeployPending
    }
}