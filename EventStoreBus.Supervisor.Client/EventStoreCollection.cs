﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class EventStoreCollection
    {
        [DataMember]
        public List<EventStoreReference> Stores { get; set; }
        
        [DataMember]
        public HyperLink Register { get; set; }
    }
}