﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ServiceRepository
    {
        [DataMember]
        public List<ServiceReference> Services { get; set; }

        [DataMember]
        public HyperLink Register { get; set; }
    }
}