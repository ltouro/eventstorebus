﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Deployment
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string Component { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Status { get; set; }
        [DataMember]
        public HyperLink Undeploy { get; set; }
        [DataMember]
        public HyperLink Parent { get; set; }
        [DataMember]
        public HyperLink Self { get; set; }
    }
}