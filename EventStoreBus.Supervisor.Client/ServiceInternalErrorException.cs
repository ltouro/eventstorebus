﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [Serializable]
    public class ServiceInternalErrorException : Exception
    {
        public ServiceInternalErrorException()
        {
        }

        public ServiceInternalErrorException(string message) : base(message)
        {
        }

        public ServiceInternalErrorException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ServiceInternalErrorException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}