﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Service
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string CurrentVersion { get; set; }

        [DataMember]
        public HyperLink SetVersion { get; set; }

        [DataMember]
        public HyperLink Redeploy { get; set; }

        [DataMember]
        public HyperLink Upgrade { get; set; }
    }
}