﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ViewStoreReference
    {
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public HyperLink Details { get; set; }
    }
}