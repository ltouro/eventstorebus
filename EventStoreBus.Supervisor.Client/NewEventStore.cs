﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewEventStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int Port { get; set; }

        public NewEventStore(string name, string address, int port)
        {
            Name = name;
            Address = address;
            Port = port;
        }
    }
}