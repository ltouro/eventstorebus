﻿using System.Collections.Generic;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public abstract class AggregateState
    {
        private readonly Dispatcher dispatcher;
        private readonly List<AggregateState> subStates = new List<AggregateState>();

        protected AggregateState()
        {
            dispatcher = new Dispatcher(GetType());
        }

        public void AddSubState(AggregateState subState)
        {
            subStates.Add(subState);
        }

        internal void RebuildFromHistory(IEnumerable<IEvent> events)
        {
            foreach (var evnt in events)
            {
                Mutate(evnt);
                foreach (var subState in subStates)
                {
                    subState.Mutate(evnt);
                }
            }            
        }

        internal void Mutate(IEvent evnt)
        {
            if (dispatcher.CanHandle(evnt))
            {
                dispatcher.Handle(this, evnt);
            }
        }
    }
}