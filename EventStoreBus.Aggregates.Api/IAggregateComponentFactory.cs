using System;

namespace EventStoreBus.Aggregates.Api
{
    public interface IAggregateComponentFactory
    {
        object Create(Type componentImplementation, IRepositories repositories, IFactories factories);
        void Release(object component);
    }
}