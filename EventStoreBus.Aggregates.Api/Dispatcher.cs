﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EventStoreBus.Aggregates.Api
{
    public class Dispatcher
    {
        private readonly Dictionary<Type, MethodInfo> handlingMethods;

        public Dispatcher(Type targetType)
            : this(targetType, "When")
        {
        }

        public Dispatcher(Type targetType, string handlerMethodName)
        {
            handlingMethods = targetType.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.Name == handlerMethodName)
                .ToDictionary(x => x.GetParameters()[0].ParameterType, x => x);
        }

        public bool CanHandle(object payload)
        {
            return handlingMethods.ContainsKey(payload.GetType());
        }

        public object Handle(object target, params object[] arguments)
        {
            var method = handlingMethods[arguments[0].GetType()];
            return method.Invoke(target, arguments);
        }
    }
}