﻿using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public class Port<T> : IPort
        where T : AggregateState, new()
    {
        private readonly Dispatcher dispatcher;
        protected T State;

        protected Port()
        {
            dispatcher = new Dispatcher(GetType());
        }

        void IPort.LoadState(object state)
        {
            State = (T) state;
        }

        void IPort.Handle(IEvent evnt)
        {
            if (dispatcher.CanHandle(evnt))
            {
                dispatcher.Handle(this, evnt);
            }
        }
    }
}