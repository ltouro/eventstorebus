﻿using System;
using System.Collections.Generic;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public class Aggregate<T> : IAggregate
        where T : AggregateState, new()
    {
        private readonly List<IEvent> changes = new List<IEvent>();
        protected string Id { get; private set; }
        private T state;
        public T State
        {
            get { return state ?? (state = new T()); }
            private set { state = value; }
        }

        void IAggregate.SetId(string id)
        {
            Id = id;
        }

        void IAggregate.LoadState(IEnumerable<IEvent> events)
        {
            State.RebuildFromHistory(events);
        }

        IEnumerable<IEvent> IAggregate.Changes
        {
            get { return changes; }
        }

        object IAggregate.State
        {
            get { return State; }
        }


        protected void Apply(IEvent evnt)
        {
            changes.Add(evnt);
            State.Mutate(evnt);
        }
    }
}