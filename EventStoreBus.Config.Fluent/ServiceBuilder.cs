﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace EventStoreBus.Config.Fluent
{
    public class ServiceBuilder
    {
        private readonly Service service;

        public ServiceBuilder(string name)
        {
            service = new Service()
                          {
                              Name = name
                          };
        }

        public ServiceBuilder AddReceptor<T>(string name, Action<IConfigureReceptor> receptorConfig)
        {
            var receptor = new Receptor();
            receptor.Name = name;

            var configurator = new ConfigureReceptor(receptor);
            receptorConfig(configurator);
            service.Components.Add(receptor);
            return this;
        }

        public string GenerateXml()
        {
            var serializer = new XmlSerializer(typeof (Service));

            var buffer = new StringBuilder();
            using (var writer = new StringWriter(buffer))
            {
                serializer.Serialize(writer, service);
                writer.Flush();
            }
            return buffer.ToString();
        }
    }
}
