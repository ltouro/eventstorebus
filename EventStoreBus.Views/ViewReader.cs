﻿using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class ViewReader<TView, TViewStorage> : IViewReader<TView>
        where TView : class
        where TViewStorage : IViewStorage<TView>, new()
    {
        private readonly IDocumentStore documentStore;

        public ViewReader(IDocumentStore documentStore)
        {
            this.documentStore = documentStore;
        }

        public IViewReaderSession<TView> OpenSession()
        {
            return new ViewReaderSession<TView, TViewStorage>(documentStore.OpenSession());
        }
    }
}