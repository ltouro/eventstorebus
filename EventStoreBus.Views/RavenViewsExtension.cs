﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventStoreBus.Views
{
    public class RavenViewsExtension : IExtension
    {
        private static readonly ITypeAbbreviationProvider[] abbreviationProviders = new ITypeAbbreviationProvider[]
            {
                new RavenDBTypeAbbreviationProvider()
            };

        public IEnumerable<ITypeWrapper> TypeWrappers
        {
            get { return Enumerable.Empty<ITypeWrapper>(); }
        }

        public IEnumerable<ITypeAbbreviationProvider> AbbreviationProviders
        {
            get { return abbreviationProviders; }
        }

        private class RavenDBTypeAbbreviationProvider : ITypeAbbreviationProvider
        {
            public bool KnowsAbbreviationFor(string typeName)
            {
                return string.Compare(@"RavenDB", typeName, StringComparison.OrdinalIgnoreCase) == 0;
            }

            public string GetFullName(string abbreviation)
            {
                return typeof (RavenViewEngine).AssemblyQualifiedName;
            }
        }
    }
}