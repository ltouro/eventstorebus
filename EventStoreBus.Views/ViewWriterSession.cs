﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;
using Raven.Abstractions.Exceptions;
using Raven.Client;

namespace EventStoreBus.Views
{
    /// <summary>
    /// Intentionally not safe wrt concurrent writes. Will throw ConcurrencyException somethimes but this
    /// is fine as the event handling will re re-tried.
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TViewStorage"></typeparam>
    public abstract class ViewWriterSession<TView, TViewStorage> : IViewWriterSession<TView>
        where TView : class 
        where TViewStorage : IViewStorage<TView>, new()
    {
        private static readonly TViewStorage storage = new TViewStorage();

        private readonly IDocumentSession documentSession;

        protected ViewWriterSession(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;
        }

        public void CreateOrUpdate(object viewId, IEnvelope envelope, Func<TView> create, Action<TView> update)
        {
            var viewDocumentId = GetDocumentId(viewId);
            var view = documentSession.Load<TView>(viewDocumentId);
            if (view == null)
            {
                view = create();
                OnHandled(view, envelope);
                documentSession.Store(view, Guid.Empty, viewDocumentId);
            }
            else
            {
                if (ShouldHandle(view, envelope))
                {
                    update(view);
                    OnHandled(view, envelope);
                }
            }
            documentSession.SaveChanges();
        }

        

        public void Update(object viewId, IEnvelope envelope, Action<TView> update)
        {
            var viewDocumentId = GetDocumentId(viewId);
            var view = documentSession.Load<TView>(viewDocumentId);
            if (view == null)
            {
                throw new InvalidOperationException("Projection expected view to be already created: " + viewDocumentId);
            }
            if (ShouldHandle(view, envelope))
            {
                update(view);
                OnHandled(view, envelope);
            }
            documentSession.SaveChanges();
        }

        public void Create(object viewId, IEnvelope envelope, TView view)
        {
            var viewDocumentId = GetDocumentId(viewId);
            var existingView = documentSession.Load<TView>(viewDocumentId);
            if (existingView != null)
            {
                if (ShouldHandle(existingView, envelope))
                {
                    throw new InvalidOperationException("Projection expected view to not be already created: " + viewDocumentId);
                }
            }
            else
            {
                try
                {
                    OnHandled(view, envelope);
                    documentSession.Store(view, Guid.Empty, viewDocumentId);
                    documentSession.SaveChanges();
                }
                catch (ConcurrencyException)
                {
                    throw new InvalidOperationException("Projection expected view to not be already created: " + viewDocumentId);
                }
            }
        }

        public void Delete(object viewId, IEnvelope envelope)
        {
            var viewDocumentId = GetDocumentId(viewId);
            var view = documentSession.Load<TView>(viewDocumentId);
            if (view == null)
            {
                return;
            }
            documentSession.Delete(view);
            documentSession.SaveChanges();
        }

        private static string GetDocumentId(object viewId)
        {
            return string.Format("{0}/{1}", storage.CollectionName, viewId);
        }

        public void Dispose()
        {
            documentSession.Dispose();
        }

        protected abstract void OnHandled(object view, IEnvelope envelope);
        protected abstract bool ShouldHandle(object view, IEnvelope envelope);
    }
}