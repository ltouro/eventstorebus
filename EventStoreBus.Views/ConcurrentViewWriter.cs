﻿using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class ConcurrentViewWriter<TView, TViewStorage> : IViewWriter<TView>
        where TView : class 
        where TViewStorage : IViewStorage<TView>, new()
    {
        private readonly IDocumentStore documentStore;

        public ConcurrentViewWriter(IDocumentStore documentStore)
        {
            this.documentStore = documentStore;
        }

        public IViewWriterSession<TView> OpenSession()
        {
            var session = documentStore.OpenSession();
            session.Advanced.UseOptimisticConcurrency = true;
            return new ConcurrentViewWriterSession<TView, TViewStorage>(session);
        }
    }
}