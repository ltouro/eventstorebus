﻿using System;
using System.Linq;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Views.Api;
using Raven.Client;
using Raven.Client.Document;

namespace EventStoreBus.Views
{
    public class RavenViewEngine : IViewEngine
    {
        private readonly string storeName;
        private IDocumentStore documentStore;
        private readonly string url;
        private readonly string defaultDatabase;

        public RavenViewEngine(string storeName, string connectionString)
        {
            if (storeName == null) throw new ArgumentNullException("storeName");
            if (connectionString == null) throw new ArgumentNullException("connectionString");

            this.storeName = storeName;
            var connectionStringValues = connectionString
                .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries))
                .ToDictionary(x => x[0].ToLowerInvariant(), x => x[1]);

            url = connectionStringValues["url"];
            defaultDatabase = connectionStringValues["database"];
        }

        public IViewReaders CreateReaders()
        {
            return new ViewReaders(documentStore);
        }

        public IViewWriters CreateWriters()
        {
            return new ViewWriters(documentStore);
        }

        public State State
        {
            get
            {
                return new State("RavenDB view engine (" + storeName + ")", GetType())
                    .WithProperty("Name", storeName)
                    .WithProperty("Url", url)
                    .WithProperty("Database", defaultDatabase);
            }
        }

        public void Start()
        {
            if (documentStore != null)
            {
                return;
            }
            documentStore = Connect();
        }

        private DocumentStore Connect()
        {
            var store = new DocumentStore
            {
                Url = url,
                DefaultDatabase = defaultDatabase
            };
            store.Initialize();
            return store;
        }

        public void Stop()
        {
            if (documentStore == null)
            {
                return;
            }
            documentStore.Dispose();
            documentStore = null;
        }
    }
}