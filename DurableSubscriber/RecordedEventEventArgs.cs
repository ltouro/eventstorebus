﻿using System;

namespace DurableSubscriber
{
    public class RecordedEventEventArgs<T> : EventArgs
    {
        public readonly object Event;
        public readonly T Position;

        public RecordedEventEventArgs(object evnt, T position)
        {
            Event = evnt;
            Position = position;
        }
    }
}