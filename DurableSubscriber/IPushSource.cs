﻿using System;
using DurableSubscriber.Diagnostics;

namespace DurableSubscriber
{
    public interface IPushSource<T> : IInstrumentable
    {
        event EventHandler<RecordedEventEventArgs<T>> Event;
        event EventHandler<SubscriptionDroppedEventArgs<T>> SubscriptionDropped;
        void Start();
        void Stop();
    }
}