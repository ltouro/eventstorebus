﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;
using EventStoreBus.Logging;
using ILogger = EventStoreBus.Logging.ILogger;

namespace DurableSubscriber
{
    public class EventBuffer<T> : IInstrumentable
    {
        private static readonly ILogger logger = LogManager.GetLogger("EventBuffer");

        private readonly IEventIdentityExtractor eventIdentityExtractor;
        private BlockingCollection<Tuple<object, T>> store;
        private long counter;

        public EventBuffer(IEventIdentityExtractor eventIdentityExtractor)
        {
            this.eventIdentityExtractor = eventIdentityExtractor;
        }

        public void Add(Tuple<object, T> evnt)
        {
            store.Add(evnt);
        }

        public void Complete()
        {
            store.CompleteAdding();
        }

        public Task StartProcessing(IEnumerable<Tuple<object, T>> lastPulledSlice, Action<Tuple<object, T>> handler)
        {
            var processed = new HashSet<object>(lastPulledSlice.Select(x => eventIdentityExtractor.GetIdentity(x.Item1)));

            var task = Task.Factory.StartNew(() =>
                                      {
                                          foreach (var evnt in store.GetConsumingEnumerable())
                                          {
                                              try
                                              {
                                                  var identity = eventIdentityExtractor.GetIdentity(evnt.Item1);
                                                  if (processed.Contains(identity))
                                                  {
                                                      continue;
                                                  }
                                                  handler(evnt);
                                                  counter++;
                                              }
                                              catch (Exception ex)
                                              {
                                                  logger.FatalException(ex, "Unexpected exception.");
                                                  //Skip
                                              }
                                          }
                                      });
            return task;
        }

        public void Reset()
        {
            store = new BlockingCollection<Tuple<object, T>>();
        }

        public State State
        {
            get
            {
                return new State("Buffer", GetType())
                    .WithAttribute("Length", () => store.Count)
                    .WithAttribute("Processed", () => counter);
            }
        }
    }
}