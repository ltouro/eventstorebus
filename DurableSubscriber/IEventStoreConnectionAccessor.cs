﻿using System;
using DurableSubscriber.Diagnostics;

namespace DurableSubscriber
{
    public interface IEventStoreConnectionAccessor : IInstrumentable
    {
        bool TryInvoke(Action<IEventStoreConnection> action);
        void Invoke(Action<IEventStoreConnection> action);
        bool TryInvoke<T>(Func<IEventStoreConnection, T> func, out T result);
        T Invoke<T>(Func<IEventStoreConnection, T> func);
    }
}