﻿namespace DurableSubscriber
{
    public interface ICheckpointingStrategy<T>
    {
        void NotifyEventsProcessed(T until);
        T LoadState();
    }
}