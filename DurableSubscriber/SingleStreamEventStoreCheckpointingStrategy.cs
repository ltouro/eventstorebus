﻿using System;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class SingleStreamEventStoreCheckpointingStrategy : EventStoreCheckpointingStrategy<int>
    {
        public SingleStreamEventStoreCheckpointingStrategy(IEventStoreConnectionAccessor connection, string streamName)
            : base(connection, streamName)
        {
        }

        protected override byte[] SerializeState(int state)
        {
            return BitConverter.GetBytes(state);
        }

        protected override int DeserializeState(byte[] data)
        {
            return BitConverter.ToInt32(data,0);
        }

        protected override int GetBeginState()
        {
            return StreamPosition.Start;
        }
    }
}