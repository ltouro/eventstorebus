﻿namespace DurableSubscriber.Diagnostics
{
    public class PropertyValue
    {
        private readonly string name;
        private readonly string value;

        public PropertyValue(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        public string Value
        {
            get { return value; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}