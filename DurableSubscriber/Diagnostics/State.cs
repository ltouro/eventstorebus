using System;
using System.Collections.Generic;
using System.Linq;

namespace DurableSubscriber.Diagnostics
{
    public class State
    {
        private readonly List<IAttributeValue> attributes = new List<IAttributeValue>();
        private readonly List<PropertyValue> properties = new List<PropertyValue>();
        private readonly Type type;
        private readonly string name;
        private string domainPrefix;
        private readonly List<State> childStates = new List<State>();

        public State(string name, Type type)
        {
            this.type = type;
            this.name = name;
        }

        public State WithAttribute<T>(string attributeName, Func<T> value)
        {
            attributes.Add(new AttributeValue<T>(attributeName, value));
            return this;
        }

        public State WithProperty(string propertyName, string value)
        {
            properties.Add(new PropertyValue(propertyName, value));
            return this;
        }

        public IEnumerable<PropertyValue> Properties
        {
            get { return properties; }
        }

        public IEnumerable<State> ChildStates
        {
            get { return childStates; }
        }

        public IEnumerable<IAttributeValue> Attributes
        {
            get { return attributes; }
        }

        public string Name
        {
            get { return name; }
        }

        public Type Type
        {
            get { return type; }
        }

        public string DomainPrefix
        {
            get { return domainPrefix; }
        }

        public State WithChildStates(IEnumerable<IInstrumentable> children)
        {
            childStates.AddRange(children.Select(x => x.State));
            return this;
        }

        public State WithChild(State state)
        {
            childStates.Add(state);
            return this;
        }

        public State WithChild(IInstrumentable child)
        {
            return WithChild(child.State);
        }

        public State WithDomainPrefix(string domainPrefix)
        {
            this.domainPrefix = domainPrefix;
            return this;
        }
    }
}