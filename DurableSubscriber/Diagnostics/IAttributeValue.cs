﻿using System;

namespace DurableSubscriber.Diagnostics
{
    public interface IAttributeValue
    {
        object Value { get; }
        string Name { get; }
        Type ValueType { get; }
    }
}