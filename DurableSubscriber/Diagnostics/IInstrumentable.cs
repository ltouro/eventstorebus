﻿namespace DurableSubscriber.Diagnostics
{
    public interface IInstrumentable
    {
        State State { get;}
    }
}