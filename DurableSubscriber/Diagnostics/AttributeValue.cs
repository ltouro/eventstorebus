﻿using System;

namespace DurableSubscriber.Diagnostics
{
    public class AttributeValue<T> : IAttributeValue
    {
        private readonly string name;
        private readonly Func<T> value;        

        public AttributeValue(string name, Func<T> value)
        {
            this.name = name;
            this.value = value;
        }

        public object Value
        {
            get { return value(); }
        }

        public string Name
        {
            get { return name; }
        }

        public Type ValueType
        {
            get { return typeof (T); }
        }
    }
}