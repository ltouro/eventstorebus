﻿using System;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;
using EventStore.ClientAPI.Exceptions;

namespace DurableSubscriber
{
    public class DefaultEventStoreConnectionAccessor : IEventStoreConnectionAccessor
    {
        private readonly IEventStoreConnection connection;

        public DefaultEventStoreConnectionAccessor(EventStoreConnection connection)
        {
            this.connection = new DefualtEventStoreConnection(connection);
        }

        public void Invoke(Action<IEventStoreConnection> action)
        {
            try
            {
                action(connection);
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

        public T Invoke<T>(Func<IEventStoreConnection, T> func)
        {
            try
            {
                return func(connection);
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

        public bool TryInvoke(Action<IEventStoreConnection> action)
        {
            try
            {
                action(connection);
                return true;
            }
            catch (OperationTimedOutException)
            {
                return false;
            }
        }

        public bool TryInvoke<T>(Func<IEventStoreConnection, T> func, out T result)
        {
            try
            {
                result = func(connection);
                return true;
            }
            catch (OperationTimedOutException ex)
            {
                result = default(T);
                return false;
            }
        }

        public State State
        {
            get { return new State("Connection", GetType()); }
        }
    }
}