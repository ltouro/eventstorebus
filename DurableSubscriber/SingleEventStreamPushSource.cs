﻿using System;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class SingleEventStreamPushSource : IPushSource<int>
    {
        private readonly IEventStoreConnectionAccessor connection;
        private readonly string streamName;
        private int lastRecordedPosition;
        private EventStoreSubscription subscription;

        public SingleEventStreamPushSource(IEventStoreConnectionAccessor connection, string streamName)
        {
            this.connection = connection;
            this.streamName = streamName;
        }

        public event EventHandler<RecordedEventEventArgs<int>> Event;
        public event EventHandler<SubscriptionDroppedEventArgs<int>> SubscriptionDropped;

        public void Start()
        {
            if (!connection.TryInvoke(x => x.SubscribeToStream(streamName, true, OnEvent, OnDropped), out subscription))
            {
                OnDropped();
            }
        }

        private void OnDropped()
        {
            if (SubscriptionDropped != null)
            {
                SubscriptionDropped(this, new SubscriptionDroppedEventArgs<int>(lastRecordedPosition));
            }
        }

        private void OnEvent(ResolvedEvent resolvedEvent)
        {
            lastRecordedPosition = resolvedEvent.OriginalEventNumber+1;
            if (Event != null)
            {
                Event(this, new RecordedEventEventArgs<int>(resolvedEvent.Event, lastRecordedPosition));
            }
        }

        public void Stop()
        {
            if (subscription != null)
            {
                subscription.Unsubscribe();
            }
        }

        public State State
        {
            get
            {
                return new State("Push source for " + streamName, GetType())
                    .WithProperty("Source", streamName)
                    .WithAttribute("Postion", () => lastRecordedPosition);
            }
        }
    }
}