﻿using System;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class SubscriptionDroppedEventArgs<T> : EventArgs
    {
        public readonly T LastRecordedPosition;

        public SubscriptionDroppedEventArgs(T lastRecordedPosition)
        {
            LastRecordedPosition = lastRecordedPosition;
        }
    }
}