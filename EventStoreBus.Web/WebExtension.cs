﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Web
{
    //EventStoreBus.Web.RestEndpoint, EventStoreBus.Web
    public class WebExtension : IExtension
    {
        private static readonly ITypeWrapper[] typeWrappers = new ITypeWrapper[]
            {
                new WebFactoryTypeWrapper()
            };
        private static readonly ITypeAbbreviationProvider[] abbreviationProviders = new ITypeAbbreviationProvider[]
            {
                new RavenDBTypeAbbreviationProvider()
            };

        public IEnumerable<ITypeWrapper> TypeWrappers
        {
            get { return typeWrappers; }
        }

        public IEnumerable<ITypeAbbreviationProvider> AbbreviationProviders
        {
            get { return abbreviationProviders; }
        }

        private class RavenDBTypeAbbreviationProvider : ITypeAbbreviationProvider
        {
            public bool KnowsAbbreviationFor(string typeName)
            {
                return string.Compare(@"WebAPI", typeName, StringComparison.OrdinalIgnoreCase) == 0;
            }

            public string GetFullName(string abbreviation)
            {
                return typeof(RestEndpoint).AssemblyQualifiedName;
            }
        }

        private class WebFactoryTypeWrapper : ITypeWrapper
        {
            public bool CanWrap(Type canidate)
            {
                return canidate.GetInterfaces().Contains(typeof (IWebFactory))
                    && canidate.GetConstructor(Type.EmptyTypes) != null;
            }

            public Type Wrap(Type bareType)
            {
                return typeof (RestEndpointFactory<>).MakeGenericType(bareType);
            }
        }
    }
}