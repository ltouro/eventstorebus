﻿using System;
using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EventStoreBus.Logging;
using RazorEngine;
using RazorEngine.Templating;

namespace EventStoreBus.Web
{
    public class HtmlFormatter : MediaTypeFormatter
    {
        private static readonly ILogger log = LogManager.GetLoggerFor<HtmlFormatter>();

        public HtmlFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/xhtml+xml"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, System.Net.Http.HttpContent content, System.Net.TransportContext transportContext)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        try
                        {
                            var streamWriter = new StreamWriter(writeStream, System.Text.Encoding.UTF8);
                            var result = Razor.Resolve(type.FullName, value).Run(new ExecuteContext());
                            streamWriter.Write(result);
                            streamWriter.Flush();
                        }
                        catch (Exception ex)
                        {
                            log.ErrorException(ex, "Error while generating HTML.");
                        }
                    });
        }
    }
}