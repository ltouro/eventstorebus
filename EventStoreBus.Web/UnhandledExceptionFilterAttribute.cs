﻿using System;
using System.Web.Http.Filters;
using EventStoreBus.Logging;

namespace EventStoreBus.Web
{
    public class UnhandledExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var logger = LogManager.GetLoggerFor(context.ActionContext.ControllerContext.Controller.GetType());
            logger.ErrorException(context.Exception, "Unhandled exception.");
        }
    }
}