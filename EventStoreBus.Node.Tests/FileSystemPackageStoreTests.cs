﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NuGet;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Node.Tests
{
    [TestFixture]
    public class FileSystemPackageStoreTests
    {
        [Test]
        public void It_can_store_deploy_undeploy_and_remove_a_package()
        {
            var fileSystem = new PhysicalFileSystem(".");
            var remoteRepo = new DataServicePackageRepository(new Uri("http://localhost/NuGetServer/nuget"));
            var store = new FileSystemPackageStore(fileSystem,  remoteRepo, "");

            var packageId = new PackageId("Library1", new SemanticVersion(1,0,0,0));

            var deployment = store.DeployPackage("1", packageId, "SomeReceptor");

            Assert.IsTrue(fileSystem.FileExists(@"Packages\Library1.1.0.0.0\Library1.1.0.0.0.nupkg"));
            Assert.IsTrue(fileSystem.FileExists(@"Deployments\1\Library1.dll"));
            Assert.IsTrue(fileSystem.FileExists(@"Deployments\1\Library1.dll.config"));
            Assert.IsTrue(fileSystem.FileExists(@"Deployments\1\Library2.dll"));

            store.UndeployPackage(deployment);

            Assert.IsFalse(fileSystem.FileExists(@"Deployments\1\Library1.dll"));
            Assert.IsFalse(fileSystem.FileExists(@"Deployments\1\Library1.dll.config"));
            Assert.IsFalse(fileSystem.FileExists(@"Deployments\1\Library2.dll"));
            Assert.IsFalse(fileSystem.FileExists(@"Packages\Library1.1.0.0.0\Library1.1.0.0.0.nupkg"));
        }
    }
}
// ReSharper restore InconsistentNaming
